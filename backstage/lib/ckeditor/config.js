/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.width = '100%';
	config.height = '500px';
	config.language =  'zh';
	config.enterMode = CKEDITOR.ENTER_P;
	config.toolbar = [
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source' ] },
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
		{ name: 'editing', groups: [ 'find', 'selection' ], items: [ 'Find', 'Replace', 'SelectAll' ] },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
		{ name: 'links', items: [ 'Link', 'Unlink' ] },
		{ name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule' ] },
		'/',
		{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
		{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
		{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
	];

	// config.filebrowserUploadUrl = 'ckeditor/ckupload.php';
	config.filebrowserUploadUrl = 'lib/ckeditor/ckupload.php';
	config.filebrowserWindowHeight = '500';


};

CKEDITOR.on('instanceReady', function(ev) {
    with (ev.editor.dataProcessor.writer) {
        setRules("p", { indent: true, breakBeforeOpen: false, breakAfterOpen: false, breakBeforeClose: false, breakAfterClose: false });
        setRules("h1", { indent: true, breakBeforeOpen: false, breakAfterOpen: false, breakBeforeClose: false, breakAfterClose: false });
        setRules("h2", { indent: true, breakBeforeOpen: false, breakAfterOpen: false, breakBeforeClose: false, breakAfterClose: false });
        setRules("h3", { indent: true, breakBeforeOpen: false, breakAfterOpen: false, breakBeforeClose: false, breakAfterClose: false });
        setRules("h4", { indent: true, breakBeforeOpen: false, breakAfterOpen: false, breakBeforeClose: false, breakAfterClose: false });
        setRules("h5", { indent: true, breakBeforeOpen: false, breakAfterOpen: false, breakBeforeClose: false, breakAfterClose: false });
        setRules("div", { indent: true, breakBeforeOpen: false, breakAfterOpen: false, breakBeforeClose: false, breakAfterClose: false });
        setRules("table", { indent: true, breakBeforeOpen: false, breakAfterOpen: false, breakBeforeClose: false, breakAfterClose: false });
        setRules("tr", { indent: true, breakBeforeOpen: false, breakAfterOpen: false, breakBeforeClose: false, breakAfterClose: false });
        setRules("td", { indent: true, breakBeforeOpen: false, breakAfterOpen: false, breakBeforeClose: false, breakAfterClose: false });
        setRules("iframe", { indent: true, breakBeforeOpen: false, breakAfterOpen: false, breakBeforeClose: false, breakAfterClose: false });
        setRules("li", { indent: true, breakBeforeOpen: false, breakAfterOpen: false, breakBeforeClose: false, breakAfterClose: false });
        setRules("ul", { indent: true, breakBeforeOpen: false, breakAfterOpen: false, breakBeforeClose: false, breakAfterClose: false });
        setRules("ol", { indent: true, breakBeforeOpen: false, breakAfterOpen: false, breakBeforeClose: false, breakAfterClose: false });
        setRules("object", { indent: true, breakBeforeOpen: false, breakAfterOpen: false, breakBeforeClose: false, breakAfterClose: false });
        setRules("embed", { indent: true, breakBeforeOpen: false, breakAfterOpen: false, breakBeforeClose: false, breakAfterClose: false });
        setRules("param", { indent: true, breakBeforeOpen: false, breakAfterOpen: false, breakBeforeClose: false, breakAfterClose: false });
    }
})
