<?php

$url = '../';

function photoUpload($fileSource, $photoDir, $thumbDir, $photoIDname){
  
  $fileExt = $fileSource['type'];
  
  if($fileExt == 'image/jpeg'){
    $fileType=".jpg";
  }elseif($fileExt == 'image/gif'){
    $fileType=".gif";
  }elseif($fileExt == 'image/png'){
    $fileType=".png";
  }else{
    exit;
  }

  if($fileType == ".jpg"){
    $src = imagecreatefromjpeg($fileSource['tmp_name']);
  }elseif($fileType == ".gif"){
    $src = imagecreatefromgif($fileSource['tmp_name']);
  }elseif($fileType == ".png"){
    $src = imagecreatefrompng($fileSource['tmp_name']);
  }

  //取得來源圖片長寬
  $src_w = imagesx($src);
  $src_h = imagesy($src);

  // //設定小縮圖長寬
  // if($src_w < $src_h){
  //   $thumb_w = 200;
  //   $thumb_h = intval($src_h / $src_w * 200);
  // }else{
  //   $thumb_h = 130;
  //   $thumb_w = intval($src_w / $src_h * 130);
  // }
  
  // //建立小縮圖
  // $thumb = imagecreatetruecolor($thumb_w, $thumb_h);
  // imagecopyresampled($thumb, $src, 0, 0, 0, 0, $thumb_w, $thumb_h, $src_w, $src_h);
  // //儲存小縮圖到thumb
  // imagejpeg($thumb, $thumbDir.$photoIDname.".jpg", 100);

  //設定大縮圖長寬
  // if($src_w > $src_h){
  //   $thumb_w = 800;
  //   $thumb_h = intval($src_h / $src_w * 800);
  // }else{
  //   $thumb_h = 600;
  //   $thumb_w = intval($src_w / $src_h * 600);
  // }
  //保持原圖大小
  $thumb_h = $src_h;
  $thumb_w = $src_w;

  //建立儲存大縮圖
  $thumb = imagecreatetruecolor($thumb_w, $thumb_h);
  imagecopyresampled($thumb, $src, 0, 0, 0, 0, $thumb_w, $thumb_h, $src_w, $src_h);
  imagejpeg($thumb, $photoDir.$photoIDname.".jpg", 100);
  
  return($photoIDname.".jpg");
}

  // $url = "../images/uploads/".time()."_".$_FILES['upload']['name'];

  $imageUploaded = photoUpload($_FILES['upload'], "../../../upload/pic/", "../../../upload/pic/thumb/", date("Y-m-d-").time());
  //echo("<img src='".$imageUploaded."''>");

?>

<script type="text/javascript">
    var CKEditorFuncNum = <?php echo $_GET['CKEditorFuncNum'];?>;
    window.parent.CKEDITOR.tools.callFunction( CKEditorFuncNum, '<?php echo($url."upload/pic/".$imageUploaded);?>' );
</script>

