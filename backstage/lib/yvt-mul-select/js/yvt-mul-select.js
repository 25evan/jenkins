/*
* YvetteMultiSelect v0.1.0
* Copyright (c) 2014 Thundersha Kuo @ 25sprout
* REF: https://github.com/lou/multi-select/blob/master/js/jquery.multi-select.js
*/
!function ($) {
	"use strict";

 	/* YvetteMultiSelect CLASS DEFINITION
  	* ====================== */

  	var YvetteMultiSelect = function(element, options){
  		this.opts = options;
  		this.$ele = $(element);
  		this.$container = $('<div/>', { 'class': "yms-container" });
  		this.$srcSel = $('<select multiple/>');
  		this.$srcFlt = $('<input/>');
  	}

	YvetteMultiSelect.prototype = {
		constructor: YvetteMultiSelect,
		init: function(){
			var that = this,
				$yms = this.$ele,
				$ymsParent = $yms.parent();

			var sel_arr = this.opts.data_array;

			if( sel_arr.length > 0 ){
				//UI moving
				// $ymsParent.append( this.$container );
				// this.$srcSel.attr('style',"height:20em;").appendTo(this.$container);
				// this.$container.append(this.$srcFlt);
				// $yms.attr('style',"height:20em;").appendTo(this.$container);

				//bootstrap UI ini
  				this.$container.addClass('form-group');
  				this.$srcFlt.attr('placeholder','快速篩選').addClass('form-control');
  				this.$srcSel.attr('placeholder','快速篩選').addClass('form-control');
  				$yms.attr('placeholder','快速篩選').addClass('form-control');

  				var $l_container = $('<div/>', { 'class': "col-md-4" });
  				var $m_container = $('<div/>', { 'class': "col-md-4" });
  				var $r_container = $('<div/>', { 'class': "col-md-4" });

  				$ymsParent.append( this.$container );

  				this.$container.append($l_container).append($m_container).append($r_container);

				this.$srcSel.attr('style',"height:20em;").appendTo($l_container);

				$m_container
					.append('<div class="text-center"><br/><br/><p class="bg-info yvt-info"><br/>點選左邊將選項加入右邊<br/><br/></p><i class="fa fa-arrows-h"></i><br/></div><br/><br/>')
					.append(this.$srcFlt)
					.append('<div class="text-center"><p class="bg-success yvt-search"><br/><i class="fa fa-search"></i> 利用文字篩選左側選項<br/>找不到標籤嗎？請先<a href="yvt-list.php?yvtset=tags" target="_blank">建立新的標籤</a><br/><br/></p></div>');

				$yms.attr('style',"height:20em;").appendTo($r_container);
  				//bootstrap UI end



				var $ySrcSel = this.$srcSel;
				var $ySrcIpt = this.$srcFlt;
				var $yTgtSel = $yms;


				var selectedOptsArr = $yTgtSel.val();

				// for (var i = 0 ; i<1024; i++) {
				for(var i in sel_arr) {
					if(i > 1024){
						continue;
					}else{
						var se = sel_arr[i];
						if(selectedOptsArr!=null){
							if(selectedOptsArr.indexOf(se.v)<0){
								var $nOpt = $('<option value="'+se.v+'">'+se.t+'</option>');
								$ySrcSel.append($nOpt);
							}
						}else{
							var $nOpt = $('<option value="'+se.v+'">'+se.t+'</option>');
							$ySrcSel.append($nOpt);
						}
					}
				};

				if( sel_arr.length > 1024 ){
					//$ySrcIpt
					$ySrcIpt.attr('placeholder','選項超過1000個，請用這邊篩選');
				}

				$ySrcIpt.on('keydown',function(e){
					if ( e.which == 13 ) {
						e.preventDefault();
						$ySrcIpt.trigger('change');
					}
				});

				$ySrcIpt.on('change',function(){

					var selectedOptsArr = $yTgtSel.val();

					var sch_str = $(this).val();
					var rlt_arr = [];
					$.each(sel_arr,function(){
						if(this.t.search(sch_str) >= 0){
							rlt_arr.push(this);
						}
					});

					if(rlt_arr.length > 0){
						$ySrcSel.children().remove();
						$.each(rlt_arr,function(){
							// var $nOpt = $('<option value="'+this.v+'">'+this.t+'</option>');
							// $ySrcSel.append($nOpt);
							if(selectedOptsArr!=null){
								if(selectedOptsArr.indexOf(this.v)<0){
									var $nOpt = $('<option value="'+this.v+'">'+this.t+'</option>');
									$ySrcSel.append($nOpt);
								}
							}else{
								var $nOpt = $('<option value="'+this.v+'">'+this.t+'</option>');
								$ySrcSel.append($nOpt);
							}
						});
					}
				});

				
				//TODO try filter or find
				$ySrcSel.on('click',function(){
					$(this).find('option:selected').each(function(){
						var $nOpt = $('<option value="'+$(this).attr('value')+'" selected="selected">'+$(this).html()+'</option>');
						$yTgtSel.append($nOpt);
						$(this).remove();

						$yTgtSel.find('option').attr('selected','selected');
					});
				});

				$yTgtSel.on('click',function(){
					$(this).find('option:selected').each(function(){
						var $nOpt = $('<option value="'+$(this).attr('value')+'">'+$(this).html()+'</option>');
						$ySrcSel.prepend($nOpt);
						$(this).remove();

						$yTgtSel.find('option').attr('selected','selected');
					});
				});

			}else{
				//TODO err_msg
				alert('data_array in options need to set');
			}


		}
	};

	/* YvetteMultiSelect PLUGIN DEFINITION
	* ======================= */
	$.fn.yvetteMultiSelect = function(){
		var option = arguments[0],
			args = arguments;
    	
    	return this.each(function(){
			var $this = $(this),
				data = $this.data('yvetteMultiSelect'),
				options = $.extend({}, $.fn.yvetteMultiSelect.defaults, $this.data(), typeof option === 'object' && option);

			if (!data){ $this.data('yvetteMultiSelect', (data = new YvetteMultiSelect(this, options))); }

			if (typeof option === 'string'){
				data[option](args[1]);
			} else {
				data.init();
			}
		});
	};

	$.fn.yvetteMultiSelect.defaults = {
		// keySelect: [32],
		// selectableOptgroup: false,
		// disabledClass : 'disabled',
		// dblClick : false,
		// keepOrder: false,
		// cssClass: ''
	};

  $.fn.yvetteMultiSelect.Constructor = YvetteMultiSelect;

}(window.jQuery);