<?php
	session_start();
	include("apo/sqldata.php");

	// 如果沒有權限 直接踢出
	if($_SESSION['ACCOUNT_TYPE'] == null || $_SESSION['ACCOUNT_TYPE'] != 1){
		echo "<script language=javascript>
	      window.location.replace(\"apo/logout.php\");
	      top.leftFrame.location.reload();
	      </script>";
	}

	// 刪除帳號
	if(isset($_GET["delete"]) && $_GET["delete"] == true){
		$accoutDeleteSql = $dbConnect->prepare("DELETE FROM `Account` WHERE `company_id` = ?;");
		$accoutDeleteSql->execute(array($_GET["cid"]));
		header('Location: account.php');
	}

	// 更新 Update 表單
	if(isset($_POST["updateForm"]) && $_POST["updateForm"] == "update"){
		if(isset($_POST["password"]) && $_POST["password"] != null){
			$accoutUpdateSql = $dbConnect->prepare("UPDATE `Account` SET `name` = ?,`department` = ?, `account_type` = ?, `password` = ? WHERE `company_id` = ? ;");
			$err = $accoutUpdateSql->execute(array($_POST["name"], $_POST["department"], $_POST["account_type"], md5($_POST["company_id"].$_POST["password"]), $_GET["cid"]));
		}else if(isset($_POST["email"]) && $_POST["email"] != null){
			$accoutUpdateSql = $dbConnect->prepare("UPDATE `Account` SET `name` = ?,`department` = ?, `email` = ?, `account_type` = ? WHERE `company_id` = ? ;");
			$err = $accoutUpdateSql->execute(array($_POST["name"], $_POST["department"], $_POST["email"], $_POST["account_type"], $_GET["cid"]));
		}else{
			$accoutUpdateSql = $dbConnect->prepare("UPDATE `Account` SET `name` = ?,`department` = ?, `account_type` = ? WHERE `company_id` = ? ;");
			$err = $accoutUpdateSql->execute(array($_POST["name"], $_POST["department"], $_POST["account_type"], $_GET["cid"]));
		}
		if($err){
			echo "<script language=javascript>
				alert('更新成功！');
				</script>";
		}else{
			echo "<script language=javascript>
				alert('更新失敗！');
				</script>";
		}
	}

	// 把欄位帶入
	if(isset($_GET["cid"]) && $_GET["cid"] != ""){
		$accoutUpdateListSql = $dbConnect->prepare("SELECT a.*, p.`account_type_name` FROM `Account` AS a LEFT JOIN `AccountPrivilege` AS p ON a.`account_type` = p.`account_type` WHERE a.`company_id` = ?;");
		$accoutUpdateListSql->execute(array($_GET["cid"]));

		if($accoutUpdateListSql->rowCount() > 0){
			$accoutUpdateListRow = $accoutUpdateListSql->fetch(PDO::FETCH_ASSOC);
		}else{
			header('Location: account.php');
		}
	}

	// 重送驗證信
	if(isset($_GET["email"]) && $_GET["email"] != "" && isset($_GET['cid']) && $_GET['cid'] != ""){
		$email = $_GET["email"];

		$accoutListSql = $dbConnect->prepare("SELECT * FROM `Account` WHERE `company_id` = ?;");
		$accoutListSql->execute(array($_GET['cid']));

		if($accoutListSql->rowCount() > 0){
			$accoutListRow = $accoutListSql->fetch(PDO::FETCH_ASSOC);
			$token = $config['hostUrl']."/backstage/pw-reset.php?cid=".$_GET["cid"]."&token=".md5($_GET["cid"].$accoutListRow["create_time"]);
		
			$message = '\
				<meta http-equiv="content-type" content="text/html; charset=UTF-8" />\
				<body style="margin:0; padding:15px; background: #F5F5F5">\
				<div style="width:100%; background: #F5F5F5; padding: 30px">\
					<div style="background: #FFF; margin: 0 auto; padding: 20px; width: 420px; -webkit-border-bottom-right-radius: 10px;-webkit-border-bottom-left-radius: 10px;-moz-border-radius-bottomright: 10px;-moz-border-radius-bottomleft: 10px;border-bottom-right-radius: 10px;border-bottom-left-radius: 10px; border-top: 5px solid #009bc2">\
						<h1 style="font: 20px/36px Lucida Grande, Helvetica, Arial, sans-serif; color: #444;">親愛的 '.$accoutListRow["name"].',</h1>\
						<h2 style="font: 16px/16px Lucida Grande, Helvetica, Arial, sans-serif; color: #444;">歡迎來到 '.$config['project'].' Backstage管理後台</h2><br/>\
						<p style="font: 14px/26px Lucida Grande, Helvetica, Arial, sans-serif; color: #555;">\
							您的帳號已經建立成功，請點擊以下連結設定您的密碼，完成註冊：</p>\
						<p style="font: 14px/26px Lucida Grande, Helvetica, Arial, sans-serif; color: #000;">\
						<p style="font: 14px/20px Lucida Grande, Helvetica, Arial, sans-serif; color: #555;">\
							<a href="'.$token.'" style="background:#48B0EC; color:#FFF; text-decoration:none; padding:8px 15px; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;">設定我的密碼</a>\
						</p>\
						<br />\
						<p style="font: 12px/20px Lucida Grande, Helvetica, Arial, sans-serif; color: #F26522;">With love,	<br />\
							25sprout</p>\
					</div>\
				</div>\
				</body>\
				</html>';
		}
	}else{
		$email = null;
		$message = null;
	}
?>


<?php include("source/head.php"); ?>

<script type="text/javascript">
	$(document).ready(function() {
		var cid = "<?php echo($_GET['cid']); ?>";
		var regMail = "<?php echo($email); ?>";

		$('.btn-account-delete').click(function() {
			var deleteAccountChk = confirm('你確定要刪除這個帳號嗎？刪除後是沒有辦法復原的喔，請三思！');
			
			if (deleteAccountChk) {
				window.location = 'account-update.php?cid='+cid+'&delete=true';
			}
		});

		$('.btn-account-mail').click(function() {
			window.location = 'account-update.php?email='+$('.email-input').val()+'&cid='+$('.cid-input').val();
		});

		if(regMail != ""){
			$.ajax({
				type: 'POST',
				cache: false,
				url: 'http://www.25sprout.com/aws_ses/index_super.php',
				data: {
					mailer: 'aws',
					Source: 'evan@25sprout.com',
					ToAddresses: regMail,
					CcAddresses: '',
					BccAddresses: '',
					Subject: '註冊通知：愛藝享 Backstage管理後台',
					Body: '<?php echo($message); ?>',
					ReplyToAddresses: 'evan@25sprout.com',
					ReturnPath: 'evan@25sprout.com',
					Success: '成功寄出，謝謝您的來信',
					Failed: '信件尚未寄出，請稍後再試'
				},
				error: function() {
					alert("送出失敗");
					// 看你前台有沒有要有什麼變化。
				},
				success: function(data) {
					alert('已成功寄出密碼開通信至該帳戶信箱，請提醒對方收信完成開通帳號。');
					window.location = "account.php";
					// location.reload();
					// 看你前台有沒有要有什麼變化。
				}
			});
		}
	});
</script>


<body id="account">
	<section id="container">
		<?php include("source/header.php"); ?>
		<?php include("source/navi.php"); ?>
	
		<section id="main">
			<div class="title-wrapper">
				<div class="title-content pull-left">
					<h3>帳號資訊更新</h3>
					<small>變更使用者的基本資料</small>
				</div>
				<div class="title-plus pull-right">
					<?php
						if($accoutUpdateListRow["email"] != null){
							echo '
					<a href="#" type="button" class="btn btn-danger btn-account-mail"><i class="fa fa-paper-plane-o"></i> 再次發送驗證 E-mail</a>
							';
						}
					?>
					<a href="#" type="button" class="btn btn-danger btn-account-delete"><i class="fa fa-trash-o"></i> 刪除本帳號</a>
				</div>
			</div>

			<form class="form-horizontal  tasi-form" action="" method="post">
				<section class="panel">
					<header class="panel-heading">
						修改帳號
					</header>
					
					<div class="panel-body">
						<div class="form-group">
							<label class="control-label col-md-2">帳號</label>
							<div class="col-md-8">
								<input size="16" type="text" name="company_id" class="form-control wf200 cid-input" readonly="readonly" value="<?php echo($accoutUpdateListRow["company_id"]); ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">登入會員姓名</label>
							<div class="col-md-8">
								<input size="16" type="text" name="name" class="form-control wp50" value="<?php echo($accoutUpdateListRow["name"]); ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">部門別</label>
							<div class="col-md-8">
								<input size="16" type="text" name="department" class="form-control wp50" value="<?php echo($accoutUpdateListRow["department"]); ?>">
							</div>
						</div>
						<?php
							if($accoutUpdateListRow["email"] != null){
								echo '
						<div class="form-group">
							<label class="control-label col-md-2">E-mail</label>
							<div class="col-md-8">
								<input size="16" type="text" name="email" class="form-control wp50 email-input" value="'.$accoutUpdateListRow["email"].'">
								<br/>
								<span class="label label-danger">Note</span>
								當建立帳號時，系統會自動寄發一封驗證信至這個 E-mail 要求設定新的密碼進行登入。
							</div>
						</div>
								';
							}else{
								echo '
						<div class="form-group password">
							<label class="control-label col-md-2">重設密碼</label>
							<div class="col-md-8">
								<input size="16" type="password" maxLength="8" name="password" class="form-control wf200">
								<br/>
								<span class="label label-danger">Note</span>
								請輸入8個字密碼
							</div>
						</div>
								';
							}
						?>
						<div class="form-group">
							<label class="control-label col-md-2">會員類別</label>
							<div class="col-md-8">
								<select class="form-control m-bot15 wf200" name="account_type" value="<?php echo($accoutUpdateListRow["account_type"]); ?>">
									<?php
										$privilegeSelectSql = $dbConnect->prepare("SELECT * FROM `AccountPrivilege` ORDER BY `account_type` ;");
										$privilegeSelectSql->execute();
										$privilegeSelectDetail = $privilegeSelectSql->fetchAll(PDO::FETCH_ASSOC);
										foreach ($privilegeSelectDetail as $key => $value) {
											if($privilegeSelectDetail[$key]['account_type'] == $accoutUpdateListRow['account_type']){
												echo '
									<option value="'.$privilegeSelectDetail[$key]['account_type'].'" selected>'.$privilegeSelectDetail[$key]['account_type_name'].'</option>
												';
											}else{
												echo '
									<option value="'.$privilegeSelectDetail[$key]['account_type'].'">'.$privilegeSelectDetail[$key]['account_type_name'].'</option>
												';
											}
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">帳號建立者</label>
							<div class="col-md-8">
								<input size="16" type="text" class="form-control wf200" readonly="readonly" value="<?php echo($accoutUpdateListRow["creator"]); ?>">
							</div>
						</div>
					</div>
				</section>

				<hr/>
				<input type="hidden" name="updateForm" value="update"/>
				<input type="submit" class="btn btn-primary" value="確定">
			</form>
		</section>
	</section>


</body>

<?php include("source/footer.php"); ?>
