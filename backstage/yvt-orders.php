<?php
	require("apo/session.php");
	include("apo/sqldata.php");

	$slides_dir = '../upload/slides/';

?>


<?php include("source/head.php"); ?>

<script type="text/javascript">
$('document').ready(function(){
	$( ".slides-sortable" ).sortable({
		// placeholder: "ui-state-highlight",
		stop:function(event, ui){
           console.log("stop: "+$(ui.item).attr('rel'));
		}
		// stop: function(){
		// 	// $('.bs_btn_upd').addClass('bs_btn_sort_key');
		// 	// $('.bs_btn_upd').
		// 	console.log("stop: "+$(this).children('').attr('rel'));
		// 	// arr.push();
		// }
	});
});
</script>

<body id="slides">
	<section id="container">
		<?php include("source/header.php"); ?>
		<?php include("source/navi.php"); ?>
	
		<section id="main">
			<div class="title-wrapper">
				<div class="title-content pull-left">
					<h3>首頁 slides 管理</h3>
					<small>您可以上傳 slides、調整順序 </small>
				</div>
				<div class="title-plus pull-right">
					<!-- 可以在 #main 右上角放一些額外的按鈕 -->
				</div>
			</div>
				<section class="panel">
					<header class="panel-heading">
						輪播slides上傳 <span class="size">尺寸：980px 寬 * 675px 高</span>
					</header>
					
					<div class="panel-body">

						<div class="form-group">
							<label class="control-label col-md-2">輪播slides上傳</label>
							<div class="col-md-8">
								<form id="form-upload" action="apo/slides-upload.php" method="post" enctype="multipart/form-data">
									<input name="upload[]" type="file" multiple class="form-control">
									<input name="tabid" type="hidden" value="46" />
									<input name="width" type="hidden" value="1440"/>
									<br/>
									<button class="btn btn-primary" type="submit" btn-done="完成">
										<i class="icon-upload icon-white"></i> 
										上傳
									</button>
								</form>
							</div>
						</div>
					</div>
				</section>


				<section class="panel">
					<header class="panel-heading">
						編輯原有的 slides (您可以移動順序)
					</header>
					
					<div class="panel-body">

			<form class="form-horizontal  tasi-form" action="apo/slides-update.php" method="post" enctype="multipart/form-data">

						<div class="form-group">
						<ul class="slides-sortable">
						<?php
							$slidesSelectSql = $dbConnect->prepare("SELECT * FROM `Slides` ORDER BY `seqid` ASC;");
							$slidesSelectSql->execute();
							while($slidesSelectDetail = $slidesSelectSql->fetch(PDO::FETCH_ASSOC)){
								// print_r($slidesSelectDetail);
						?>
						<div class="bs-callout bs-callout-warning">
							<li id="slide-<?php echo $slidesSelectDetail['id']; ?>" rel="<?php echo $slidesSelectDetail['seqid']; ?>">
								<!-- <form> -->
									<h3><?php echo $slidesSelectDetail['seqid']; ?></h3>
									<div class="slides-img-wrapper">
										<img src="<?php echo $slides_dir.$slidesSelectDetail['imgname']; ?>" style="width: 300px;"/>
									</div>
									<div class="slides-content-wrapper">
										<input type="hidden" name="id[]" value="<?php echo $slidesSelectDetail['id']; ?>" />
										<table>
											<tr>
												<td>連結</td>
												<td>
													<?php 
														if($slidesSelectDetail['link'] != ''){
															echo'<input size="16" class="form-control" type="text" name="linkhref[]" placeholder="請輸入連結" value="'.$slidesSelectDetail['link'].'" />';
														} else {
															echo'<input size="16" class="form-control" type="text" name="linkhref[]" placeholder="請輸入連結" value="http://" />';
														}
													?>
												</td>
											</tr>
											<tr>
												<td>圖片說明</td>
												<td>
													<?php 
														if($slidesSelectDetail['caption'] != ''){
															echo'<input size="16" class="form-control" type="text" name="caption[]" placeholder="請輸入圖片說明" value="'.$slidesSelectDetail['caption'].'" />';
														} else {
															echo'<input size="16" class="form-control" type="text" name="caption[]" placeholder="請輸入圖片說明" value="" />';
														}
													?>
												</td>
											</tr>
										</table>
										<hr/>
										<a href="apo/slides-delete.php?delete=<?php echo $slidesSelectDetail['id']; ?>" class="btn btn-small btn-danger">
											<i class="icon-trash icon-white"></i>
											刪除
										</a>
									</div>
								<!-- </form> -->
							</li>
						</div>
						<?php
							}
						?>
						</ul>
						</div>

					</div>
				</section>
				<input type="submit" class="btn btn-primary" value="確定">
			</form>
		</section>
	</section>


</body>

<?php include("source/footer.php"); ?>
