<?php
	require("apo/session.php");
	include("apo/sqldata.php");
	include("source/head.php");

	//TODO
	//2file ini
	if(isset($_GET['yvtset'])){
		$yvtset = $_GET['yvtset'];
	}

	require("yvtset/".$yvtset.".php");

	$idx_photo_flag = false;
	$url_photo_flag = false;

	$idx_photo_default_path = null;
	//2file end

	//foreign keys
	$fk_defaults = array();
	if( isset($yvtListMeta['fk_keys']) ){
		foreach ($_GET as $gk => $gv) {
			if( in_array($gk, $yvtListMeta['fk_keys']) ){
				$fk_defaults[$gk] = $gv;
			}
		}
	}
?>

<!-- CKEditor  -->
<script type="text/javascript" src="lib/ckeditor/ckeditor.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	// CKEDITOR
	// CKEDITOR.replaceAll();
	// CKEDITOR.replace( '.bs-ckeditor', {
	// });
});
</script>

<style>
 
/* Side notes for calling out things
-------------------------------------------------- */
 
/* Base styles (regardless of theme) */
.bs-callout {
  margin: 20px 0;
  padding: 15px 30px 15px 15px;
  border-left: 5px solid #eee;
}
.bs-callout h4 {
  margin-top: 0;
}
.bs-callout p:last-child {
  margin-bottom: 0;
}
.bs-callout code,
.bs-callout .highlight {
  background-color: #fff;
}
 
/* Themes for different contexts */
.bs-callout-danger {
  background-color: #fcf2f2;
  border-color: #dFb5b4;
}
.bs-callout-warning {
  background-color: #fefbed;
  border-color: #f1e7bc;
}
.bs-callout-info {
  background-color: #f0f7fd;
  border-color: #d0e3f0;
}

</style>

<body id="<?php echo($yvtListMeta['label_id']); ?>">
	<section id="container">
		<?php include("source/header.php"); ?>
		<?php include("source/navi.php"); ?>
	
		<section id="main">
			<div class="title-wrapper">
				<div class="title-content pull-left">
					<h3><?php echo($yvtListMeta['labelname']); ?>建立</h3>
					<small>建立一個新<?php echo($yvtListMeta['labelname']); ?></small>
				</div>
				<div class="title-plus pull-right">
					<!-- 可以在 #main 右上角放一些額外的按鈕 -->
					<a href="yvt-list.php?yvtset=<?php echo($yvtset);?>" type="button" class="btn btn-danger"><i class="fa fa-list"></i> <?php echo($yvtListMeta['labelname']); ?>列表</a>
				</div>
			</div>
			<form class="form-horizontal  tasi-form" action="apo/yvt-insert.php" method="post" enctype="multipart/form-data">
				<section class="panel">
					<header class="panel-heading">
						新增<?php echo($yvtListMeta['labelname']); ?>
					</header>

					<!-- for vyt system -->
					<input type="hidden" name="yvtset" value="<?php echo($yvtset);?>" />
					
					<div class="panel-body">

						<!-- <div class="form-group">
							<label class="control-label col-md-2">狀態</label>
							<div class="col-md-8">
								<select class="form-control m-bot15 wf200" name="types">
									<option value="SHOW">顯示(上線)</option>
									<option value="OFF">隱藏(草稿)</option>
								</select>
							</div>
						</div> -->
						<?php
							foreach($yvtListMeta['columns'] as $mtk => $mtv){
								if(isset($mtv['hint'])){
									$labelname .= '<br/><span class="yvt-hint">'.$mtv['hint'].'</span>';
								}
								if( isset($mtv['edittype']) ){
									if( $mtv['edittype']=="input" ){
										//INPUT
						// 				echo('
						// <div class="form-group">
						// 	<label class="control-label col-md-2">'.$mtv['label'].'</label>
						// 	<div class="col-md-8">
						// 		<input size="16" style="float:left;" type="text" name="'.$mtk.'" class="form-control wp25" ');
										// by Evan editclass
										if(isset($mtv['editclass'])){
											$editclass = $mtv['editclass'];
										}else{
											$editclass = null;
										}
										echo('
						<div class="form-group">
							<label class="control-label col-md-2">'.$mtv['label'].'</label>
							<div class="col-md-8">
								<input size="16" style="float:left;" type="text" name="'.$mtk.'" class="form-control '.$editclass.'" ');
										if( isset($fk_defaults[$mtk]) ){
											echo(' value="'.$fk_defaults[$mtk].'" ');
										}else if( isset($mtv['defaultvalue']) ){
											echo(' value="'.$mtv['defaultvalue'].'" ');
										}
										if( isset($mtv['placeholder']) ){
											echo(' placeholder="'.$mtv['placeholder'].'" ');
										}
										if( isset($_POST[$mtk]) ){
											echo(' value="'.$_POST[$mtk].'" ');
										}
										echo(' >
							</div>
						</div>
											');
									}else if( $mtv['edittype']=="select" ){
										//SELECT
										echo('
						<div class="form-group">
							<label class="control-label col-md-2">'.$mtv['label'].'</label>
							<div class="col-md-8">
								<select class="form-control m-bot15" name="'.$mtk.'">');
										foreach ($mtv['editarray'] as $ek => $ev) {
											//<option value="'.$ev['ID'].'">'.$ev['text'].print_r($mtv['listmapping'],true).'</option>
											$selk = "ID";
											$selv = "text";
											foreach( $mtv['listmapping'] as $k => $v ){
												$selk = $k;
												$selv = $v;
											}

											echo('
									<option value="'.$ev[$selk].'" ');

											if( isset($_POST[$mtk]) ){
												if($_POST[$mtk]===$ev[$selk]){
													echo(' selected="selected" ');
												}
											}


											echo(' >'.$ev[$selv].'</option>
												');
										}
								echo('</select>
							</div>
						</div>
											');
							// <div class="col-md-3">
							// 	<div class="input-group">
      	// 								<input type="text" class="form-control q-sch-ipt" placeholder="快速篩選" />
      	// 								<div class="input-group-addon">
      	// 									<i class="fa fa-search"></i>
      	// 								</div>
    			// 					</div>
							// </div>
									}else if( $mtv['edittype']=="textarea" || $mtv['edittype']=="ckeditor" ){
										//TEXTAREA
										if(isset($mtv['editclass'])){
											$editclass = $mtv['editclass'];
										}else{
											$editclass = null;
										}

										if( $mtv['edittype']=="ckeditor" ){
											$editclass .= " ckeditor ";
										}

										echo('
						<div class="form-group">
							<label class="control-label col-md-2">'.$mtv['label'].'</label>
							<div class="col-md-8">
								<textarea class="form-control '.$editclass.'" name="'.$mtk.'" ');
										if( isset($mtv['placeholder']) ){
											echo(' placeholder="'.$mtv['placeholder'].'" ');
										}
										echo(' >');
										if( isset($_POST[$mtk]) ){
											echo($_POST[$mtk]);
										}else if( isset($mtv['defaultvalue']) ){
											echo($mtv['defaultvalue']);
										}
										echo('</textarea>
							</div>
						</div>');
									}else if( $mtv['edittype']=="idx_photo" ){
										$idx_photo_flag = true;
										if( isset($mtv['defaultvalue']) ){
											if( file_exists("../".$idx_photo_default_path) ){
												$idx_photo_default_path = $mtv['defaultvalue'];
											}
										}
									}else if( $mtv['edittype']=="url_photo" ){
										$url_photo_flag = true;
									}else{
										//HIDE
						// 				echo('
						// <div class="form-group">'.$mtv['edittype'].'</div>
						// 				');
									}
								}
							}

							//idx_photo
							if( $idx_photo_flag ){
								$idxPhotoMeta = null;
								$cut_info = "";
								$labelname = $yvtListMeta['labelname']."圖片";
								foreach($yvtListMeta['columns'] as $mtk => $mtv){
									if( isset($mtv['edittype']) ){
										if( $mtv['edittype']=="idx_photo" ){
											$idxPhotoMeta = $mtv;
										}
									}
								}

								if( $idxPhotoMeta!=null ){
									if(isset($idxPhotoMeta['photo_sizes'])){
										$cut_info = '上傳後系統自動裁切大小：寬：'.$idxPhotoMeta['photo_sizes']['w'].'px';
										if(isset($idxPhotoMeta['photo_sizes']['h'])){
											$cut_info .= ', 高：'.$idxPhotoMeta['photo_sizes']['h'].'px';
										}
									}
									if(isset($idxPhotoMeta['label'])){
										$labelname = $idxPhotoMeta['label'];
									}
									if(isset($idxPhotoMeta['hint'])){
										$labelname .= '<br/><span class="yvt-hint">'.$idxPhotoMeta['hint'].'</span>';
									}
								}
								echo('
						<div class="form-group">
							<label class="control-label col-md-2">'.$labelname.'</label>
							<div class="col-md-8">
								<input size="16" type="file" name="upload-photos-idx" class="form-control wp50">
								<span class="size">'.$cut_info.'</span><br/>
								');
								if($idx_photo_default_path!=null){
									echo('
								或者<br/>
								<input type="checkbox" name="upload-photos-idx-default" value="default" checked="checked"> 使用預設圖片 (將會刪除上傳圖片)<br/>
								<img src="'.$idx_photo_default_path.'" class="img-thumbnail wp25" />
								
									');
								}
								echo('
							</div>
						</div>
							');
							}

							//url_photo
							if( $url_photo_flag ){
								$urlPhotoMeta = null;
								$cut_info = "";
								$labelname = $yvtListMeta['labelname'];
								foreach($yvtListMeta['columns'] as $mtk => $mtv){
									if( isset($mtv['edittype']) ){
										if( $mtv['edittype']=="url_photo" ){
											$urlPhotoMeta = $mtv;
										}
									}
								}

								if( $urlPhotoMeta!=null ){
									if(isset($urlPhotoMeta['photo_sizes'])){
										$cut_info = '上傳後系統自動裁切大小：寬：'.$urlPhotoMeta['photo_sizes']['w'].'px';
										if(isset($urlPhotoMeta['photo_sizes']['h'])){
											$cut_info .= ', 高：'.$urlPhotoMeta['photo_sizes']['h'].'px';
										}
									}
									if(isset($urlPhotoMeta['label'])){
										$labelname = $urlPhotoMeta['label'];
									}
								}
								echo('
						<div class="form-group">
							<label class="control-label col-md-2">'.$labelname.'</label>
							<div class="col-md-8">
								<input size="16" type="file" name="upload-photos-url" class="form-control wp50">
								<span class="size">'.$cut_info.'</span><br/>
							</div>
						</div>
							');
							}
						?>
					</div>
				</section>
				<hr/>
				<?php
				$submitStr = "確定";
				$submitStr_NEW = false;
				if(isset($yvtListMeta['rel_tbl'])){
					$submitStr .= "新增，繼續處理 ";
					foreach ($yvtListMeta['rel_tbl'] as $fk => $fv) {
						if($submitStr_NEW){
							$submitStr .= "、";
						}
						$submitStr .= $fv['label']." ";
						$submitStr_NEW = true;
					}
				}
				?>
				<input type="submit" class="btn btn-primary" value="<?php echo($submitStr); ?>">
			</form>
		</section>
	</section>

	<script type="text/javascript">
	var qsi_cache_obj = {};
	var qsi_cache_idx = 0;
	function qSearchIpt(){

		var k_idx = null;

		if( typeof $(this).attr('qsi_cache_idx') == 'undefined' ){
			$(this).attr('qsi_cache_idx',qsi_cache_idx);
		}else{
			k_idx = $(this).attr('qsi_cache_idx');
		}

		var $par_div = $(this).parents('div.form-group');
		var $flt_sel = $par_div.find('select');	
		var $all_opts = $flt_sel.find('option');

		var sch_str = $(this).val();
		var sel_val = $flt_sel.val();

		if( k_idx==null || typeof qsi_cache_obj[k_idx] == 'undefined' ){
			var all_opts_objs = [];

			$all_opts.each(function(){
				all_opts_objs.push({'v':$(this).val(),'n':$(this).html()});
			});

			qsi_cache_obj[qsi_cache_idx] = all_opts_objs;
			k_idx = qsi_cache_idx;
			qsi_cache_idx++;
		}

		$all_opts.remove();

		$.each(qsi_cache_obj[k_idx],function(){
			if(this.n.search(sch_str) >= 0){
				var $newOpt = $('<option>');
				$newOpt.attr('value',this.v).html(this.n);
				$flt_sel.prepend($newOpt);
			}else if(this.v == sel_val){
				var $newOpt = $('<option>');
				$newOpt.attr('value',this.v).html(this.n);
				$flt_sel.prepend($newOpt);
			}
		});
	}
	$('document').ready(function(){
		$( ".slides-sortable" ).sortable({
			// placeholder: "ui-state-highlight",
			stop:function(event, ui){
	           console.log("stop: "+$(ui.item).attr('rel'));
			}
		});

		//HACK
		$('input.q-sch-ipt').on('change',qSearchIpt);
	});
	</script>

</body>

<?php include("source/footer.php"); ?>
