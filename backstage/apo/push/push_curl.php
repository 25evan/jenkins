<?php
require("push_config.php");

$apiKey = $config['androidKey'];
$iosPem = $config['iosPem'];
$iosPassphrass = $config['iosPassphrass'];
$projectUrl = $config['projectUrl'];
$distinctDeviceFlag = $config['distinctDeviceFlag'];

if(isset($_REQUEST['yvtset']) && $_REQUEST['yvtset'] != ''){
	$yvtset = $_REQUEST['yvtset'];
}else{
	$yvtset = null;
}
if(isset($_REQUEST['id']) && $_REQUEST['id'] != ''){
	$id = $_REQUEST['id'];
}else{
	$id = null;
}
// 緩衝時間
if(isset($_REQUEST['sleepTime']) && $_REQUEST['sleepTime'] != ''){
	$sleepTime = $_REQUEST['sleepTime'];
}else{
	$sleepTime = null;
}
// CURL等待時間(射後不理)
if(isset($_REQUEST['timeout']) && $_REQUEST['timeout'] != ''){
	$timeout = $_REQUEST['timeout'];
}else{
	$timeout = null;
}

if($yvtset != null && $id != null){
	require("../../yvtset/".$yvtset.".php");
	require("../sqldata.php");

	if(isset($yvtListMeta['push_device_sql_tbl_name']) === false){
		echo "push_device_sql_tbl_name not set";
		exit;
	}else if(file_exists("../../yvtset/".$yvtListMeta['push_device_sql_tbl_name'].".php") === false){
		echo "push_device_sql_tbl_name file not found";
		exit;
	}

	//load data before update
	$newsSelectSql = $dbConnect->prepare("SELECT * FROM `".$yvtListMeta['sql_tbl_name']."` WHERE `".$yvtListMeta['columns_idx']."` = ? AND `status` = 'PUBLISH' ;");
	$newsSelectSql->execute(array($id));
	$newsSelectDetail = $newsSelectSql->fetch(PDO::FETCH_ASSOC);
	if($newsSelectSql->rowCount() == 0){
		// do nothing
		echo "yvtset=".$yvtset.", id=".$id." not found or status is not publish";
		exit;
	}

	$messageCol = null;
	foreach ($yvtListMeta['columns'] as $key => $value) {
		if(isset($yvtListMeta['columns'][$key]['pushattr']) && $yvtListMeta['columns'][$key]['pushattr'] == 'message'){
			$messageCol = $key;
			break;
		}
	}
	if($messageCol === null){
		echo "push message not set";
		exit;
	}

	$message = $newsSelectDetail[$messageCol];
	$publish_date = date('Y-m-d H:i:s');

	$columns_idx = $yvtListMeta['columns_idx'];
	$push_log_sql_tbl_name = $yvtListMeta['push_log_sql_tbl_name'];
	$push_device_sql_tbl_name = $yvtListMeta['push_device_sql_tbl_name'];

	// 清除推播內容table設定
	unset($yvtListMeta);

	// device設定初始化
	require("../../yvtset/".$push_device_sql_tbl_name.".php");

	$tokenCol = null;
	foreach ($yvtListMeta['columns'] as $key => $value) {
		if(isset($yvtListMeta['columns'][$key]['pushattr']) && $yvtListMeta['columns'][$key]['pushattr'] == 'token'){
			$tokenCol = $key;
			break;
		}
	}
	if($tokenCol === null){
		echo "push token not set";
		exit;
	}

	$typeCol = null;
	foreach ($yvtListMeta['columns'] as $key => $value) {
		if(isset($yvtListMeta['columns'][$key]['pushattr']) && $yvtListMeta['columns'][$key]['pushattr'] == 'type'){
			$typeCol = $key;
			break;
		}
	}
	if($typeCol === null){
		echo "push type not set";
		exit;
	}

	if($distinctDeviceFlag){
		$devicesCountSelectSql = $dbConnect->prepare("SELECT COUNT(DISTINCT(`".$tokenCol."`)) AS count FROM `".$push_device_sql_tbl_name."` WHERE `".$tokenCol."` != 'IOS_APNS_UNAUTHORIZED' AND `".$tokenCol."` != '' ;");
	}else{
		$devicesCountSelectSql = $dbConnect->prepare("SELECT COUNT(*) AS count FROM `".$push_device_sql_tbl_name."` WHERE `".$tokenCol."` != 'IOS_APNS_UNAUTHORIZED' AND `".$tokenCol."` != '' ;");
	}
	$devicesCountSelectSql->execute();
	$devicesCountSelectDetail = $devicesCountSelectSql->fetch(PDO::FETCH_ASSOC);
	$totalCount = $devicesCountSelectDetail['count'];

	$newsLogSelectSql = $dbConnect->prepare("SELECT MAX(`".$push_log_sql_tbl_name."_endnum`) AS endnum FROM `".$push_log_sql_tbl_name."` WHERE `".$columns_idx."` = ? ;");
	$newsLogSelectSql->execute(array($id));
	$newsLogSelectDetail = $newsLogSelectSql->fetch(PDO::FETCH_ASSOC);

	if($newsLogSelectDetail['endnum'] == null){
		$startnum = 0;
		$endnum = 0;
	}else{
		$startnum = $newsLogSelectDetail['endnum'];
		$endnum = $newsLogSelectDetail['endnum'];
	}

 	$start_time = date('Y-m-d H:i:s');

 	if($distinctDeviceFlag){
		$devicesSelectSql = $dbConnect->prepare("SELECT DISTINCT(`".$tokenCol."`), `type` FROM `".$push_device_sql_tbl_name."` WHERE `".$typeCol."` != 'IOS_APNS_UNAUTHORIZED' AND `".$typeCol."` != '' LIMIT ".$startnum.", 700 ;");
 	}else{
		$devicesSelectSql = $dbConnect->prepare("SELECT `".$tokenCol."`, `type` FROM `".$push_device_sql_tbl_name."` WHERE `".$typeCol."` != 'IOS_APNS_UNAUTHORIZED' AND `".$typeCol."` != '' LIMIT ".$startnum.", 700 ;");
	}
	$devicesSelectSql->execute();
	$endnum += $devicesSelectSql->rowCount();
	$devicesSelectDetail = $devicesSelectSql->fetchAll(PDO::FETCH_ASSOC);

	$androidToken = array();
	$iosToken = array();

	foreach ($devicesSelectDetail as $key => $value) {
		if($devicesSelectDetail[$key][$typeCol] == 'android'){
			$androidToken[] = $devicesSelectDetail[$key][$tokenCol];
		}else if($devicesSelectDetail[$key][$typeCol] == 'ios'){
			$iosToken[] = $devicesSelectDetail[$key][$tokenCol];
		}
	}

	androidPush($apiKey, $androidToken, $message, $publish_date);

	iosPush($iosPassphrass, $iosPem, false, $iosToken, $message);

 	$end_time = date('Y-m-d H:i:s');

	$eventInsertSql = $dbConnect->prepare("INSERT INTO `".$push_log_sql_tbl_name."`(`".$columns_idx."`, `".$push_log_sql_tbl_name."_endnum`, `".$push_log_sql_tbl_name."_start_time`, `".$push_log_sql_tbl_name."_end_time`) VALUES(?, ?, ?, ?) ;");
	$result = $eventInsertSql->execute(array($id, $endnum, $start_time, $end_time));

	// 緩衝時間, default 20 sec
	if($sleepTime == null){
		$sleepTime = 20;
	}
	sleep($sleepTime);

	// CURL等待時間, default 60 sec
	if($timeout == null){
		$timeout = 60;
	}

	if($result && $endnum < $totalCount){

		$push_url = $projectUrl.'/backstage/apo/push/push_curl.php?yvtset='.$yvtset.'&id='.$id.'&sleepTime='.$sleepTime.'&timeout='.$timeout;
		$headers = array('Content-Type: application/json');
	 
	 	// Open connection
		$ch = curl_init();
		// Set the url, number of POST vars, POST data
		curl_setopt( $ch, CURLOPT_URL, $push_url );
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		// 不等待結果
		// curl_setopt($ch, CURLOPT_TIMEOUT_MS, 250);
		curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);

		// 發送的訊息內容轉成 JSON 格式
		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields) );
		$result = curl_exec($ch);
		curl_close($ch);

	}

}else{
	echo "yvtset or id not set";
}

function androidPush($apiKey, $deviceToken, $message, $publishDate){
	
	// Set POST variables
	$url = 'https://android.googleapis.com/gcm/send';

	$fields = array('registration_ids'  => $deviceToken,
                'data'              => array( 'message' => $message,
                                                'campaigndate' => $publishDate,
                                                'title' => $message,
                                                'description' => $message
                                            )
                );
 
	$headers = array('Content-Type: application/json',
                'Authorization: key='.$apiKey
                );
 
	// Open connection
	$ch = curl_init();
	// Set the url, number of POST vars, POST data
	curl_setopt( $ch, CURLOPT_URL, $url );
	curl_setopt( $ch, CURLOPT_POST, true );
	curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

	// 發送的訊息內容轉成 JSON 格式
	curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields) );
	$result = curl_exec($ch);
	curl_close($ch);
	// print_r($result);
	$deviceToken = implode(',', $deviceToken);
	if($result){
		//UPDATE
		echo('success');
		return "deviceToken: ".$deviceToken." success\n";
	}else{
		return "deviceToken: ".$deviceToken." failure\n";
	}
}

function iosPush($passphrase, $pemUrl, $sanbox, $deviceToken, $message){

	$ctx = stream_context_create();
	stream_context_set_option($ctx, 'ssl', 'local_cert', $pemUrl);
	stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

	// Open a connection to the APNS server
	if($sanbox){
		$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 5, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx); // 測試機
	}else{
		$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 5, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx); // 正式機
	}

	if (!$fp){
		// exit("Failed to connect: $err $errstr" . PHP_EOL);
		echo(0);
	}else{
	// echo 'Connected to APNS' . PHP_EOL;

		// Create the payload body
		$body['aps'] = array(
			'alert' => $message,
			'sound' => 'default'
			);

		// Encode the payload as JSON
		$payload = json_encode($body);

		$pushCount = 0;
		// Build the binary notification
		foreach ($deviceToken as $k => $v) {
			if(strlen($v) == 64){
				$msg = chr(0) . pack('n', 32) . pack('H*', $v) . pack('n', strlen($payload)) . $payload;
				$result = fwrite($fp, $msg, strlen($msg));
				$pushCount++;
			}
		}


		if (!$result)
			// echo 'Message not delivered' . PHP_EOL;
			echo(0);
		else
			// echo 'Message successfully delivered' . PHP_EOL;
			echo($pushCount);

		// Close the connection to the server
		fclose($fp);

		$deviceToken = implode(',', $deviceToken);
		if(!$result){
			return "deviceToken: ".$deviceToken." failure\n";
		}else{
			return "deviceToken: ".$deviceToken." success\n";
		}
	}
}

?>