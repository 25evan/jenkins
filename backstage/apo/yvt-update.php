<?php
// error_reporting(E_ALL | E_NOTICE); # do not show notices as library is php4 compatable
// ini_set('display_errors', true);
require('session.php');
require('yvtphotos.php');
echo "loading...";

//move to file ini
function keyStartWith($key,$str){
	$slen = strlen($str);
	if( substr($key, 0, $slen) == $str ){
		return true;
	}
	return false;
}

function keyStrRemove($key,$str){
	return substr($key, strlen($str));
}

function makeRelUpdate($yvtset, $parr, $orders = 1){
	global $dbConnect;
	require("../yvtset/".$yvtset.".php");

	$uno_src_dir = '../'.$yvtListMeta['src_dir'];
	$uno_id = $parr[ $yvtListMeta['columns_idx'] ];

	$yvtUpdateSQL_PRE = "UPDATE `".$yvtListMeta['sql_tbl_name']."` SET ";
	$yvtUpdateSQL_MID = "";
	$yvtUpdateSQL_FIN = " WHERE `".$yvtListMeta['columns_idx']."` = ?;";
	$yvtUpdateSQLpArr = array();

	$yvtUpdateSQL_NEW_FLAG = false;

	foreach($yvtListMeta['columns'] as $mtk => $mtv){				//make SQL
		if($mtk != $yvtListMeta['columns_idx']){
			//no url photo here
			if( isset($parr[$mtk])  ){
				if($yvtUpdateSQL_NEW_FLAG){
					$yvtUpdateSQL_MID .= ",";
				}
				$yvtUpdateSQL_MID .= " `".$mtk."` = ? ";
				
				if( $mtk == $yvtListMeta['columns_orders'] ){
					$yvtUpdateSQLpArr[] = $orders;
				}else{
					$yvtUpdateSQLpArr[] = $parr[$mtk];
				}
				$yvtUpdateSQL_NEW_FLAG = true;
			}
		}
	}

	$yvtUpdateSQLpArr[] = $parr[ $yvtListMeta['columns_idx'] ];		//idx column key

	$yvtUpdateSQL = $yvtUpdateSQL_PRE.$yvtUpdateSQL_MID.$yvtUpdateSQL_FIN;
// echo("<hr/>".$yvtUpdateSQL."<br/><pre>".print_r($yvtUpdateSQLpArr,true)."</pre><br/><pre style='color:blue;'>".print_r($parr,true)."</pre>");
	$relUpdStmt = $dbConnect->prepare($yvtUpdateSQL);
	$relUpdStmt->execute($yvtUpdateSQLpArr);
}

function makeRelInsert($yvtset, $parr, $orders = 1){
	$pavcount = 0;
	foreach ($parr as $pak => $pav) {
		if(strlen($pav) > 1 || is_numeric($pav)){
		// if(strlen($pav) > 1){
		// if(strlen($pav) > 0){	//HACK0109
			$pavcount++;
		// }else if($pak==='orders'){	//HACK0112
			// $pavcount++;
		}
	}

	// echo("<h4>".$yvtset." ".$orders." count:".$pavcount."</h4> <pre>".print_r($parr,true)."</pre>");

	if($pavcount > 1){

		global $dbConnect;
		require("../yvtset/".$yvtset.".php");

		$uno_src_dir = '../'.$yvtListMeta['src_dir'];
		$uno_id = $parr[ $yvtListMeta['columns_idx'] ];

		$yvtInsertSQL_PRE = "INSERT INTO `".$yvtListMeta['sql_tbl_name']."` (";
		$yvtInsertSQL_MID = ") VALUES (";
		$yvtInsertSQL_FIN = ");";
		$yvtInsertSQLpArr = array();

		$yvtInsertSQL_NEW_FLAG = false;

		foreach($yvtListMeta['fk_keys'] as $fkk => $fkv){
			if($fkv != $yvtListMeta['columns_idx']){
				if( isset($_POST[$fkv]) ){
					$yvtInsertSQL_PRE .= " `".$fkv."`, ";
					$yvtInsertSQL_MID .= " ?, ";

					$yvtInsertSQLpArr[] = $_POST[$fkv];
				}
			}
		}

		foreach($yvtListMeta['columns'] as $mtk => $mtv){
			if($mtk != $yvtListMeta['columns_idx']){
				if( isset($parr[$mtk]) || $mtv['edittype'] == "url_photo" ){
					if($yvtInsertSQL_NEW_FLAG){
						$yvtInsertSQL_PRE .= ",";
					}
					$yvtInsertSQL_PRE .= " `".$mtk."` ";

					if($yvtInsertSQL_NEW_FLAG){
						$yvtInsertSQL_MID .= ",";
					}
					$yvtInsertSQL_MID .= " ? ";

					if($mtv['edittype'] == "url_photo"){
						$yvtInsertSQLpArr[] = $uno_url_photo_name;	//for urls
					}else{
						$yvtInsertSQLpArr[] = $parr[$mtk];
					}

					$yvtInsertSQL_NEW_FLAG = true;

				}
			}
		}

		//sql merge
		$yvtInsertSQL = $yvtInsertSQL_PRE.$yvtInsertSQL_MID.$yvtInsertSQL_FIN;

		// echo("<h4>".$yvtInsertSQL." ".print_r($yvtInsertSQLpArr,true)."</h4>");
		//execute SQL
		$rupInsertSql = $dbConnect->prepare( $yvtInsertSQL );
		$rupInsertSql->execute($yvtInsertSQLpArr);
	}
}

function makeMulUpdate($yvtset, $allarr, $orders = 1){
	global $_POST;
	global $dbConnect;
	require("../yvtset/".$yvtset.".php");

	$uno_src_dir = '../'.$yvtListMeta['src_dir'];
	// $uno_id = $parr[ $yvtListMeta['columns_idx'] ];
	$mul_fk_key = "";	//like items_id
	$mul_fk_val = "";	//like items_id = "3"
	$mul_col_key = "";	//like tags_id
	$pre_sel_arr = array();

	//DELETE WHEN NULL
	if( is_array($allarr) ){
	// if( count($allarr) > 0 ){
		//pre select block ini
		$selMulpArr = array();
		$selMulSql_WHERE = "";
		$selMulSql_WHERE_NEW_FLAG = false;

		foreach( $yvtListMeta['fk_keys'] as $fkk => $fkv){
			if( isset($_POST[$fkv]) ){
				if($selMulSql_WHERE_NEW_FLAG){
					$selMulSql_WHERE .= " AND ";
				}
				$selMulSql_WHERE .= " `".$fkv."` = ? ";
				$mul_fk_key = $fkv;							//TODO check if problem

				$selMulSql_WHERE_NEW_FLAG = true;

				$selMulpArr[] = $_POST[$fkv];
				$mul_fk_val = $_POST[$fkv];					//TODO check if problem
			}else{
				$mul_col_key = $fkv;	//get key to update //TODO check if problem
				
			}
		}
		// echo("<h2>".$mul_col_key."</h2>");

		if($selMulSql_WHERE_NEW_FLAG){
			$selMulSql_WHERE = " WHERE ".$selMulSql_WHERE;
		}

		$selMulSql = 'SELECT * FROM `'.$yvtListMeta['sql_tbl_name'].'` '.$selMulSql_WHERE.' ;';
		// echo($selMulSql); print_r($selMulpArr); echo('<hr/>');
		$selMulStmt = $dbConnect->prepare($selMulSql);
		$selMulStmt->execute($selMulpArr);
		if($selMulStmt->rowCount() > 0){
			while( $selMulRow = $selMulStmt->fetch(PDO::FETCH_ASSOC) ){
				// echo('<p> presel sql : '.print_r($selMulRow,true).'</p>');
				if( $mul_col_key != "" )
					$pre_sel_arr[ $selMulRow[$mul_col_key] ] = $selMulRow;
			}
		}

		// echo('<h2> presel : '.print_r($pre_sel_arr,true).'</h2>');
		//pre select block end

		//start to insert
		foreach( $allarr as $allk => $unov){
			if( !isset($pre_sel_arr[$unov]) ){
				$mulInsSql = 'INSERT INTO `'.$yvtListMeta['sql_tbl_name'].'` ( `'.$mul_col_key.'`,`'.$mul_fk_key.'` ) VALUES ( ?, ? );';
				$mulInsPArr = array($unov,$mul_fk_val);

				$mulInsStmt = $dbConnect->prepare($mulInsSql);
				$mulInsStmt->execute($mulInsPArr);
				// echo('<h2>DO INS:'.$mulInsSql.'<br/>'.print_r($mulInsPArr,true).'</h2>');
			}else{
				// echo('<h2>NO INS</h2>');
			}
		}

		//start to del
		foreach( $pre_sel_arr as $pselk => $pselv){
			// echo("pselk:".$pselk." in ".print_r($allarr,true));
			if( in_array($pselk, $allarr) ){
				// echo('<h2>NO DEL</h2>');
			}else{
				$mulDelSql = 'DELETE FROM `'.$yvtListMeta['sql_tbl_name'].'` WHERE `'.$mul_col_key.'` = ? AND `'.$mul_fk_key.'` = ? ;';
				$mulDelPArr = array($pselk,$mul_fk_val);

				$mulDelStmt = $dbConnect->prepare($mulDelSql);
				$mulDelStmt->execute($mulDelPArr);

				// echo('<h2>DO DEL:'.$mulDelSql.'<br/>'.print_r($mulDelPArr,true).'</h2>');
			}
		}

		//update?
		// $updMulSql = 'UPDATE `'.$yvtListMeta['sql_tbl_name'].'` SET `'.$mul_col_key.'` = ? WHERE `'.$yvtListMeta['columns_idx'].'` = ? ;'
		// $updMulpArr = array($uv, $idx);
	}
}

function addRelUrlPhoto($yvtset, $fileInputName, $output_size = null){

	if( isset($yvtset) ){
		global $_POST;
		global $dbConnect;
		require("../yvtset/".$yvtset.".php");

		$uno_src_dir = '../'.$yvtListMeta['src_dir'];
	
		$photos_dir = $uno_src_dir;

		// if(strlen($_FILES[$fileInputName]['name'])>1){
			foreach ($_FILES[$fileInputName]['type'] as $fsnk => $fsnv) {

				if( isset($yvtListMeta['src_prefix']) ){
					$photos_name = uniqid($yvtListMeta['src_prefix']."_");
				}else{
					$photos_name = uniqid($yvtset."_");
				}

				if (isset( $_FILES[$fileInputName]['type'][$fsnk] )){

					if(is_uploaded_file( $_FILES[$fileInputName]['tmp_name'][$fsnk] )){
						$fileSource = $_FILES[$fileInputName];

						$fileExt = $fileSource['type'][$fsnk];
						if($fileExt == 'image/jpeg'){
							$fileType=".jpg";
							$src = imagecreatefromjpeg($fileSource['tmp_name'][$fsnk]);
						}elseif($fileExt == 'image/gif'){
							$fileType=".gif";
							$src = imagecreatefromgif($fileSource['tmp_name'][$fsnk]);
						}elseif($fileExt == 'image/png'){
							$fileType=".png";
							$src = imagecreatefrompng($fileSource['tmp_name'][$fsnk]);
						}else{
							continue;
							// return null;
						}

						$fileName = $photos_name.$fileType;
						$filePathName = $photos_dir."/".$fileName;

						//no override, all new photo

						//取得來源圖片長寬
						$src_w = imagesx($src);
						$src_h = imagesy($src);


						if($output_size == null){
							//設定圖長寬
							$thumb_w = $src_w;
							$thumb_h = $src_h;

							//建立儲存圖
							$thumb = imagecreatetruecolor($thumb_w, $thumb_h);
							imagealphablending($thumb, false);
							imagesavealpha($thumb, true);
							imagecopyresampled($thumb, $src, 0, 0, 0, 0, $thumb_w, $thumb_h, $src_w, $src_h);

							if($fileType == ".jpg"){
								imagejpeg($thumb, $filePathName, 100);
							}elseif($fileType == ".gif"){
								// imagegif($thumb, $filePathName, 100);
								copy($fileSource['tmp_name'][$fsnk],$filePathName);
							}elseif($fileType == ".png"){
								imagepng($thumb, $filePathName, 0);	// 0-9 range of png, 9 is maximum compression
							}
						}else{
							if( isset($output_size['width']) ){
								$output_w = $output_size['width'];
							}else{
								$output_w = $output_size['w'];
							}
							// $output_w = $output_size['width'];
							if(isset($output_size['height'])||isset($output_size['h'])){
								// $output_h = $output_size['height'];
								if( isset($output_size['height']) ){
									$output_h = $output_size['height'];
								}else{
									$output_h = $output_size['h'];
								}


								$src_ratio = $src_w / $src_h;
								$output_ratio = $output_w / $output_h;

								if ( $src_ratio > $output_ratio ){
									// Triggered when source image is wider
									$temp_height = $output_h;
									$temp_width = ( int ) ( $output_h * $src_ratio );
								} else {
									// Triggered otherwise (i.e. source image is similar or taller)
									$temp_width = $output_w;
									$temp_height = ( int ) ( $output_w / $src_ratio );
								}
							}else{
								$output_h = $output_w / $src_w * $src_h;
								$temp_height = $output_h;
								$temp_width = $output_w;
							}

							
							// Resize the image into a temporary GD image
							$temp_gdim = imagecreatetruecolor( $temp_width, $temp_height );
							imagecopyresampled(
								$temp_gdim,
								$src,
								0, 0,
								0, 0,
								$temp_width, $temp_height,
								$src_w, $src_h
							);

							// Copy cropped region from temporary image into the desired GD image
							$x0 = ( $temp_width - $output_w ) / 2;
							$y0 = ( $temp_height - $output_h ) / 2;

							$desired_gdim = imagecreatetruecolor( $output_w, $output_h );
							imagecopy(
								$desired_gdim,
								$temp_gdim,
								0, 0,
								$x0, $y0,
								$output_w, $output_h
							);

							if($fileType == ".jpg"){
								imagejpeg($desired_gdim, $filePathName, 100);
							}elseif($fileType == ".gif"){
								imagegif($desired_gdim, $filePathName, 100);
							}elseif($fileType == ".png"){
								imagepng($desired_gdim, $filePathName, 0);	// 0-9 range of png, 9 is maximum compression
							}
						}

						// echo("<h1>".$fileName."</h1>");

						$yvtInsertSQL_PRE = "INSERT INTO `".$yvtListMeta['sql_tbl_name']."` (";
						$yvtInsertSQL_MID = " `".$yvtListMeta['columns_orders']."` ) SELECT ";
						$yvtInsertSQL_MID_2 = " MAX(`".$yvtListMeta['columns_orders']."`)+1 FROM `".$yvtListMeta['sql_tbl_name']."` ";
						$yvtInsertSQL_WHERE = "";	//HACK0122 
						$yvtInsertSQL_FIN = " ;";
						//TODO WHERE FK = ?
						$yvtInsertSQLpArr = array();

						//INSERT INTO posts (post_user_id,gen_id) 
	  					//SELECT 1, MAX(gen_id)+1 FROM posts;
						foreach($yvtListMeta['columns'] as $mtk => $mtv){
							if($mtk != $yvtListMeta['columns_idx']){
								if( $mtv['edittype'] == "url_photo" ){
									$yvtInsertSQL_PRE .= " `".$mtk."`, ";
									$yvtInsertSQL_MID .= " ?, ";

									$yvtInsertSQLpArr[] = $fileName;	//for urls
								}
							}
						}

						foreach($yvtListMeta['fk_keys'] as $fkk => $fkv){
							if($fkv != $yvtListMeta['columns_idx']){
								if( isset($_POST[$fkv]) ){
									$yvtInsertSQL_PRE .= " `".$fkv."`, ";
									$yvtInsertSQL_MID .= " ?, ";
									$yvtInsertSQLpArr[] = $_POST[$fkv];

									//HACK0122 fix orders wrong
									$yvtInsertSQL_WHERE = " `".$fkv."` = ? ";
									$yvtInsertSQLpArr[] = $_POST[$fkv];
								}
							}
						}

						//HACK0122 
						if(strlen($yvtInsertSQL_WHERE)>0){
							$yvtInsertSQL_WHERE = " WHERE ".$yvtInsertSQL_WHERE;
						}

						//sql merge
						$yvtInsertSQL = $yvtInsertSQL_PRE.$yvtInsertSQL_MID.$yvtInsertSQL_MID_2.$yvtInsertSQL_WHERE.$yvtInsertSQL_FIN;//HACK0122 

						//execute SQL
						$rupInsertSql = $dbConnect->prepare( $yvtInsertSQL );
						$rupInsertSql->execute($yvtInsertSQLpArr);

						// echo("<h4>".$yvtInsertSQL." ".print_r($yvtInsertSQLpArr,true)."</h4>");
					}

				}
			}
		// }
	}
}
//move to file end

// by Evan
function addRelUrlYoutube($yvtset, $youtube_id){

	if( isset($yvtset) ){
		global $_POST;
		global $dbConnect;
		require("../yvtset/".$yvtset.".php");

		$uno_src_dir = '../'.$yvtListMeta['src_dir'];

		$photos_dir = $uno_src_dir;

		$yvtInsertSQL_PRE = "INSERT INTO `".$yvtListMeta['sql_tbl_name']."` (";
		$yvtInsertSQL_MID = " `".$yvtListMeta['columns_orders']."` ) SELECT ";
		$yvtInsertSQL_FIN = " MAX(`".$yvtListMeta['columns_orders']."`)+1 FROM `".$yvtListMeta['sql_tbl_name']."` ;";
		//TODO WHERE FK = ?
		$yvtInsertSQLpArr = array();

		//INSERT INTO posts (post_user_id,gen_id) 
			//SELECT 1, MAX(gen_id)+1 FROM posts;
		foreach($yvtListMeta['columns'] as $mtk => $mtv){
			if($mtk != $yvtListMeta['columns_idx']){
				if( $mtk == "youtube_id" ){
					$yvtInsertSQL_PRE .= " `".$mtk."`, ";
					$yvtInsertSQL_MID .= " ?, ";

					$yvtInsertSQLpArr[] = $youtube_id;	//for urls
				}
			}
		}

		foreach($yvtListMeta['fk_keys'] as $fkk => $fkv){
			if($fkv != $yvtListMeta['columns_idx']){
				if( isset($_POST[$fkv]) ){
					$yvtInsertSQL_PRE .= " `".$fkv."`, ";
					$yvtInsertSQL_MID .= " ?, ";

					$yvtInsertSQLpArr[] = $_POST[$fkv];
				}
			}
		}

		//sql merge
		$yvtInsertSQL = $yvtInsertSQL_PRE.$yvtInsertSQL_MID.$yvtInsertSQL_FIN;
		// print_r($yvtInsertSQL);
		// print_r($yvtInsertSQLpArr);

		//execute SQL
		$rupInsertSql = $dbConnect->prepare( $yvtInsertSQL );
		$rupInsertSql->execute($yvtInsertSQLpArr);

		// echo("<h4>".$yvtInsertSQL." ".print_r($yvtInsertSQLpArr,true)."</h4>");	
	}
}

function push($projectUrl, $yvtset, $id, $timeout = 60, $sleepTime = 20){
	$url = $projectUrl.'/backstage/apo/push/push_curl.php?yvtset='.$yvtset.'&id='.$id.'&sleepTime='.$sleepTime;

	$headers = array('Content-Type: application/json');

	// Open connection
	$ch = curl_init();
	// Set the url, number of POST vars, POST data
	curl_setopt( $ch, CURLOPT_URL, $url );
	curl_setopt( $ch, CURLOPT_POST, true );
	curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	// 不等待結果
	curl_setopt( $ch, CURLOPT_TIMEOUT, $timeout );

	// 發送的訊息內容轉成 JSON 格式
	// curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields) );
	$result = curl_exec($ch);
	curl_close($ch);
}

//defaults
$uno_src_dir = '../../photos';
$uno_id = 0;
$uno_url_photo_name = "yvtdef";
// push curl
include("push/push_config.php");
$projectUrl = $config['projectUrl'];

// echo('<pre style="color:green">'.print_r($_POST,true).'</pre><hr/>');
// print_r($_FILES);
// echo('<hr/>');

if (isset($_POST['yvtset'])){

	require('sqldata.php');

	if(isset($_POST['yvtset'])){
		$yvtset = $_POST['yvtset'];
	}
	require("../yvtset/".$yvtset.".php");

	//photo path
	$uno_src_dir = '../'.$yvtListMeta['src_dir'];
	$uno_id = $_POST[ $yvtListMeta['columns_idx'] ];

	//if url_photo
	// if(isset($_FILES['upload-photos-url'])){ CHK }

	$yvtUpdateSQL_PRE = "UPDATE `".$yvtListMeta['sql_tbl_name']."` SET ";
	$yvtUpdateSQL_MID = "";
	$yvtUpdateSQL_FIN = " WHERE `".$yvtListMeta['columns_idx']."` = ?;";
	$yvtUpdateSQLpArr = array();

	$yvtUpdateSQL_NEW_FLAG = false;

	foreach($yvtListMeta['columns'] as $mtk => $mtv){
		if($mtk != $yvtListMeta['columns_idx']){
			//HACK 0108 DEAL url_photo
			if( isset($_POST[$mtk]) ){

				if($yvtUpdateSQL_NEW_FLAG){
					$yvtUpdateSQL_MID .= ",";
				}
				$yvtUpdateSQL_MID .= " `".$mtk."` = ? ";
				$yvtUpdateSQLpArr[] = $_POST[$mtk];

				$yvtUpdateSQL_NEW_FLAG = true;
			}
			/*
			if( isset($_POST[$mtk]) || $mtv['edittype'] == "url_photo" ){

				if($yvtUpdateSQL_NEW_FLAG){
					$yvtUpdateSQL_MID .= ",";
				}
				$yvtUpdateSQL_MID .= " `".$mtk."` = ? ";

				if($mtv['edittype'] == "url_photo"){
					$yvtUpdateSQLpArr[] = $uno_url_photo_name;	//for urls
				}else{
					$yvtUpdateSQLpArr[] = $_POST[$mtk];
				}

				$yvtUpdateSQL_NEW_FLAG = true;
			}
			*/
		}
	}

	//0122 checkbox
	foreach($yvtUpdateSQLpArr as $pak => $pav){
		if(is_array($pav)){
			$yvtUpdateSQLpArr[$pak]= implode(",", $pav);
		}
	}

	$yvtUpdateSQLpArr[] = $_POST[ $yvtListMeta['columns_idx'] ];
	$yvtUpdateSQL = $yvtUpdateSQL_PRE.$yvtUpdateSQL_MID.$yvtUpdateSQL_FIN;
	// echo($yvtUpdateSQL.'<br/><code>'.print_r($yvtUpdateSQLpArr,true).'</code>');

	$unoUpdateStmt = $dbConnect->prepare( $yvtUpdateSQL );
	if($unoUpdateStmt->execute($yvtUpdateSQLpArr) ){
		//修改成功
		$jsonArray['status'] = true;
		$jsonArray['errcode'] = '97';
		$jsonArray['errmsg'] = $uno_id;

		$REL_POST_ARR = array();
		$MUL_POST_ARR = array();

		//rel ini
		if( isset($yvtListMeta['rel_tbl']) ) {

			foreach ($yvtListMeta['rel_tbl'] as $rtk => $rtv) {
				$prefix_rel = "YVTREL_".$rtk."_";
				$prefix_mul = "YVTMUL_".$rtk."_";
				$prefix_rel_add = "YVTRELADD_".$rtk."_";

				// echo("
				// 	<h3>".$rtk." => ".$prefix_rel." \ ".$prefix_mul."</h3>
				// 	");

				// $yvtListMeta in not in scope

				//POST ini
				$REL_POST = array();
				$MUL_POST = array();
				$REL_ADD_POST = array();
				foreach ($_POST as $postk => $postv) {
					if( keyStartWith($postk,$prefix_rel) ){ //DEBUG echo("<h5>".$postk."</h5><h6>".print_r($postv,true)."</h6>");
						foreach ($postv as $pvk => $pvv) {
							$REL_POST[$pvk][keyStrRemove($postk,$prefix_rel)] = $pvv;
						}
					}else if( keyStartWith($postk,$prefix_mul) ){
						//TODO mul
						foreach ($postv as $pvk => $pvv) {
							$MUL_POST[$pvk] = $pvv;	//single layer
						}
					}else if( keyStartWith($postk,$prefix_rel_add) ){ //DEBUG echo("<h5>".$rtk." ".$prefix_rel_add." <= ".$postk."</h5><h6>".print_r($postv,true)."</h6>");
						foreach ($postv as $pvk => $pvv) {
							$REL_ADD_POST[$pvk][keyStrRemove($postk,$prefix_rel_add)] = $pvv;
						}
					}
				}

				$REL_POST_ARR[$rtk] = $REL_POST;
				$MUL_POST_ARR[$rtk] = $MUL_POST;
				if( count($REL_ADD_POST) > 0 ){
					$REL_ADD_POST_ARR[$rtk] = $REL_ADD_POST;
				}
				//POST end

				//FILES ini
				if( isset($_FILES[$prefix_rel.'upload-photos-url']) ){
					//DO ADD
					// addRelUrlPhoto($rtk,$prefix_rel.'upload-photos-url');
					if(isset($rtv['photo_sizes'])){
						addRelUrlPhoto($rtk,$prefix_rel.'upload-photos-url', $rtv['photo_sizes']);
					}else{
						addRelUrlPhoto($rtk,$prefix_rel.'upload-photos-url');
					}
				}
				// by Evan
				if( $rtk == 'items_youtube' && isset($_POST[$prefix_rel.'youtube-id']) && isset($_POST[$prefix_rel.'youtube-title']) && ($_POST[$prefix_rel.'youtube-id'] != null || $_POST[$prefix_rel.'youtube-title'] != null) ){
					addRelUrlYoutube('items_youtube', $_POST[$prefix_rel.'youtube-id']);
				}
			}
		}

		// echo('<hr/><pre style="color:blue;">'.print_r($REL_POST_ARR,true).'</pre>');
		// echo('<hr/><pre style="color:green;">'.print_r($MUL_POST_ARR,true).'</pre>');
		// echo('<hr/><pre style="color:red;">'.print_r($REL_ADD_POST_ARR,true).'</pre>');

		//TODO cmp DELETE
		foreach( $REL_POST_ARR as $sk => $svArr ){
			foreach($svArr as $eachk => $eachv){
				makeRelUpdate($sk,$eachv,$eachk);
			}
		}

		foreach( $MUL_POST_ARR as $sk => $svArr ){
			//TODO makeRelUpdate 應該也要改成全傳
			makeMulUpdate($sk,$svArr);
		}

		if( isset($REL_ADD_POST_ARR) ) {
			//HACK 0108
			foreach( $REL_ADD_POST_ARR as $sk => $svArr ){
				if( is_array($svArr) ){
					foreach($svArr as $eachk => $eachv){
						makeRelInsert($sk,$eachv,$eachk);
					}
				}
			}
		}
		//rel end

		//關聯圖片
		// 存圖
		if(isset($_FILES['upload-photos-idx'])){
			// saveUnoPhotos('upload-photos-idx', $uno_src_dir, $uno_id, true);

			//HACK1230
			$uploadFlag = true;
			if(isset($_POST['upload-photos-idx-default'])){
				if($_POST['upload-photos-idx-default'] == 'default'){
					foreach($yvtListMeta['columns'] as $mtk => $mtv){
						if($mtv['edittype'] == 'idx_photo'){
							if( isset($mtv['defaultvalue'])){
								if(file_exists('../'.$mtv['defaultvalue'])){
									copy( '../'.$mtv['defaultvalue'], $uno_src_dir."/".$uno_id.'.jpg');
									$uploadFlag = false;
									//saveCustomizedPhotos('upload-photos-idx', $uno_src_dir, $uno_id, true, $uno_output_size);
								}
							}
						}
					}
				}
			}

			if(is_uploaded_file( $_FILES['upload-photos-idx']['tmp_name'] )){
				$fin_output_size = null;
				$idxPhotoMeta = null;
				foreach($yvtListMeta['columns'] as $mtk => $mtv){
					if( isset($mtv['edittype']) ){
						if( $mtv['edittype']=="idx_photo" ){
							$idxPhotoMeta = $mtv;
						}
					}
				}

				if(isset($idxPhotoMeta['photo_sizes'])){
					$fin_output_size = $idxPhotoMeta['photo_sizes'];
				}

				$fin_output_type = ( isset($idxPhotoMeta['photo_type']) ) ? $idxPhotoMeta['photo_type'] : null;

				if($uploadFlag){
					saveCustomizedPhotos('upload-photos-idx', $uno_src_dir, $uno_id, true, $fin_output_size, $fin_output_type);
				}
			}
		}

		//HACK 1018 url_photo
		if(isset($_FILES['upload-photos-url'])){
			// print_r($_FILES['upload-photos-url']);
			if(is_uploaded_file( $_FILES['upload-photos-url']['tmp_name'] )){
				if( isset($yvtListMeta['src_prefix']) ){
					$rnd_url_photo_name = uniqid($yvtListMeta['src_prefix']."_");
				}else{
					$rnd_url_photo_name = uniqid($yvtset."_");
				}

				$urlPhotoMeta = null;
				$fin_output_size = null;
				$cut_info = "";
				foreach($yvtListMeta['columns'] as $mtk => $mtv){
					if( isset($mtv['edittype']) ){
						if( $mtv['edittype']=="url_photo" ){
							$urlPhotoMeta = $mtv;
						}
					}
				}
				if(isset($urlPhotoMeta['photo_sizes'])){
					$fin_output_size = $urlPhotoMeta['photo_sizes'];
				}
				// $uno_url_photo_name = saveUnoPhotos('upload-photos-url', $uno_src_dir, $rnd_url_photo_name);
				$uno_url_photo_name = saveCustomizedPhotos('upload-photos-url', $uno_src_dir, $rnd_url_photo_name, true, $fin_output_size);

				foreach($yvtListMeta['columns'] as $mtk => $mtv){
					if( $mtv['edittype'] == "url_photo" ){
						if($mtk != $yvtListMeta['columns_idx']){
							$deleteUrlPhotoSql = $dbConnect -> prepare( "SELECT `".$mtk."` FROM `".$yvtListMeta['sql_tbl_name']."` WHERE `".$yvtListMeta['columns_idx']."` = ?;" );
							$deleteUrlPhotoSql -> execute( array($_POST[$yvtListMeta['columns_idx']]) );
							$deleteUrlPhotoDetail = $deleteUrlPhotoSql -> fetch(PDO::FETCH_ASSOC);
							$deletePhotoSrc = "../".$yvtListMeta['src_dir']."/".$deleteUrlPhotoDetail[$mtk];
							if( file_exists( $deletePhotoSrc ) )
								unlink( $deletePhotoSrc );

							$urlPhotoUpdateStm = $dbConnect->prepare( "UPDATE `".$yvtListMeta['sql_tbl_name']."` SET `".$mtk."` = ? WHERE `".$yvtListMeta['columns_idx']."` = ?;" );
							$urlPhotoUpdateStm->execute( array($uno_url_photo_name, $_POST[$yvtListMeta['columns_idx']]) );
						}

					}
				}
			}
		}

		if($jsonArray['status'] && is_numeric($uno_id)){
			if(isset($yvtListMeta['admin_log'])){
				// print_r($yvtListMeta['admin_log']);
				if(is_array($yvtListMeta['admin_log'])){
					$updSql_PRE = "UPDATE `".$yvtListMeta['sql_tbl_name']."` SET ";
					$updSql_MID = "";
					$updSql_FIN = "WHERE `".$yvtListMeta['columns_idx']."` = ?; ";
					$updPArr = array();
					foreach($yvtListMeta['admin_log'] as $auk => $auv){
						if(strlen($updSql_MID)>0){
							$updSql_MID .= ", ";
						}
						$updSql_MID .= " `".$auk."`=? ";

						if($auv=="TIMESTAMP"){
							$updPArr[] = date("Y-m-d H:i:s");
						}else if(isset($_SESSION[$auv])){
							$updPArr[] = $_SESSION[$auv];
						}else{
							$updPArr[] = $_SESSION[$auv];
						}
					}
					if(count($updPArr)>0){
						$updPArr[] = $uno_id;
						$updSql = $updSql_PRE.$updSql_MID.$updSql_FIN;

						$uaUpdStm = $dbConnect->prepare( $updSql );
						$uaUpdStm->execute( $updPArr );
						// echo($updSql_PRE.$updSql_MID.$updSql_FIN);
						// print_r($updPArr);
					}
				}
				// UPDATE `peoples` SET `updadm_id`=[value-11],`updadm_time`=[value-12]  ``
			}
		}

		// 推播公告 by Evan
		if(isset($yvtListMeta['push_table']) && $yvtListMeta['push_table']){
			if(isset($_POST['status']) && $_POST['status'] == 'PUBLISH'){
				push($projectUrl, $yvtset, $uno_id);
			}
		}

	}else{
		//sql error
		$jsonArray['errcode'] = '105';
		$jsonArray['errmsg'] = 'sql fail';
	}
}else{
	// post error
	$jsonArray['errcode'] = '103';
	$jsonArray['errmsg'] = 'post fail';
}
echo "<script language=javascript>
		setTimeout(function(){
			window.location.replace(\"../yvt-modify.php?yvtset=".$yvtset."&id=".$uno_id."\");
			top.leftFrame.location.reload();
		}, 50);
	</script>";
?>
