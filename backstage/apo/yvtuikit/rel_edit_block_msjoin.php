<?php
// require('session.php');

global $dbConnect;
require_once("yvtset/".$yvtset.".php");

$gk = $rparr['idx'];

//TODO chk if isset $yvtListMeta['columns_orders']

echo('
		<section class="panel" yvtnavi="'.$yvtset.'">
			<header class="panel-heading">
				<h3>'.$rparr['label'].'</h3>
			</header>
			<input name="rel_tbl_meta[]" type="hidden" value="'.$yvtset.'" />
			<section class="panel">
				<header class="panel-heading">
					編輯相關的'.$rparr['label'].'
				</header>
				
				<div class="panel-body">
					<div class="form-group">
						<select name="YVTMUL_'.$yvtset.'_'.$yvtListMeta['sql_tbl_name'].'[]" class="multi-select" multiple="" id="yvt_multisel_'.$yvtListMeta['sql_tbl_name'].'" >
	');





$join_key = "";
$join_key_array = array();
foreach($yvtListMeta['fk_keys'] as $fkk => $fkv){
	if( $fkv != $gk ){
		$join_key = $fkv;
	}
}


//make SQL
$listSqlStr_PRE = "SELECT * FROM `".$yvtListMeta['sql_tbl_name']."` ";
$listSqlStr_MID = "";
$listSqlStr_FIN = "ORDER BY `".$yvtListMeta['columns_idx']."` DESC ;";		//orders asc

$listSqlpArr = array();

//foreign keys
if( isset($yvtListMeta['fk_keys']) ){
	$whereSqlStr_MID = "";
	$whereSqlStr_NEW = false;
	// foreach ($_GET as $gk => $gv) {
		if( in_array($gk, $yvtListMeta['fk_keys']) ){

			// if($yvtInsertSQL_NEW_FLAG){	$whereSqlStr_MID .= ",";	}

			$whereSqlStr_MID .= ' `'.$gk.'` = ? ';
			$whereSqlStr_NEW = true;
			$listSqlpArr[] = $gv;
		}
	// }

	if( strlen($whereSqlStr_MID) > 0 ){
		$listSqlStr_MID = " WHERE ".$whereSqlStr_MID;
	}
}

$listSqlStr = $listSqlStr_PRE.$listSqlStr_MID.$listSqlStr_FIN;

//sql execute
$listSelectSql = $dbConnect->prepare( $listSqlStr );
$listSelectSql->execute( $listSqlpArr );

if($listSelectSql->rowCount() > 0){
	while($listSelectRow = $listSelectSql->fetch(PDO::FETCH_ASSOC)){
		if(isset($listSelectRow[$join_key])){
			$join_key_array[] = $listSelectRow[$join_key];
		}
	}
}

// print_r($join_key_array);


$mtv = $yvtListMeta['columns'][$join_key];
$selk = "ID";
$selv = "text";
foreach( $mtv['listmapping'] as $k => $v ){
	$selk = $k;
	$selv = $v;
}

$yvtOptArr = array();

foreach($mtv['editarray'] as $jk => $jv){
	$yvtOptArr[] = array('v'=>$jv[$selk],'t'=>'('.$jv[$selk].') '.$jv[$selv]);
	if( in_array($jv[$selk], $join_key_array) ){
		echo('
							<option value="'.$jv[$selk].'" selected="selected" >('.$jv[$selk].') '.$jv[$selv].'</option>
			');
	}
}
echo('
						</select>
					</div>
					<script type="text/javascript">
					$("document").ready(function(){
						$("#yvt_multisel_'.$yvtListMeta['sql_tbl_name'].'").yvetteMultiSelect({
							data_array : '.json_encode($yvtOptArr).'
						});
					});
					</script>
				</div>
			</section>
		</section>
	');
// echo( "<h4>".$listSqlStr." ".print_r($join_key_array,true)."</h4>" );
?>