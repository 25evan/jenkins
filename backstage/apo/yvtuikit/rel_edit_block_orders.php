<?php
// require('session.php');

// $gk is key of FK
// $gv is id of FK

global $dbConnect;
require_once("yvtset/".$yvtset.".php");

$gk = $rparr['idx'];

	//TODO chk if isset $yvtListMeta['columns_orders']
if( isset($yvtListMeta['columns_orders']) ){
	//init var
	$idx_photo_flag = false;
	$url_photo_flag = false;
	$url_photo_colname = "";

	//TODO 整合？
	//check meta
	foreach($yvtListMeta['columns'] as $mtk => $mtv){
		if( $mtv['edittype']=="url_photo" ){
			$url_photo_flag = true;
			$url_photo_colname = $mtk;
		}
		// by Evan
		if( $mtk=="youtube_id" ){
			$url_youtube_flag = true;
			$url_youtube_colname = $mtk;
		}
	}

	//make SQL
	$listSqlStr_PRE = "SELECT * FROM `".$yvtListMeta['sql_tbl_name']."` ";
	$listSqlStr_MID = "";
	$listSqlStr_FIN = "ORDER BY `".$yvtListMeta['columns_orders']."` ASC ;";		//orders asc

	$listSqlpArr = array();

	//for fast add
	// $addQsParam = "yvtset=".$yvtset;

	//foreign keys
	if( isset($yvtListMeta['fk_keys']) ){
		$whereSqlStr_MID = "";
		$whereSqlStr_NEW = false;
		// foreach ($_GET as $gk => $gv) {
			if( in_array($gk, $yvtListMeta['fk_keys']) ){

				//HACK 0120
				// if($yvtInsertSQL_NEW_FLAG){	$whereSqlStr_MID .= ",";	}

				$whereSqlStr_MID .= ' `'.$gk.'` = ? ';
				$whereSqlStr_NEW = true;
				$listSqlpArr[] = $gv;

				// $addQsParam .= "&".$gk."=".$gv;
			}
		// }

		if( strlen($whereSqlStr_MID) > 0 ){
			$listSqlStr_MID = " WHERE ".$whereSqlStr_MID;
		}
	}

	$listSqlStr = $listSqlStr_PRE.$listSqlStr_MID.$listSqlStr_FIN;

	echo('
			<section class="panel" yvtnavi="'.$yvtset.'">
				<header class="panel-heading">
					<h4>'.$rparr['label'].'</h4>
				</header>
				<input name="rel_tbl_meta[]" type="hidden" value="'.$yvtset.'" />
		');

	if($url_photo_flag){

		$url_photo_input_name = "YVTREL_".$yvtset."_".'upload-photos-url[]';

		if( isset($yvtListMeta['photo_sizes']['w']) && isset($yvtListMeta['photo_sizes']['h']) )
			$photo_size_note = '<span class="size">尺寸：'.$yvtListMeta['photo_sizes']['w'].'px 寬 * '.$yvtListMeta['photo_sizes']['h'].'px 高</span>';
		else
			$photo_size_note = null;

		echo('
				<section class="panel">
					<header class="panel-heading">
						上傳 新的'.$yvtListMeta['labelname'].$photo_size_note.'
					</header>
					
					<div class="panel-body">
						<div class="form-group">
							<!-- <label class="control-label col-md-2">上傳</label> -->
							<div class="col-md-4">
								<input name="'.$url_photo_input_name.'" type="file" multiple class="form-control">
								<br/>
							</div>
							<div class="col-md-4">
								<button class="btn btn-primary" type="submit" btn-done="完成">
									<i class="icon-upload icon-white"></i> 
									上傳（更新）
								</button>
							</div>
						</div>
					</div>
				</section>
		');
		//<input name="tabid" type="hidden" value="'.$gv.'" />
		//<input name="width" type="hidden" value="'.1440.'"/>
	}else{

		//HACK new GenAdd
		echo('
			<section class="panel">
				<header class="panel-heading">
					新增 新的'.$yvtListMeta['labelname'].'
				</header>
				
				<div class="panel-body">
					<div class="">
			');
		// <div class="form-group">
		foreach($yvtListMeta['columns'] as $mtk => $mtv){
			$new_ipt_name = "YVTRELADD_".$yvtset."_".$mtk."[]";
			if( isset($mtv['edittype']) && !in_array($mtk, $yvtListMeta['fk_keys']) ){
				if( $mtv['edittype']=="input" ){
					//INPUT
	// 				echo('
	// <div class="form-group">
	// 	<label class="control-label col-md-2">'.$mtv['label'].'</label>
	// 	<div class="col-md-8">
	// 		<input size="16" style="float:left;" type="text" name="'.$mtk.'" class="form-control" ');
					// by Evan editclass
					if( isset($mtv['editclass']) ){
						$editClass = $mtv['editclass'];
					}else{
						$editClass = "";
					}


					echo('
						<div class="form-group">
							<label class="control-label col-md-2">'.$mtv['label'].'</label>
							<div class="col-md-8">
								<input size="16" style="float:left;" type="text" name="'.$new_ipt_name.'" class="form-control '.$editClass.'" ');
					if( isset($fk_defaults[$new_ipt_name]) ){
						echo(' value="'.$fk_defaults[$new_ipt_name].'" ');
					}else if( isset($mtv['defaultvalue']) ){
						echo(' value="'.$mtv['defaultvalue'].'" ');
					}
					if( isset($mtv['placeholder']) ){
						echo(' placeholder="'.$mtv['placeholder'].'" ');
					}
					echo(' >
							</div>
						</div>
							');
				}else if( $mtv['edittype']=="select" ){

					if(count($mtv['editarray'])>50){
						$bigCountFlag = true;
						$lSelectClass = 'col-md-5';
					}else{
						$bigCountFlag = false;
						$lSelectClass = 'col-md-8';
					}



					//SELECT
					echo('
						<div class="form-group">
							<label class="control-label col-md-2">'.$mtv['label'].'</label>
							<div class="'.$lSelectClass.'">
								<select class="form-control m-bot15 wf200" name="'.$new_ipt_name.'">
									<option value="">請選擇</option>');
					foreach ($mtv['editarray'] as $ek => $ev) {
						//<option value="'.$ev['ID'].'">'.$ev['text'].print_r($mtv['listmapping'],true).'</option>
						$selk = "ID";
						$selv = "text";
						if( isset($mtv['listmapping']) ){
							foreach( $mtv['listmapping'] as $k => $v ){
								$selk = $k;
								$selv = $v;
							}
						}

						echo('
									<option value="'.$ev[$selk].'">'.$ev[$selv].'</option>
							');
					}
					echo('</select>
							</div>
							');

					if($bigCountFlag){
						echo('
							<div class="col-md-3">
								<div class="input-group">
	  									<input type="text" class="form-control q-sch-ipt" placeholder="快速篩選" />
	  									<div class="input-group-addon">
	  										<i class="fa fa-search"></i>
	  									</div>
									</div>
							</div>
						');
					}
					echo('

						</div>
						');
				}else if( $mtv['edittype']=="textarea" ){
					//TEXTAREA
					echo('
						<div class="form-group">
							<label class="control-label col-md-2">'.$mtv['label'].'</label>
							<div class="col-md-8">
								<textarea class="form-control" name="'.$new_ipt_name.'" ');
					if( isset($mtv['placeholder']) ){
						echo(' placeholder="'.$mtv['placeholder'].'" ');
					}
					echo(' >');
					if( isset($mtv['defaultvalue']) ){
						echo($mtv['defaultvalue']);
					}
					echo('</textarea>
							</div>
						</div>');
					
				}
			}
		}
		echo('
						<div class="col-md-4">
							<button class="btn btn-primary" type="submit" btn-done="完成">
								<i class="icon-upload icon-white"></i> 
								新增（更新）
							</button>
						</div>
					</div>
				</div>
			</section>
		');
	}

	/*
	// by Evan
	if($url_youtube_flag){

		$url_youtube_input_name = "YVTREL_".$yvtset."_".'youtube-id';
		$url_youtube_input_name_title = "YVTREL_".$yvtset."_".'youtube-title';
		echo('
				<section class="panel">
					<header class="panel-heading">
						輸入 新的'.$yvtListMeta['labelname'].' id</span>
					</header>
					
					<div class="panel-body">
						<div class="form-group">
							<label class="control-label col-md-2">影片標題</label>
							<div class="col-md-4">
								<input size="16" style="float:left;" type="text" name="'.$url_youtube_input_name_title.'" class="form-control" value="">
								<br/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">Youtube ID</label>
							<div class="col-md-4">
								<input size="16" style="float:left;" type="text" name="'.$url_youtube_input_name.'" class="form-control" value="">
								<br/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-4">
								<button class="btn btn-primary" type="submit" btn-done="完成">
									<i class="icon-upload icon-white"></i> 輸入（更新）
								</button>
							</div>
						</div>
					</div>
				</section>
		');
		//<input name="tabid" type="hidden" value="'.$gv.'" />
		//<input name="width" type="hidden" value="'.1440.'"/>
	}
	*/

	
	//'.print_r($yvtListMeta,true).'

	//sql execute
	$listSelectSql = $dbConnect->prepare( $listSqlStr );
	$listSelectSql->execute( $listSqlpArr );

	if($listSelectSql->rowCount() > 0){

		echo('
			<section class="panel">
				<header class="panel-heading">
					編輯原有的 '.$yvtListMeta['labelname'].' (您可以移動順序)
				</header>
				<div class="panel-body">
					<div class="form-group">
						<ul class="slides-sortable">
		');




		while($listSelectRow = $listSelectSql->fetch(PDO::FETCH_ASSOC)){
			echo('
				<div class="bs-callout bs-callout-warning">
					<li id="slide-'.$listSelectRow[$yvtListMeta['columns_idx']].'" rel="'.$listSelectRow['orders'].'>">
						<h3>'.$listSelectRow['orders'].'</h3>');
			//if url photo
			if( $url_photo_flag ){
				$img_src = $yvtListMeta['src_dir'].'/'.$listSelectRow[$url_photo_colname];
				if( file_exists($img_src)){
					$img_block = '<img class="preview-image" src="'.$img_src.'" />';
				}else{
					$img_block = "<p>圖片不見了</p>";
				}
				echo('
						<div class="slides-img-wrapper">
							'.$img_block.'
						</div>
				');
			}
			echo('
						<div class="slides-content-wrapper">
							<table>
				');
			//TODO 換成 $yvtListMeta['columns']?
			foreach($yvtListMeta['columns'] as $mtk => $mtv){

				if( isset($listSelectRow[$mtk]) ){
					$rv = $listSelectRow[$mtk];
				}else{
					$rv = null;
				}

				$ipt_name = "YVTREL_".$yvtset."_".$mtk."[]";

				if($mtk == $yvtListMeta['columns_idx']){
					echo('
					<input type="hidden" name="'.$ipt_name.'" value="'.$rv.'" />
						');
				}else if($mtk == $yvtListMeta['columns_orders']){
					echo('
					<input type="hidden" name="'.$ipt_name.'" value="'.$rv.'" />
						');
				}else if($mtk == $gk){	//FK, not show
					echo('
					<input type="hidden" name="'.$ipt_name.'" value="'.$rv.'" />
						');
				}else{
					if( $yvtListMeta['columns'][$mtk]['edittype'] == 'input' ){
						echo('
					<tr>
						<td>'.$mtv['label'].'</td>
						<td>
							<input size="16" class="form-control" type="text" name="'.$ipt_name.'"');
						if( isset($mtv['placeholder']) ){
							echo(' placeholder="'.$mtv['placeholder'].'" ');
						}
						echo(' value="'.$rv.'" />
						</td>
					</tr>
							');
					}else if( $yvtListMeta['columns'][$mtk]['edittype'] == 'select' ){
						echo('
					<tr>
						<td>'.$mtv['label'].'</td>
						<td>
							<select class="form-control m-bot15 wf200" name="'.$ipt_name.'">');
						foreach ($mtv['editarray'] as $ek => $ev) {
							$selk = "ID";
							$selv = "text";
							if( isset($mtv['listmapping']) ){
								foreach( $mtv['listmapping'] as $k => $v ){
									$selk = $k;
									$selv = $v;
								}
							}
							echo('
									<option value="'.$ev[$selk].'"');

							if($ev[$selk] == $listSelectRow[$mtk]){	//$rv
								echo(' selected="selected" ');
							}

							echo('>'.$ev[$selv].'</option>
								');
						}
						echo('</select>
						</td>
					</tr>
							');
					}else if( $yvtListMeta['columns'][$mtk]['edittype'] == 'textarea' ){
						echo('
					<tr>
						<td>'.$mtv['label'].'</td>
						<td>
							<textarea class="form-control" name="'.$ipt_name.'"');
						if( isset($mtv['placeholder']) ){
							echo(' placeholder="'.$mtv['placeholder'].'" ');
						}
						echo('>'.$rv.'</textarea>
						</td>
					</tr>
							');
					}else if( $yvtListMeta['columns'][$mtk]['edittype'] == 'url_photo' ){
						//nothing
					}else{
						echo('
					<tr>
						<td>'.$mtv['label'].'</td>
						<td>	
						'.$rv.'
						</td>
					</tr>
							');
					}
				}
			}
			echo('
							</table>
							<hr/>
							<a href="apo/yvt-delete.php?yvtset='.$yvtset.'&id='.$listSelectRow[$yvtListMeta['columns_idx']].'" class="btn btn-small btn-danger">
								<i class="icon-trash icon-white"></i>
								刪除
							</a>
						</div>
					</li>
				</div>
				');
		}
		echo('
							</ul>
						</div>
					</div>
				</section>
		</section>
		');
	}
	
	// echo( "<h4>".$listSqlStr."</h4>" );
}else{
	echo('
		<section class="panel">
			<header class="panel-heading">
				<h3>'.$yvtListMeta['labelname'].' 這個關聯表似乎不支援排序喔</h3>
			</header>
		</section>
		');
}
?>