<?php
require('session.php');

if (isset($_GET['yvtset'])){
	require('sqldata.php');

	if(isset($_GET['yvtset'])){
		$yvtset = $_GET['yvtset'];
	}

	$bk_yvtset = $yvtset;
	$bk_yvtid = null;

	if(strpos($_SERVER['HTTP_REFERER'], "?")){
		$qsref = explode("?", $_SERVER['HTTP_REFERER']);
		if(isset($qsref[1])){
			parse_str($qsref[1],$qsArr);

			if(isset($qsArr['yvtset'])&&isset($qsArr['id'])){
				$bk_yvtset = $qsArr['yvtset'];
				$bk_yvtid = $qsArr['id'];
			}
		}
	}

	require("../yvtset/".$yvtset.".php");

	// delete the photo

	foreach( $yvtListMeta['columns'] as $key => $value ) {
		// idx_photo
		if( $yvtListMeta['columns'][$key]['edittype'] == "idx_photo" ) {
			$file_src = "../".$yvtListMeta['src_dir']."/".$_GET['id'];
			if(is_file($file_src.".jpg")) unlink( $file_src.".jpg" );
			if(is_file($file_src.".png")) unlink( $file_src.".png" );
			if(is_file($file_src.".gif")) unlink( $file_src.".gif" );
		}

		// url_photo
		if( $yvtListMeta['columns'][$key]['edittype'] == "url_photo" ) {
			$sql = "SELECT * FROM `".$yvtListMeta['sql_tbl_name']."` WHERE `".$yvtListMeta['columns_idx']."` = :unoId ";

			$fileSelectSql = $dbConnect -> prepare( $sql );
			$fileSelectSql -> bindParam( ':unoId', $_GET['id'] );
			$fileSelectSql -> execute();

			$fileSelectDetail = $fileSelectSql -> fetch(PDO::FETCH_ASSOC);

			$file_src = "../".$yvtListMeta['src_dir']."/".$fileSelectDetail[$key];
			if(is_file($file_src)) unlink( $file_src );
		}
	}

	// ---------------------------------

	$unoDeleteSql = $dbConnect->prepare('DELETE FROM `'.$yvtListMeta['sql_tbl_name'].'` WHERE `'.$yvtListMeta['columns_idx'].'` = :unoId ;');
	$unoDeleteSql->bindParam(':unoId', $_GET['id']);


	if($unoDeleteSql->execute()){
		$jsonArray['status'] = true;
		$jsonArray['errcode'] = '97';
		$jsonArray['errmsg'] = $_GET['id'];
	}else{
		//sql error
		$jsonArray['errcode'] = '105';
		$jsonArray['errmsg'] = 'sql fail';
	}
		
}else{
	//post error
	$jsonArray['errcode'] = '103';
	$jsonArray['errmsg'] = 'post fail';
}

if($bk_yvtset!=$yvtset&&$bk_yvtid!=null){
	echo "<script language=javascript>
	      window.location.replace(\"../yvt-modify.php?yvtset=".$bk_yvtset."&id=".$bk_yvtid."\");
	      top.leftFrame.location.reload();
	      </script>";
}else{
	echo "<script language=javascript>
	      window.location.replace(\"../yvt-list.php?yvtset=".$yvtset."\");
	      top.leftFrame.location.reload();
	      </script>";
}
?>