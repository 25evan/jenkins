<?php
require('session.php');
require('yvtphotos.php');
echo "loading...";

// error_reporting(E_ALL);
// ini_set('display_errors', '1');
// echo('<pre style="color:green">'.print_r($_POST,true).'</pre><hr/>');
// print_r($_FILES);
// echo('<hr/>');

function push($projectUrl, $yvtset, $id, $timeout = 60, $sleepTime = 20){
	$url = $projectUrl.'/backstage/apo/push/push_curl.php?yvtset='.$yvtset.'&id='.$id.'&sleepTime='.$sleepTime;

	$headers = array('Content-Type: application/json');

	// Open connection
	$ch = curl_init();
	// Set the url, number of POST vars, POST data
	curl_setopt( $ch, CURLOPT_URL, $url );
	curl_setopt( $ch, CURLOPT_POST, true );
	curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	// 不等待結果
	curl_setopt( $ch, CURLOPT_TIMEOUT, $timeout );

	// 發送的訊息內容轉成 JSON 格式
	// curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields) );
	$result = curl_exec($ch);
	curl_close($ch);
}

//defaults
$uno_src_dir = '../../photos';
$uno_id = 0;
$uno_url_photo_name = "yvtdef";
// push curl
include("push/push_config.php");
$projectUrl = $config['projectUrl'];

if (isset($_POST['yvtset'])){
	require('sqldata.php');

	if(isset($_POST['yvtset'])){
		$yvtset = $_POST['yvtset'];
	}

	require("../yvtset/".$yvtset.".php");

	//photo path
	$uno_src_dir = '../'.$yvtListMeta['src_dir'];

	//if url_photo
	// if(isset($_FILES['upload-photos-url'])){
	// 	if( isset($yvtListMeta['src_prefix']) ){
	// 		$rnd_url_photo_name = uniqid($yvtListMeta['src_prefix']."_");
	// 	}else{
	// 		$rnd_url_photo_name = uniqid($yvtset."_");
	// 	}

	// 	//TODO check if null
	// 	$uno_url_photo_name = saveUnoPhotos('upload-photos-url', $uno_src_dir, $rnd_url_photo_name);
	// }

	//HACK 1018 url_photo
	if(isset($_FILES['upload-photos-url'])){
		if(is_uploaded_file( $_FILES['upload-photos-url']['tmp_name'] )){
			if( isset($yvtListMeta['src_prefix']) ){
				$rnd_url_photo_name = uniqid($yvtListMeta['src_prefix']."_");
			}else{
				$rnd_url_photo_name = uniqid($yvtset."_");
			}

			$urlPhotoMeta = null;
			$fin_output_size = null;
			$cut_info = "";
			foreach($yvtListMeta['columns'] as $mtk => $mtv){
				if( isset($mtv['edittype']) ){
					if( $mtv['edittype']=="url_photo" ){
						$urlPhotoMeta = $mtv;
					}
				}
			}
			if(isset($urlPhotoMeta['photo_sizes'])){
				$fin_output_size = $urlPhotoMeta['photo_sizes'];
			}
			// print_r(array('upload-photos-url', $uno_src_dir, $rnd_url_photo_name, true, $fin_output_size));
			$uno_url_photo_name = saveCustomizedPhotos('upload-photos-url', $uno_src_dir, $rnd_url_photo_name, true, $fin_output_size);
			// echo($uno_url_photo_name);
		}
	}

	$yvtInsertSQL_PRE = "INSERT INTO `".$yvtListMeta['sql_tbl_name']."` (";
	$yvtInsertSQL_MID = ") VALUES (";
	$yvtInsertSQL_FIN = ");";
	$yvtInsertSQLpArr = array();

	$yvtInsertSQL_NEW_FLAG = false;

	foreach($yvtListMeta['columns'] as $mtk => $mtv){
		if($mtk != $yvtListMeta['columns_idx']){
			if( isset($_POST[$mtk]) || $mtv['edittype'] == "url_photo" ){
				if($yvtInsertSQL_NEW_FLAG){
					$yvtInsertSQL_PRE .= ",";
				}
				$yvtInsertSQL_PRE .= " `".$mtk."` ";

				if($yvtInsertSQL_NEW_FLAG){
					$yvtInsertSQL_MID .= ",";
				}
				$yvtInsertSQL_MID .= " ? ";

				if($mtv['edittype'] == "url_photo"){
					$yvtInsertSQLpArr[] = $uno_url_photo_name;	//for urls
				}else{
					$yvtInsertSQLpArr[] = $_POST[$mtk];
				}

				$yvtInsertSQL_NEW_FLAG = true;

			}
		}
	}

	//sql merge
	$yvtInsertSQL = $yvtInsertSQL_PRE.$yvtInsertSQL_MID.$yvtInsertSQL_FIN;

	//debug
	// echo('<p>'.$yvtInsertSQL.'</p><pre style="color:red">'.print_r($yvtInsertSQLpArr,true).'</pre><hr/>');

	//DEBUG
	// $dbConnect->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );


	//execute SQL
	$unoInsertSql = $dbConnect->prepare( $yvtInsertSQL );
	if($unoInsertSql->execute($yvtInsertSQLpArr) ){

		//DEBUG
		// print_r($unoInsertSql);
		$uno_id = $dbConnect->lastInsertId();

		// 存圖
		if(isset($_FILES['upload-photos-idx'])){
			// saveUnoPhotos('upload-photos-idx', $uno_src_dir, $uno_id);
			// by Evan
			// saveCustomizedPhotos('upload-photos-idx', $uno_src_dir, $uno_id, true, $uno_output_size);

			//HACK1230
			$uploadFlag = true;
			if(isset($_POST['upload-photos-idx-default'])){
				if($_POST['upload-photos-idx-default'] == 'default'){
					foreach($yvtListMeta['columns'] as $mtk => $mtv){
						if($mtv['edittype'] == 'idx_photo'){
							if( isset($mtv['defaultvalue'])){
								if(file_exists('../'.$mtv['defaultvalue'])){
									copy( '../'.$mtv['defaultvalue'], $uno_src_dir."/".$uno_id.'.jpg');
									$uploadFlag = false;
									//saveCustomizedPhotos('upload-photos-idx', $uno_src_dir, $uno_id, true, $uno_output_size);
								}
							}
						}
					}
				}
			}
			if(is_uploaded_file( $_FILES['upload-photos-idx']['tmp_name'] )){
				$fin_output_size = null;
				$idxPhotoMeta = null;
				foreach($yvtListMeta['columns'] as $mtk => $mtv){
					if( isset($mtv['edittype']) ){
						if( $mtv['edittype']=="idx_photo" ){
							$idxPhotoMeta = $mtv;
						}
					}
				}

				if(isset($idxPhotoMeta['photo_sizes'])){
					$fin_output_size = $idxPhotoMeta['photo_sizes'];
				}

				$fin_output_type = ( isset($idxPhotoMeta['photo_type']) ) ? $idxPhotoMeta['photo_type'] : null;

				if($uploadFlag){
					saveCustomizedPhotos('upload-photos-idx', $uno_src_dir, $uno_id, true, $fin_output_size, $fin_output_type);
				}
			}
			// if($uploadFlag){
			// 	saveCustomizedPhotos('upload-photos-idx', $uno_src_dir, $uno_id, true, $uno_output_size);
			// }
		}

		//新增成功
		$jsonArray['status'] = true;
		$jsonArray['errcode'] = '97';
		$jsonArray['errmsg'] = 'sql succeed';

		if($jsonArray['status'] && is_numeric($uno_id)){
			if(isset($yvtListMeta['admin_log'])){
				print_r($yvtListMeta['admin_log']);
				if(is_array($yvtListMeta['admin_log'])){
					$updSql_PRE = "UPDATE `".$yvtListMeta['sql_tbl_name']."` SET ";
					$updSql_MID = "";
					$updSql_FIN = "WHERE `".$yvtListMeta['columns_idx']."` = ?; ";
					$updPArr = array();
					foreach($yvtListMeta['admin_log'] as $auk => $auv){
						if(strlen($updSql_MID)>0){
							$updSql_MID .= ", ";
						}
						$updSql_MID .= " `".$auk."`=? ";

						if($auv=="TIMESTAMP"){
							$updPArr[] = date("Y-m-d H:i:s");
						}else if(isset($_SESSION[$auv])){
							$updPArr[] = $_SESSION[$auv];
						}else{
							$updPArr[] = $_SESSION[$auv];
						}
					}
					if(count($updPArr)>0){
						$updPArr[] = $uno_id;
						$updSql = $updSql_PRE.$updSql_MID.$updSql_FIN;

						$uaUpdStm = $dbConnect->prepare( $updSql );
						$uaUpdStm->execute( $updPArr );
					}
				}
			}
		}

		// 推播公告 by Evan
		if($yvtListMeta['push_table']){
			if(isset($_POST['status']) && $_POST['status'] == 'PUBLISH'){
				push($projectUrl, $yvtset, $uno_id);
			}
		}
		
	}else{
		//sql error
		$jsonArray['errcode'] = '105';
		$jsonArray['errmsg'] = 'sql fail';
	}
	// echo json_encode($jsonArray);
}else{
	//post error
	$jsonArray['errcode'] = '103';
	$jsonArray['errmsg'] = 'post fail';
	// echo json_encode($jsonArray);
}

echo "<script language=javascript>
		setTimeout(function(){
			window.location.replace(\"../yvt-modify.php?yvtset=".$yvtset."&id=".$uno_id."\");
      		top.leftFrame.location.reload();
  		}, 50);
      </script>";			
?>