<?php
	require_once("session.php");
	require_once("sqldata.php");

	$slides_src_dir = '../upload/';
?>

<script type="text/javascript">
$('document').ready(function(){
	$( ".slides-sortable" ).sortable({
		// placeholder: "ui-state-highlight",
		stop:function(event, ui){
           console.log("stop: "+$(ui.item).attr('rel'));
		}
	});
});
</script>
		<!-- sliders -->
		<hr/>
		<section id="orders-sliders">
			<div class="title-wrapper">
				<div class="title-content pull-left">
					<h3> slides 管理</h3>
					<small>您可以上傳 slides、調整順序 </small>
				</div>
				<div class="title-plus pull-right">
					<!-- 可以在 #main 右上角放一些額外的按鈕 -->
				</div>
			</div>

			<section class="panel">
				<header class="panel-heading">
					slides上傳 <span class="size">尺寸：980px 寬 * 675px 高</span>
				</header>
				
				<div class="panel-body">

					<div class="form-group">
						<label class="control-label col-md-2">輪播slides上傳</label>
						<div class="col-md-8">
							<form id="form-upload" action="apo/slides-upload.php" method="post" enctype="multipart/form-data">
								<input name="upload[]" type="file" multiple class="form-control">
								<input name="tabid" type="hidden" value="46" />
								<input name="width" type="hidden" value="1440"/>
								<br/>
								<button class="btn btn-primary" type="submit" btn-done="完成">
									<i class="icon-upload icon-white"></i> 
									上傳
								</button>
							</form>
						</div>
					</div>
				</div>
			</section>
				
			<input type="submit" class="btn btn-primary" value="確定">
		</section>