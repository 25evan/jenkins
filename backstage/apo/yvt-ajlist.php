<?php
require("session.php");
include("sqldata.php");

//TODO
//2file ini
if(isset($_GET['yvtset'])){
	$yvtset = $_GET['yvtset'];
}

require("../yvtset/".$yvtset.".php");
//2file end

$stlCols = null;

foreach($yvtListMeta['columns'] as $mtk => $mtv){
	if( $mtv['listshow'] != false ){

		
		if( $mtv['edittype']!='idx_photo' ){
			if($stlCols!=null){
				$stlCols.=',';
			}
			$stlCols .= ' `'.$mtk.'` ';
		}
	}
}

if($stlCols==null){
	$stlCols = '*';
}

$listSqlStr_PRE = "SELECT ".$stlCols." FROM `".$yvtListMeta['sql_tbl_name']."` ";
$listSqlStr_MID = "";
$listSqlStr_FIN = "ORDER BY `".$yvtListMeta['sql_tbl_order_desc']."` DESC ;";

$listSqlpArr = array();

//foreign keys
if( isset($yvtListMeta['fk_keys']) ){
	$whereSqlStr_MID = "";
	$whereSqlStr_NEW = false;
	foreach ($_GET as $gk => $gv) {
		if( in_array($gk, $yvtListMeta['fk_keys']) ){

			if($yvtInsertSQL_NEW_FLAG){	$whereSqlStr_MID .= ",";	}

			$whereSqlStr_MID .= ' `'.$gk.'` = ? ';
			$whereSqlStr_NEW = true;
			$listSqlpArr[] = $gv;
		}
	}

	if( strlen($whereSqlStr_MID) > 0 ){
		$listSqlStr_MID = " WHERE ".$whereSqlStr_MID;
	}
}

$i = 1;

$listSqlStr = $listSqlStr_PRE.$listSqlStr_MID.$listSqlStr_FIN;
// echo($listSqlStr);

//sql execute

$retArr = array();
$retArr['data'] = array();
$listSelectSql = $dbConnect->prepare( $listSqlStr );
$listSelectSql->execute( $listSqlpArr );

if($listSelectSql->rowCount() > 0){
	while($listSelectRow = $listSelectSql->fetch(PDO::FETCH_ASSOC)){

		$sRow = array();
		foreach($yvtListMeta['columns'] as $mtk => $mtv){

			if( $mtv['listshow'] != false ){
				if( isset($mtv['listshowtype']) && $mtv['listshowtype'] == "idx_photo" ){
					// idx_photo
					// $img_src = $yvtListMeta['src_dir']."/".$listSelectRow[$yvtListMeta['columns_idx']].'.jpg';
					if( isset( $mtv['photo_type'] ) ) {
						$photo_type = $mtv['photo_type'];
					}else{
						$photo_type = 'jpg';
					}
					$img_src = $yvtListMeta['src_dir']."/".$listSelectRow[$yvtListMeta['columns_idx']].'.'.$photo_type;


					if(file_exists('../'.$img_src)){
						$img_src = '<img style="width:80px;" src="'.$img_src.'"/>';
					}else{
						$img_src = '無'.$mtv['label'];
					}

					$sRow[] = $img_src;
				}else if( isset($mtv['listshowtype']) && $mtv['listshowtype'] == "json_array_simple" ){
					$jasStr = "";
					foreach(json_decode($listSelectRow[$mtk]) as $ja => $jv){
						//<p>
						foreach($mtv['jsonsimplekey'] as $jkk => $jkv){
							$jasStr .= $jkv.':'.$jv->$jkv."<br/>";
						}
						//</p>
					}
					$sRow[] = $jasStr;
				}else if( isset($mtv['listshowtype']) && $mtv['listshowtype'] == "url_photo" ){
					$img_src = $yvtListMeta['src_dir']."/".$listSelectRow[$mtk];
					if(file_exists('../'.$img_src)){
						$img_src = '<img style="width:80px;" src="'.$img_src.'"/>';
					}else{
						$img_src = '無'.$mtv['label'];
					}
					$sRow[] = $img_src;
				}else if( isset($mtv['listshowtype']) && $mtv['listshowtype'] == "button" ){
					// <a target="_blank" href="yvt-modify.php?yvtset='.$yvtset.'&id='.$listSelectRow[$yvtListMeta['columns_idx']].'" class="btn btn-info">編輯</a>
					$sRow[] = $listSelectRow[$mtk];
				}else{
					
					if( isset($mtv['editarray']) && isset($mtv['listmapping']) ){

						//TODO move outside for cacheing
						//listmapping
						$mappingArr = array();
						foreach ($mtv['listmapping'] as $mapk => $mapv) {
							// echo($mapk.'=>'.$mapv);

							foreach ($mtv['editarray'] as $eak => $eav) {
								if(isset($eav[$mapk]) && isset($eav[$mapv])){
									$mappingArr[$eav[$mapk]] = $eav[$mapv];
									// echo($eav[$mapk]." -> ".$eav[$mapv]);
								}
							}
						}

						if( isset($mappingArr[ $listSelectRow[$mtk] ]) ){
							$sRow[] = $mappingArr[ $listSelectRow[$mtk] ];
						}else{
							$sRow[] = $listSelectRow[$mtk];
						}
					}else{
						// $sRow[] = $listSelectRow[$mtk];
						$sRow[] = mb_substr(strip_tags($listSelectRow[$mtk]),0,50,'utf-8');
					}
				}
			}
		}
		$retArr['data'][] = $sRow;
	}
}
// $retArr['dbg'] = $listSqlStr;
echo( json_encode($retArr) );
?>
