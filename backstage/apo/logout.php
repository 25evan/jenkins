<?php
session_start();

$_SESSION['CID'] = null;
$_SESSION['NAME'] = null;
$_SESSION['ACCOUNT_TYPE'] = null;


session_destroy();

header("Location: ../index.php");

?>
