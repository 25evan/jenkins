<?php
require('session.php');
require('sqldata.php');

// echo("<pre>".print_r($_POST,1)."</pre>");

if( isset($_POST['items_id']) && isset($_POST['new_opts']) && isset($_POST['new_opts_qty']) && isset($_POST['new_opts_price']) ){
	$optsStr = "";
	if( is_numeric($_POST['items_id']) && is_array($_POST['new_opts']) && is_numeric($_POST['new_opts_qty']) && is_numeric($_POST['new_opts_price'])){

		sort($_POST['new_opts'],SORT_NUMERIC);

		foreach ($_POST['new_opts'] as $key => $v) {
			if(strlen($v) > 0){
				$optsStr .= "_".$v;
			}
		}

		//chk if new
		$chkPcsStmt = $dbConnect->prepare("SELECT * FROM `items_opts_quantity` WHERE ( `items_id` = ? AND `items_opts_hash` = ? );");
		$chkPcsStmt->execute( array($_POST['items_id'], $optsStr) );

		if( $chkPcsStmt->rowCount() < 1 ){
			//INS prices & qty
			$newQtyStmt = $dbConnect->prepare("INSERT INTO `items_opts_quantity`(`items_id`, `items_opts_hash`, `qty_num`) VALUES (?,?,?);");
			$newQtyStmt->execute( array($_POST['items_id'], $optsStr , $_POST['new_opts_qty']) );

			$newPcsStmt = $dbConnect->prepare("INSERT INTO `items_opts_prices`(`items_id`, `items_opts_hash`, `prices`) VALUES (?,?,?);");
			$newPcsStmt->execute( array($_POST['items_id'], $optsStr , $_POST['new_opts_price']) );
		}else{
			echo('已經新增過了喔');
		}

	}
}else{

}

echo "<script language=javascript>
      window.location.replace(\"../yvt-items-manage.php?id=".$_POST['items_id']."\");
      top.leftFrame.location.reload();
      </script>";
?>