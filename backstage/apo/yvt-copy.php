<?php
require('session.php');

function makeRelCopy($yvtset, $rel_id, $main_id){
	global $dbConnect;
	require("../yvtset/".$yvtset.".php");

	$yvtInsertSQLArr = array();

	$yvtInsertSQL_NEW_FLAG = false;

	$yvtCopySQL_PRE = "INSERT INTO `".$yvtListMeta['sql_tbl_name']."` (";
	$yvtCopySQL_MID = ") SELECT ";
	$yvtCopySQL_FIN = " FROM `".$yvtListMeta['sql_tbl_name']."` WHERE `".$yvtListMeta['fk_keys'][0]."` = ";

	foreach($yvtListMeta['columns'] as $mtk => $mtv){
		if($mtk != $yvtListMeta['columns_idx']){
			if($yvtInsertSQL_NEW_FLAG){
				$yvtCopySQL_PRE .= ",";
				$yvtCopySQL_MID .= ",";
			}
			if($mtk == $yvtListMeta['fk_keys'][0]){
				$yvtCopySQL_MID .= $main_id;
				$yvtCopySQL_FIN .= $rel_id;
			}else{
				$yvtCopySQL_MID .= $mtk;
			}
			$yvtCopySQL_PRE .= $mtk;

			$yvtInsertSQL_NEW_FLAG = true;
		}
	}

	//sql merge
	$yvtInsertSQL = $yvtCopySQL_PRE.$yvtCopySQL_MID.$yvtCopySQL_FIN;

	// echo("<h4>".$yvtInsertSQL." ".print_r($yvtInsertSQLArr,true)."</h4>");
	//execute SQL
	$rupInsertSql = $dbConnect->prepare( $yvtInsertSQL );
	$rupInsertSql->execute($yvtInsertSQLArr);
}

//defaults
$uno_src_dir = '../../photos';
$uno_id = 0;
$uno_url_photo_name = "yvtdef";

if (isset($_POST['yvtset'])){
	require('sqldata.php');

	if(isset($_POST['yvtset'])){
		$yvtset = $_POST['yvtset'];
	}

	require("../yvtset/".$yvtset.".php");

	$yvtInsertSQL_PRE = "INSERT INTO `".$yvtListMeta['sql_tbl_name']."` (";
	$yvtInsertSQL_MID = ") VALUES (";
	$yvtInsertSQL_FIN = ");";
	$yvtInsertSQLArr = array();

	$yvtInsertSQL_NEW_FLAG = false;

	foreach($yvtListMeta['columns'] as $mtk => $mtv){
		if($mtk != $yvtListMeta['columns_idx']){
			if( isset($_POST[$mtk]) || $mtv['edittype'] == "url_photo" ){
				if($yvtInsertSQL_NEW_FLAG){
					$yvtInsertSQL_PRE .= ",";
				}
				$yvtInsertSQL_PRE .= " `".$mtk."` ";

				if($yvtInsertSQL_NEW_FLAG){
					$yvtInsertSQL_MID .= ",";
				}
				$yvtInsertSQL_MID .= " ? ";

				if($mtv['edittype'] == "url_photo"){
					$yvtSelectSQL = $dbConnect->query("SELECT `".$mtk."` FROM `".$yvtListMeta['sql_tbl_name']."` WHERE `".$yvtListMeta['columns_idx']."` = ".$_POST[$yvtListMeta['columns_idx']]);
					$yvtSelectRow = $yvtSelectSQL->fetch(PDO::FETCH_ASSOC);
					if($yvtSelectRow != null){
						$uno_url_photo_name = $yvtSelectRow[$mtk];
					}
					$yvtInsertSQLArr[] = $uno_url_photo_name;	//for urls
				}else{
					if($mtk == 'types'){
						$yvtInsertSQLArr[] = 'OFF';
					}else if($mtk == 'status'){
						$yvtInsertSQLArr[] = 'DRAFT';
					}else{
						$yvtInsertSQLArr[] = $_POST[$mtk];
					}
				}

				$yvtInsertSQL_NEW_FLAG = true;

			}
		}
	}

	//sql merge
	$yvtInsertSQL = $yvtInsertSQL_PRE.$yvtInsertSQL_MID.$yvtInsertSQL_FIN;

	//execute SQL
	$unoInsertSql = $dbConnect->prepare( $yvtInsertSQL );
	if($unoInsertSql->execute($yvtInsertSQLArr) ){

		$uno_id = $dbConnect->lastInsertId();

		// 存圖
		// if(isset($_FILES['upload-photos-idx'])){
		// 	// saveUnoPhotos('upload-photos-idx', $uno_src_dir, $uno_id);
		// 	// by Evan
		// 	saveCustomizedPhotos('upload-photos-idx', $uno_src_dir, $uno_id, true, $uno_output_size);
		// }

		foreach ($yvtListMeta['columns'] as $key => $value) {
			if(isset($yvtListMeta['columns'][$key]['edittype']) && $yvtListMeta['columns'][$key]['edittype'] == 'idx_photo'){
				// copy主圖
				if(isset($_POST[$yvtListMeta['columns_idx']]) && $_POST[$yvtListMeta['columns_idx']] != '' && file_exists('../'.$yvtListMeta['src_dir'].'/'.$_POST[$yvtListMeta['columns_idx']].'.'.$yvtListMeta['columns'][$key]['photo_type'])){
					copy('../'.$yvtListMeta['src_dir'].'/'.$_POST[$yvtListMeta['columns_idx']].'.'.$yvtListMeta['columns'][$key]['photo_type'], '../'.$yvtListMeta['src_dir'].'/'.$uno_id.'.'.$yvtListMeta['columns'][$key]['photo_type']);
					break;
				}
			}
		}

		//rel ini
		if( isset($yvtListMeta['rel_tbl']) ) {
			foreach ($yvtListMeta['rel_tbl'] as $rtk => $rtv) {
				makeRelCopy($rtk,$_POST[$yvtListMeta['columns_idx']],$uno_id);
			}
		}

		//新增成功
		$jsonArray['status'] = true;
		$jsonArray['errcode'] = '97';
		$jsonArray['errmsg'] = 'sql succeed';
		$jsonArray['uno_id'] = $uno_id;
	}else{
		//sql error
		$jsonArray['errcode'] = '105';
		$jsonArray['errmsg'] = 'sql fail';
	}
	echo json_encode($jsonArray);
}else{
	//post error
	$jsonArray['errcode'] = '103';
	$jsonArray['errmsg'] = 'post fail';
	echo json_encode($jsonArray);
}
?>