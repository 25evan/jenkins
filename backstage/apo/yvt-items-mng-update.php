<?php
require('session.php');
require('sqldata.php');

// echo("<pre>".print_r($_POST,1)."</pre>");

if( isset($_POST['items_id']) ){
	$itemsMngArr = array();

	foreach ($_POST as $key => $value) {
		if( substr($key,0,4) == "qty_"  ){
			$itemOptsKey = substr($key,3);
			if(!isset($itemsMngArr[$itemOptsKey])){
				$itemsMngArr[$itemOptsKey] = array();
			}
			$itemsMngArr[$itemOptsKey]['qty'] = $value;
		}else if( substr($key,0,7) == "prices_"  ){
			$itemOptsKey = substr($key,6);
			if(!isset($itemsMngArr[$itemOptsKey])){
				$itemsMngArr[$itemOptsKey] = array();
			}
			$itemsMngArr[$itemOptsKey]['prices'] = $value;
		}else{}
	}

	// echo("<pre>".print_r($itemsMngArr,1)."</pre>");

	foreach ($itemsMngArr as $key => $v) {
		if( isset($v['qty']) && isset($v['prices']) ){
			// echo("<pre>".print_r($v,1)."</pre>");

			//qty
			$qtyChkStmt = $dbConnect->prepare("SELECT * FROM `items_opts_quantity` WHERE `items_id` = ? AND `items_opts_hash` = ? ;");
			$qtyChkStmt->execute( array($_POST['items_id'], $key) );

			if( $qtyChkStmt->rowCount() > 0 ){
				$qtyChkRow = $qtyChkStmt->fetch(PDO::FETCH_ASSOC);
				if( $qtyChkRow['qty_num'] != $v['qty'] ){
					//UPD SQL
					$qtyUpdStmt = $dbConnect->prepare("UPDATE `items_opts_quantity` SET `qty_num` = ? WHERE `items_id` = ? AND `items_opts_hash` = ? ;");
					$qtyUpdStmt->execute( array( $v['qty'], $_POST['items_id'], $key) );
				}
			}

			
			//prices
			$qtyPcsStmt = $dbConnect->prepare("SELECT * FROM `items_opts_prices` WHERE ( `items_id` = ? AND `items_opts_hash` = ? AND `prices` = ? );");
			$qtyPcsStmt->execute( array($_POST['items_id'], $key , $v['prices']) );

			if( $qtyPcsStmt->rowCount() < 1 ){
				//INS prices
				$newPcsStmt = $dbConnect->prepare("INSERT INTO `items_opts_prices`(`items_id`, `items_opts_hash`, `prices`) VALUES (?,?,?);");
				$newPcsStmt->execute( array($_POST['items_id'], $key , $v['prices']) );
			}
		}
	}
}

echo "<script language=javascript>
      window.location.replace(\"../yvt-items-manage.php?id=".$_POST['items_id']."\");
      top.leftFrame.location.reload();
      </script>";			
?>