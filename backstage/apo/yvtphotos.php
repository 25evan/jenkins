<?php
// require('session.php');

function saveUnoPhotos($fileInputName, $photos_dir, $photos_name, $override = false){
	if( $photos_name!=null && $photos_dir!=null ){
		// 儲存圖片
		if (isset($_FILES[$fileInputName])){
			if(is_uploaded_file($_FILES[$fileInputName]['tmp_name'])){

				$fileSource = $_FILES[$fileInputName];

				$fileExt = $fileSource['type'];
				if($fileExt == 'image/jpeg'){
					$fileType=".jpg";
				}elseif($fileExt == 'image/gif'){
					$fileType=".gif";
				}elseif($fileExt == 'image/png'){
					$fileType=".png";
				}else{
					// exit;	using exit will cause others update fail
					return null;
				}

				$fileName = $photos_name.$fileType;
				$filePathName = $photos_dir."/".$fileName;

				//if not allow override by default
				if(file_exists($filePathName)){
					if(!$override){
						return null;
					}
				}

				if($fileType == ".jpg"){
					$src = imagecreatefromjpeg($fileSource['tmp_name']);
				}elseif($fileType == ".gif"){
					$src = imagecreatefromgif($fileSource['tmp_name']);
				}elseif($fileType == ".png"){
					$src = imagecreatefrompng($fileSource['tmp_name']);
				}

				//取得來源圖片長寬
				$src_w = imagesx($src);
				$src_h = imagesy($src);

				//設定圖長寬
				$thumb_w = $src_w;
				$thumb_h = $src_h;

				//建立儲存圖
				$thumb = imagecreatetruecolor($thumb_w, $thumb_h);
				imagealphablending($thumb, false);
				imagesavealpha($thumb, true);
				imagecopyresampled($thumb, $src, 0, 0, 0, 0, $thumb_w, $thumb_h, $src_w, $src_h);

				if($fileType == ".jpg"){
					imagejpeg($thumb, $filePathName, 100);
				}elseif($fileType == ".gif"){
					imagegif($thumb, $filePathName, 100);
				}elseif($fileType == ".png"){
					imagepng($thumb, $filePathName, 0);	// 0-9 range of png, 9 is maximum compression
				}

				return $fileName;
			}else{
				return null;
			}
		}else{
			return null;
		}
	}
}

function saveCustomizedPhotos($fileInputName, $photos_dir, $photos_name, $override = false, $output_size = null, $output_type = null){
	if( $photos_name!=null && $photos_dir!=null ){
		// 儲存圖片
		if (isset($_FILES[$fileInputName])){
			if(is_uploaded_file($_FILES[$fileInputName]['tmp_name'])){

				$fileSource = $_FILES[$fileInputName];
				$fileExt = $fileSource['type'];

				if( $fileExt == 'image/jpeg' )
					$fileUploadType = ".jpg";
				else if( $fileExt == 'image/gif' )
					$fileUploadType = ".gif";
				else if( $fileExt == 'image/png' )
					$fileUploadType = ".png";
				else
					return null;

				$fileType = ( $output_type != null ) ? ".".$output_type : $fileUploadType;

				$fileName = $photos_name.$fileType;
				$filePathName = $photos_dir."/".$fileName;

				//if not allow override by default
				if(file_exists($filePathName)){
					if(!$override){
						return null;
					}
				}

				if( $fileUploadType == ".jpg" )
					$src = imagecreatefromjpeg($fileSource['tmp_name']);
				else if( $fileUploadType == ".gif" )
					$src = imagecreatefromgif($fileSource['tmp_name']);
				else if( $fileUploadType == ".png" )
					$src = imagecreatefrompng($fileSource['tmp_name']);

				//取得來源圖片長寬
				$src_w = imagesx($src);
				$src_h = imagesy($src);

				if($output_size == null){
					//設定圖長寬
					$thumb_w = $src_w;
					$thumb_h = $src_h;

					//建立儲存圖
					$thumb = imagecreatetruecolor($thumb_w, $thumb_h);
					if( $fileType == "png" ) {
						imagecolortransparent($thumb, imagecolorallocatealpha($thumb, 0, 0, 0, 127));
						imagealphablending($thumb, false);
						imagesavealpha($thumb, true);
					} else {
						$white_background = imagecolorallocate($thumb, 255, 255, 255);
						imagefilledrectangle($thumb, 0, 0, $temp_width, $temp_height, $white_background);
					}
					imagecopyresampled($thumb, $src, 0, 0, 0, 0, $thumb_w, $thumb_h, $src_w, $src_h);

					if( $fileType == ".jpg" )
						imagejpeg($thumb, $filePathName, 100);
					else if( $fileType == ".gif" )
						imagegif($thumb, $filePathName, 100);
					else if( $fileType == ".png" )
						imagepng($thumb, $filePathName, 0);	// 0-9 range of png, 9 is maximum compression

				}else{
					if( isset($output_size['width']) ){
						$output_w = $output_size['width'];
					}else{
						$output_w = $output_size['w'];
					}

					// $output_w = $output_size['width'];
					if(isset($output_size['height'])||isset($output_size['h'])){
						// $output_h = $output_size['height'];
						if( isset($output_size['height']) ){
							$output_h = $output_size['height'];
						}else{
							$output_h = $output_size['h'];
						}


						$src_ratio = $src_w / $src_h;
						$output_ratio = $output_w / $output_h;

						if ( $src_ratio > $output_ratio ){
							// Triggered when source image is wider
							$temp_height = $output_h;
							$temp_width = ( int ) ( $output_h * $src_ratio );
						} else {
							// Triggered otherwise (i.e. source image is similar or taller)
							$temp_width = $output_w;
							$temp_height = ( int ) ( $output_w / $src_ratio );
						}
					}else{
						$output_h = $output_w / $src_w * $src_h;
						$temp_height = $output_h;
						$temp_width = $output_w;
					}

					
					// Resize the image into a temporary GD image
					$temp_gdim = imagecreatetruecolor( $temp_width, $temp_height );
					if( $fileType == ".png" ) {
						imagecolortransparent($temp_gdim, imagecolorallocatealpha($temp_gdim, 255, 255, 255, 127));
						imagealphablending($temp_gdim, false);
						imagesavealpha($temp_gdim, true);
					} else {
						$white_background = imagecolorallocate($temp_gdim, 255, 255, 255);
						imagefilledrectangle($temp_gdim, 0, 0, $temp_width, $temp_height, $white_background);
					}
					imagecopyresampled(
						$temp_gdim,
						$src,
						0, 0,
						0, 0,
						$temp_width, $temp_height,
						$src_w, $src_h
					);

					// Copy cropped region from temporary image into the desired GD image
					$x0 = ( $temp_width - $output_w ) / 2;
					$y0 = ( $temp_height - $output_h ) / 2;

					$desired_gdim = imagecreatetruecolor( $output_w, $output_h );
					if( $fileType == ".png" ) {
						imagecolortransparent($desired_gdim, imagecolorallocatealpha($desired_gdim, 255, 255, 255, 127));
						imagealphablending($desired_gdim, false);
						imagesavealpha($desired_gdim, true);
					} else {
						$white_background = imagecolorallocate($desired_gdim, 255, 255, 255);
						imagefilledrectangle($desired_gdim, 0, 0, $output_w, $output_h, $white_background);
					}
					imagecopy(
						$desired_gdim,
						$temp_gdim,
						0, 0,
						$x0, $y0,
						$output_w, $output_h
					);

					if( $fileType == ".jpg" )
						imagejpeg($desired_gdim, $filePathName, 100);
					else if( $fileType == ".gif" )
						imagegif($desired_gdim, $filePathName, 100);
					else if( $fileType == ".png" )
						imagepng($desired_gdim, $filePathName, 0);	// 0-9 range of png, 9 is maximum compression

				}

				return $fileName;
			}else{
				return null;
			}
		}else{
			return null;
		}
	}
}
?>