<?php
	include("../apo/sqldata.php");

	if(isset($_POST['eventid']) && $_POST['eventid'] != ""){
		$listSendSql = $dbConnect->prepare("SELECT * FROM `List` WHERE `event_id` = ? AND `send_time` IS NULL ORDER BY `id` ASC LIMIT 1;");
		$listSendSql->execute(array($_POST['eventid']));

		if($listSendSql->rowCount() > 0){
			$listSendRow = $listSendSql->fetch(PDO::FETCH_ASSOC);

			// 更新 Send-time
			$listSendUpdateSql = $dbConnect->prepare("UPDATE `List` SET `send_time` = ?, `send_way` = ? WHERE `id` = ? ;");
			$listSendUpdateSql->execute(array(date("Y-m-d H:i:s"), $_POST['send_way'], $listSendRow["id"]));

			$result = array('status' => true, 'email' => $listSendRow["email"], 'mobile' => $listSendRow["mobile"]);

			// 送出 email
			echo(json_encode($result));
		}
		else{
			$result = array('status' => false);
			echo(json_encode($result));
		}
	}
	else{
		echo('error');
	}
?>