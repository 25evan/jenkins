<?php
	session_start();
	include("apo/sqldata.php");
	include("source/head.php");

	// 如果沒有權限 直接踢出
	if($_SESSION['ACCOUNT_TYPE'] == null || $_SESSION['ACCOUNT_TYPE'] != 1){
		echo "<script language=javascript>
	      window.location.replace(\"apo/logout.php\");
	      top.leftFrame.location.reload();
	      </script>";
	}
?>

<body id="account">
	<section id="container">
		<?php include("source/header.php"); ?>
		<?php include("source/navi.php"); ?>
	
		<section id="main">
			<div class="title-wrapper">
				<div class="title-content pull-left">
					<h3>權限設定與管理</h3>
					<small>檢視全部的管理帳號</small>
				</div>
				<div class="title-plus pull-right">
					<a href="account-create.php" type="button" class="btn btn-danger"><i class="fa fa-plus"></i> 建立新的會員</a>
					<a href="account-setting.php" type="button" class="btn btn-info"><i class="fa fa-gear"></i> 權限設定</a>
				</div>
			</div>

			<section class="panel">
				<header class="panel-heading">
					會員明細
				</header>
				<table class="table table-striped table-bordered datatable datatable-noside">
					<thead>
						<tr>
							<th>#</th>
							<th>會員類別</th>
							<th>部門別</th>
							<th>會員登入帳號</th>
							<th>會員名稱</th>
							<th>會員Email</th>
							<th>帳號創立者</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 1;

							// 取得帳號基本資料
							$accoutListSql = $dbConnect->prepare("SELECT a.*, p.`account_type_name` FROM `Account` AS a LEFT JOIN `AccountPrivilege` AS p ON a.`account_type` = p.`account_type` ;");
							$accoutListSql->execute();

							if($accoutListSql->rowCount() > 0){
								while($accoutListRow = $accoutListSql->fetch(PDO::FETCH_ASSOC)){

									echo('
										<tr>
											<td>'.$i.'</td>
											<td>'.$accoutListRow["account_type_name"].'</td>
											<td>'.$accoutListRow["department"].'</td>
											<td>
												<a href="account-update.php?cid='.$accoutListRow["company_id"].'">'.$accoutListRow["company_id"].'</a>
											</td>
											<td>'.$accoutListRow["name"].'</td>
											<td>'.$accoutListRow["email"].'</td>
											<td>'.$accoutListRow["creator"].'</td>
										</tr>
									');

									$i++;
								}
							}else{
								echo('沒有會員資料');
							}
						?>
					</tbody>
				</table>
			</section>
		</section>
	</section>


</body>

<?php include("source/footer.php"); ?>
