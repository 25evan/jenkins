<?php

	$yvtListMeta = array();

	$yvtListMeta['labelname'] = "標籤";
	$yvtListMeta['label_id'] = "tags";

	$yvtListMeta['src_dir'] = '../upload';
	// $yvtListMeta['ouput_size'] = array( 'width' => 280, 'height' => 210 );
	// $yvtListMeta['ouput_size_rel'] = array( 'width' => 1000, 'height' => 600 );

	$yvtListMeta['sql_tbl_name'] = 'tags';
	$yvtListMeta['sql_tbl_order_desc'] = 'tag_id';

	$yvtListMeta['columns_idx'] = "tag_id";
	$yvtListMeta['columns'] = array(
			"tag_id"	=> array( "label" => "ID編號",	"listshow" => true,	"edittype" => "disabled" ),
			"text"		=> array( "label" => "標籤名稱",	"listshow" => true,	"edittype" => "input",	"placeholder" => "請輸入要顯示的標籤名稱" ),
			"status"	=> array( "label" => "顯示狀態",	"listshow" => false,"edittype" => "select",	"editarray" => array(
																											array( "ID" => "show", "text" => "顯示（上線）" ),
																											array( "ID" => "hide", "text" => "隱藏（草稿）" )
																										)
				)
		);

?>

