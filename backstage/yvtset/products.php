<?php

	$yvtListMeta = array();

	$yvtListMeta['labelname'] = "產品";
	$yvtListMeta['label_id'] = "products";

	// $yvtListMeta['src_dir'] = '../upload';
	// $yvtListMeta['ouput_size'] = array( 'width' => 280, 'height' => 210 );
	// $yvtListMeta['ouput_size_rel'] = array( 'width' => 1000, 'height' => 600 );

	$yvtListMeta['sql_tbl_name'] = 'products';
	$yvtListMeta['sql_tbl_order_desc'] = 'product_id';

	$yvtListMeta['columns_idx'] = "product_id";
	$yvtListMeta['columns'] = array(
			"product_id"	=> array( "label" => "ID編號",	"listshow" => true,	"edittype" => "disabled" ),
			"title"			=> array( "label" => "產品名稱",	"listshow" => true,	"edittype" => "input",	"placeholder" => "請輸入要顯示的產品名稱" ),
			"type"			=> array( "label" => "產品類別",	"listshow" => true, "edittype" => "select",	"editarray" => array(
																											array( "ID" => "fetnet",	"text" => "遠傳服務" ),
																											array( "ID" => "agent", 	"text" => "代理服務" )
																											),	"listmapping" => array( "ID" => "text" )
				),
			"status"		=> array( "label" => "顯示狀態",	"listshow" => true, "edittype" => "select",	"editarray" => array(
																											array( "ID" => "publish",	"text" => "顯示（上線）" ),
																											array( "ID" => "draft", 	"text" => "隱藏（草稿）" )
																											),	"listmapping" => array( "ID" => "text" )
				)
		);

	$yvtListMeta['rel_tbl']
			= array(
				"products_photos"	=>	array(	"label"=>"特色介紹", "idx"=>"product_id",	"reledittype"=>"orders", "photo_sizes" => array("w"=>480, "h"=>270) ),
				);

?>

