<?php

	$yvtListMeta = array();

	$yvtListMeta['labelname'] = "推播公告";
	$yvtListMeta['label_id'] = "pushnews";

	$yvtListMeta['sql_tbl_name'] = "pushnews";
	$yvtListMeta['sql_tbl_order_desc'] = "pushnews_id";

	$yvtListMeta['push_table'] = true;
	$yvtListMeta['push_device_sql_tbl_name'] = "devices";
	$yvtListMeta['push_log_sql_tbl_name'] = "pushnews_log";

	$yvtListMeta['columns_idx'] = "pushnews_id";
	$yvtListMeta['columns'] = array(
			"pushnews_id"	=> array( "label" => "ID編號",	"listshow" => true,		"edittype" => "disabled" ),
			"title"		=> array( "label" => "標題",		"listshow" => true,		"edittype" => "input",		"placeholder" => "標題", "pushattr" => "message" ),
			"url"		=> array( "label" => "連結",		"listshow" => false,	"edittype" => "input",		"placeholder" => "連結" ),
			"content"	=> array( "label" => "內容",		"listshow" => false,	"edittype" => "textarea", ),
			"status"	=> array( "label" => "顯示狀態",	"listshow" => true,		"edittype" => "select",		"pushattr" => "status",	"editarray" => array(
																													array( "ID" => "DRAFT",		"text" => "隱藏（草稿）" ),
																													array( "ID" => "PUBLISH",	"text" => "推播（上線）" )
																													), "listmapping" => array( "ID" => "text" )
				)
		);

?>

