<?php

	$casesSelectSql = $dbConnect -> query( "SELECT * FROM cases" );
	$casesSelectArr = $casesSelectSql -> fetchAll(PDO::FETCH_ASSOC);

	$tagsSelectSql = $dbConnect -> query( "SELECT * FROM products");
	$tagsSelectArr = $tagsSelectSql -> fetchAll(PDO::FETCH_ASSOC);

	$yvtListMeta = array();

	$yvtListMeta['labelname'] = "產品別標籤";
	$yvtListMeta['label_id'] = "case_product_tags";

	// $yvtListMeta['src_dir'] = '../upload';

	$yvtListMeta['sql_tbl_name'] = 'case_product_tags';
	$yvtListMeta['sql_tbl_order_desc'] = 'ID';

	$yvtListMeta['columns_idx'] = "ID";
	$yvtListMeta['columns'] = array(
			"ID"			=> array( "label" => "ID編號",	"listshow" => true,	"edittype" => "disabled" ),
			"case_id"		=> array( "label" => "案例ID",	"listshow" => true,	"edittype" => "select",	"editarray" => $casesSelectArr,	"listmapping" => array( "case_id" => "name_tw" ) ),
			"product_id"	=> array( "label" => "產品別ID",	"listshow" => true,	"edittype" => "select",	"editarray" => $tagsSelectArr,		"listmapping" => array( "product_id" => "title" ) )
		);

	// foreign keys
	$yvtListMeta['fk_keys'] = array( "case_id", "product_id" );

?>

