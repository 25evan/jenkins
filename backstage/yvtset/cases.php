<?php

	$yvtListMeta = array();

	$yvtListMeta['labelname'] = "客戶案例";
	$yvtListMeta['label_id'] = "cases";

	$yvtListMeta['src_dir'] = '../upload/cases';
	$yvtListMeta['ouput_size'] = array( 'width' => 900, 'height' => 675 );
	// $yvtListMeta['ouput_size_rel'] = array( 'width' => 1000, 'height' => 600 );

	$yvtListMeta['sql_tbl_name'] = "cases";
	$yvtListMeta['sql_tbl_order_desc'] = "case_id";

	$yvtListMeta['columns_idx'] = "case_id";
	$yvtListMeta['columns'] = array(
			"case_id"	=> array( "label" => "ID編號",	"listshow" => true,		"edittype" => "disabled" ),
			"imgname"	=> array( "label" => "案例縮圖",	"listshow" => true,		"edittype" => "idx_photo", 	"listshowtype" => "idx_photo" ),
			"title"		=> array( "label" => "標題",		"listshow" => true,		"edittype" => "input",	"placeholder" => "標題" ),
			"brief"		=> array( "label" => "案例簡述",	"listshow" => false,	"edittype" => "input",	"placeholder" => "案例簡述" ),
			"content"	=> array( "label" => "內容",		"listshow" => false,	"edittype" => "textarea", ),
			"status"	=> array( "label" => "顯示狀態",	"listshow" => true,		"edittype" => "select",	"editarray" => array(
																											array( "ID" => "publish",	"text" => "顯示（上線）" ),
																											array( "ID" => "draft",		"text" => "隱藏（草稿）" )
																											),	"listmapping" => array( "ID" => "text" )
				)
		);

	$yvtListMeta['rel_tbl'] = array(
			"case_property_tags" => array( "label" => "產業別標籤", "idx" => "case_id", "reledittype" => "msel_join" ),
			"case_product_tags" =>	array( "label" => "產品別標籤", "idx" => "case_id", "reledittype" => "msel_join" )
		);

?>

