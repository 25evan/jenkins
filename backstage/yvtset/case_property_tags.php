<?php

	$casesSelectSql = $dbConnect -> query( "SELECT * FROM cases" );
	$casesSelectArr = $casesSelectSql -> fetchAll(PDO::FETCH_ASSOC);

	$tagsSelectSql = $dbConnect -> query( "SELECT * FROM properties");
	$tagsSelectArr = $tagsSelectSql -> fetchAll(PDO::FETCH_ASSOC);

	$yvtListMeta = array();

	$yvtListMeta['labelname'] = "產業別標籤";
	$yvtListMeta['label_id'] = "case_property_tags";

	// $yvtListMeta['src_dir'] = '../upload';

	$yvtListMeta['sql_tbl_name'] = 'case_property_tags';
	$yvtListMeta['sql_tbl_order_desc'] = 'ID';

	$yvtListMeta['columns_idx'] = "ID";
	$yvtListMeta['columns'] = array(
			"ID"			=> array( "label" => "ID編號",	"listshow" => true,	"edittype" => "disabled" ),
			"case_id"		=> array( "label" => "案例ID",	"listshow" => true,	"edittype" => "select",	"editarray" => $casesSelectArr,	"listmapping" => array( "case_id" => "name_tw" ) ),
			"property_id"	=> array( "label" => "產業別ID",	"listshow" => true,	"edittype" => "select",	"editarray" => $tagsSelectArr,		"listmapping" => array( "property_id" => "title" ) )
		);

	// foreign keys
	$yvtListMeta['fk_keys'] = array( "case_id", "property_id" );

?>

