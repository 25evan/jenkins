<?php

	$yvtListMeta = array();

	$yvtListMeta['labelname'] = "裝置管理";
	$yvtListMeta['label_id'] = "devices";

	$yvtListMeta['sql_tbl_name'] = 'devices';
	$yvtListMeta['sql_tbl_order_desc'] = 'device_id';

	$yvtListMeta['columns_idx'] = "device_id";
	$yvtListMeta['columns'] = array(
			"device_id"	=> array( "label" => "ID編號",	"listshow" => true,	"edittype" => "disabled" ),
			"type"		=> array( "label" => "裝置類型",	"listshow" => true,	"edittype" => "select", "pushattr" => "type" ),
			"token"		=> array( "label" => "裝置token",	"listshow" => true,	"edittype" => "input", "pushattr" => "token" ),
		);

?>

