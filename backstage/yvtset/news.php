<?php

	$yvtListMeta = array();

	$yvtListMeta['labelname'] = "新聞";
	$yvtListMeta['label_id'] = "news";

	// $yvtListMeta['src_dir'] = '../upload/news';
	// $yvtListMeta['ouput_size'] = array( 'width' => 280, 'height' => 210 );
	// $yvtListMeta['ouput_size_rel'] = array( 'width' => 1000, 'height' => 600 );

	$yvtListMeta['sql_tbl_name'] = "news";
	$yvtListMeta['sql_tbl_order_desc'] = "date_show`,`news_id";

	$yvtListMeta['columns_idx'] = "news_id";
	$yvtListMeta['columns'] = array(
			"news_id"	=> array( "label" => "ID編號",	"listshow" => true,		"edittype" => "disabled" ),
			"title"		=> array( "label" => "標題",		"listshow" => true,		"edittype" => "input",		"placeholder" => "標題" ),
			"date_show"	=> array( "label" => "新聞日期",	"listshow" => true,		"edittype" => "input",		"editclass" => "bs-datetimepicker",	"placeholder" => "新聞日期" ),
			"type"		=> array( "label" => "新聞分類",	"listshow" => true,		"edittype" => "select",		"editarray" => array(
																													array( "ID" => "about",		"text" => "about" ),
																													array( "ID" => "service",	"text" => "service" ) )
																												),
			"link"		=> array( "label" => "連結",		"listshow" => false,	"edittype" => "input",		"placeholder" => "連結" ),
			"content"	=> array( "label" => "內容",		"listshow" => false,	"edittype" => "textarea", ),
			"status"	=> array( "label" => "顯示狀態",	"listshow" => true,		"edittype" => "select",		"editarray" => array(
																													array( "ID" => "publish",	"text" => "顯示（上線）" ),
																													array( "ID" => "draft",		"text" => "隱藏（草稿）" ) ),
																													"listmapping" => array( "ID" => "text" )
																												)
			// "ranks"			=>	array(	"label"=>"加權數值",	"listshow"=>true,	"edittype"=>"input",	"defaultvalue"=>"1" ),
			// "date"			=>	array(	"label"=>"時間",		"listshow"=>true,	"edittype"=>"input",	"editclass"=>"bs-datetimepicker",	"placeholder"=>"發佈時間" ),
			// "img_port"		=>	array(	"label"=>"縮圖",		"listshow"=>true,	"edittype"=>"idx_photo", "listshowtype"=>"idx_photo" ),
		);

?>

