<?php

	$yvtListMeta = array();

	$yvtListMeta['labelname'] = "常見問題分類";
	$yvtListMeta['label_id'] = "faq";

	// $yvtListMeta['src_dir'] = '../upload';
	// $yvtListMeta['ouput_size'] = array( 'width' => 280, 'height' => 210 );
	// $yvtListMeta['ouput_size_rel'] = array( 'width' => 1000, 'height' => 600 );
	$yvtListMeta['src_dir'] = '../upload/faq';
	$yvtListMeta['ouput_size'] = array( 'width' => 1280, 'height' => 450 );

	$yvtListMeta['sql_tbl_name'] = 'faq';
	$yvtListMeta['sql_tbl_order_desc'] = 'faq_id';

	$yvtListMeta['columns_idx'] = "faq_id";
	$yvtListMeta['columns'] = array(
			"faq_id"	=> array( "label" => "ID編號",	"listshow" => true,	"edittype" => "disabled" ),
			"title"			=> array( "label" => "分類名稱",	"listshow" => true,	"edittype" => "input",	"placeholder" => "請輸入要顯示的問題名稱" ),
			"status"		=> array( "label" => "顯示狀態",	"listshow" => true, "edittype" => "select",	"editarray" => array(
																											array( "ID" => "publish",	"text" => "顯示（上線）" ),
																											array( "ID" => "draft", 	"text" => "隱藏（草稿）" )
																											),	"listmapping" => array( "ID" => "text" )
				),
			"memo"			=> array( "label" => "備註",	"listshow" => true,	"edittype" => "input",	"placeholder" => "請輸入問題備註，標示該問題屬於哪個產品，以方便關聯" ),
		);

	$yvtListMeta['rel_tbl']
			= array(
				"faq_questions"	=>	array(	"label"=>"詳細問題", "idx"=>"faq_id",	"reledittype"=>"orders" ),
				);

?>

