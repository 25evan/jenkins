<?php

	$yvtListMeta = array();

	$yvtListMeta['labelname'] = "幻燈片";
	$yvtListMeta['label_id'] = "slides";

	$yvtListMeta['src_dir'] = '../upload/slides';
	$yvtListMeta['ouput_size'] = array( 'width' => 1280, 'height' => 484 );
	// $yvtListMeta['ouput_size_rel'] = array( 'width' => 1000, 'height' => 600 );

	$yvtListMeta['sql_tbl_name'] = 'slides';
	$yvtListMeta['sql_tbl_order_desc'] = 'slide_id';

	$yvtListMeta['columns_idx'] = "slide_id";
	$yvtListMeta['columns'] = array(
			"slide_id"	=> array( "label" => "ID編號",		"listshow" => true,		"edittype" => "disabled" ),
			"imgname"	=> array( "label" => "幻燈片圖片",	"listshow" => true,		"edittype" => "idx_photo", 	"listshowtype" => "idx_photo" ),
			"link"		=> array( "label" => "連結",			"listshow" => true,		"edittype" => "input",		"placeholder" => "請輸入要前往的網頁連結" ),
			"status"	=> array( "label" => "顯示狀態",		"listshow" => false,	"edittype" => "select",		"editarray" => array(
																														array( "ID" => "publish",	"text" => "顯示（上線）"),
																														array( "ID" => "draft",	"text" => "隱藏（草稿）")
																													)
				)
		);

?>

