<?php

	$yvtListMeta = array();

	$yvtListMeta['labelname'] = "產業";
	$yvtListMeta['label_id'] = "properties";

	// $yvtListMeta['src_dir'] = '../upload';
	// $yvtListMeta['ouput_size'] = array( 'width' => 280, 'height' => 210 );
	// $yvtListMeta['ouput_size_rel'] = array( 'width' => 1000, 'height' => 600 );

	$yvtListMeta['sql_tbl_name'] = 'properties';
	$yvtListMeta['sql_tbl_order_desc'] = 'property_id';

	$yvtListMeta['columns_idx'] = "property_id";
	$yvtListMeta['columns'] = array(
			"property_id"	=> array( "label" => "ID編號",	"listshow" => true,	"edittype" => "disabled" ),
			"title"			=> array( "label" => "產業名稱",	"listshow" => true,	"edittype" => "input",	"placeholder" => "請輸入要顯示的產業名稱" ),
			"status"		=> array( "label" => "顯示狀態",	"listshow" => true, "edittype" => "select",	"editarray" => array(
																											array( "ID" => "publish",	"text" => "顯示（上線）" ),
																											array( "ID" => "draft", 	"text" => "隱藏（草稿）" )
																											),	"listmapping" => array( "ID" => "text" )
				)
		);

?>

