<?php

	$yvtListMeta = array();

	$yvtListMeta['labelname'] = "產品相片";
	$yvtListMeta['label_id'] = "products_photos";

	$yvtListMeta['src_dir'] = '../upload';
	$yvtListMeta['src_prefix'] = 'phb';

	// $yvtListMeta['src_dir'] = '../upload';
	// $yvtListMeta['ouput_size'] = array( 'width' => 280, 'height' => 210 );
	// $yvtListMeta['ouput_size_rel'] = array( 'width' => 1000, 'height' => 600 );

	$yvtListMeta['sql_tbl_name'] = 'products_photos';
	$yvtListMeta['sql_tbl_order_desc'] = 'ID';

	$yvtListMeta['columns_idx'] = "ID";
	$yvtListMeta['columns_orders'] = "orders";
	$yvtListMeta['columns'] = array(
			"ID"			=>	array(	"label"=>"ID編號",		"listshow"=>true,	"edittype"=>"disabled" ),
			"types"			=>	array(	"label"=>"顯示狀態",		"listshow"=>false,	"edittype"=>"select",	"editarray"=>array(
																														array("ID"=>"SHOW","text"=>"顯示（上線）"),
																														array("ID"=>"OFF","text"=>"隱藏（草稿）") )
																													),
			"product_id"	=>	array(	"label"=>"產品ID",		"listshow"=>true,	"edittype"=>"input",	"placeholder"=>"請輸入關聯產品ID" ),
			"orders"		=>	array(	"label"=>"相片順序",		"listshow"=>true,	"edittype"=>"input",	"placeholder"=>"請輸入相片順序，範圍是0~65535，越小越前面" ),
			"title"			=> array( "label" => "相片名稱",	"listshow" => true,	"edittype" => "input",	"placeholder" => "請輸入要顯示的相片名稱" ),
			"caption"		=> array( "label" => "相片小標",	"listshow" => true,	"edittype" => "input",	"placeholder" => "請輸入要顯示的相片小標" ),
			"description"	=> array( "label" => "相片描述",	"listshow" => true,	"edittype" => "textarea" ),
			"url"			=>	array(	"label"=>"連結相片",		"listshow"=>true,	"edittype"=>"url_photo","listshowtype"=>"url_photo"	)
			);

	//foreign keys
	$yvtListMeta['fk_keys'] = array("product_id");

	//photo sizes
	$yvtListMeta['photo_sizes'] = array("w"=>640,"h"=>396);
?>
