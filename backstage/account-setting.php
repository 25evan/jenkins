<?php
	session_start();
	include("apo/sqldata.php");

	// 如果沒有權限 直接踢出
	if($_SESSION['ACCOUNT_TYPE'] == null || $_SESSION['ACCOUNT_TYPE'] != 1){
		echo "<script language=javascript>
	      window.location.replace(\"apo/logout.php\");
	      top.leftFrame.location.reload();
	      </script>";
	}

	// 更新 Update 表單
	if(isset($_POST["updateForm"]) && $_POST["updateForm"] != ""){
		$count = 0;
		foreach ($_POST["account-privilege"] as $key => $value) {
			if(isset($_POST["account-privilege"][$key]) && $_POST["account-privilege"][$key] != ""){
				$privilege = json_encode($_POST["account-privilege"][$key]);
				$accoutUpdateSql = $dbConnect->prepare("UPDATE `AccountPrivilege` SET `account_privilege` = ? WHERE `account_type` = ? ;");
				if($accoutUpdateSql->execute(array($privilege, $_POST['account-type'][$key]))){
					$count++;
				}
			}else{
				$privilege = null;
				$accoutUpdateSql = $dbConnect->prepare("UPDATE `AccountPrivilege` SET `account_privilege` = ? WHERE `account_type` = ? ;");
				if($accoutUpdateSql->execute(array($privilege, $_POST['account-type'][$key]))){
					$count++;
				}
			}
		}
		if($count == count($_POST["account-privilege"])){
			echo "<script language=javascript>
				alert('更新成功！');
				</script>";
		}else{
			echo "<script language=javascript>
				alert('更新失敗！');
				</script>";
		}
	}

	// 一般使用者
	$accountPrivilegeSelectSql = $dbConnect->prepare("SELECT * FROM `AccountPrivilege` WHERE `account_type` = 2 ;");
	$accountPrivilegeSelectSql->execute();
	$accountPrivilegeSelectDetail = $accountPrivilegeSelectSql->fetch(PDO::FETCH_ASSOC);
?>


<?php include("source/head.php"); ?>

<body id="account">
	<section id="container">
		<?php include("source/header.php"); ?>
		<?php include("source/navi.php"); ?>
	
		<section id="main">
			<div class="title-wrapper">
				<div class="title-content pull-left">
					<h3>帳號權限更新</h3>
					<small>變更權限</small>
				</div>
				<div class="title-plus pull-right">
					<a href="privilege-create.php" type="button" class="btn btn-danger"><i class="fa fa-plus"></i> 建立新的會員類別</a>
				</div>
			</div>

			<form class="form-horizontal  tasi-form" action="account-setting.php" method="post">
				<section class="panel">
					<header class="panel-heading">
						修改權限
					</header>
					
					<div class="panel-body">
						<?php
							$accountPrivilegeSelectSql = $dbConnect->prepare("SELECT * FROM `AccountPrivilege` WHERE `account_type` != 1 ;");
							$accountPrivilegeSelectSql->execute();
							$accountPrivilegeSelectDetail = $accountPrivilegeSelectSql->fetchAll(PDO::FETCH_ASSOC);
							foreach ($accountPrivilegeSelectDetail as $key2 => $value2) {
								echo '
						<div class="form-group">
							<input type="hidden" name="account-type[]" value="'.$accountPrivilegeSelectDetail[$key2]['account_type'].'">
							<label class="control-label col-md-2">會員類別：'.$accountPrivilegeSelectDetail[$key2]['account_type_name'].'</label>
							<div class="col-md-8">
								';
									$naviSelectSql = $dbConnect->prepare("SELECT * FROM `metadata` WHERE `key` = 'navi' ;");
									$naviSelectSql->execute();
									$naviSelectDetail = $naviSelectSql->fetch(PDO::FETCH_ASSOC);
									$array_navi = json_decode($naviSelectDetail['value'], true);
									$privilege = json_decode($accountPrivilegeSelectDetail[$key2]['account_privilege'], true);
									if($privilege != null){
										foreach ($array_navi as $key => $value) {
											if(in_array($array_navi[$key]['name'], $privilege)){
												echo '
								<input type="checkbox" name="account-privilege['.$key2.'][]" value="'.$array_navi[$key]['name'].'" checked>'.$array_navi[$key]['name_tw'].'<br/>
												';
											}else{
												echo '
								<input type="checkbox" name="account-privilege['.$key2.'][]" value="'.$array_navi[$key]['name'].'">'.$array_navi[$key]['name_tw'].'<br/>
												';
											}
										}
									}else{
										foreach ($array_navi as $key => $value) {
											echo '
								<input type="checkbox" name="account-privilege['.$key2.'][]" value="'.$array_navi[$key]['name'].'">'.$array_navi[$key]['name_tw'].'<br/>
											';
										}
									}
								echo '
							</div>
						</div>
								';
							}
						?>
					</div>
				</section>

				<hr/>
				<input type="hidden" name="updateForm" value="update"/>
				<input type="submit" class="btn btn-primary" value="確定">
			</form>
		</section>
	</section>


</body>

<?php include("source/footer.php"); ?>
