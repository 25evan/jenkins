<?php
	error_reporting(E_ALL | E_NOTICE); # do not show notices as library is php4 compatable
	ini_set('display_errors', true);
	
	if(isset($_GET['id'])){
		$uno_id = $_GET['id'];
	}else{
		header("Location: product.php");
	}

	require("apo/session.php");
	include("apo/sqldata.php");
	include("source/head.php");

	//TODO
	//2file ini
	if(isset($_GET['yvtset'])){
		$yvtset = $_GET['yvtset'];
	}

	require_once("yvtset/".$yvtset.".php");

	$idx_photo_flag = false;
	$url_photo_flag = false;
	$url_photo_colname = "";

	$idx_photo_default_path = null;

	//load data before update
	$unoSelectSql = $dbConnect->prepare("SELECT * FROM `".$yvtListMeta['sql_tbl_name']."` WHERE `".$yvtListMeta['columns_idx']."` = ? ;");
	$unoSelectSql->execute(array($uno_id));
	$unoSelectRow = $unoSelectSql->fetch(PDO::FETCH_ASSOC);
?>


<script type="text/javascript">
var yvtset = '<?php echo $yvtset;?>';
var pushStatus = "<?php if(isset($yvtListMeta['push_table']) && $yvtListMeta['push_table'] && isset($unoSelectRow['status'])) echo $unoSelectRow['status']; ?>";
$(document).ready(function(){
    // CKEDITOR
    // CKEDITOR.replaceAll();
	// CKEDITOR.replace( '.bs-ckeditor', {
	// 	customConfig: ''
	// });
	if(pushStatus == 'PUBLISH'){
		$('input[name="title"]').attr('readonly', true);
		$('input[name="url"]').attr('readonly', true);
		$('input[type="submit"]').attr('disabled', true);
		$('textarea').attr('readonly', true);
		$('select').attr('readonly', true);
	}
});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		// $('#copy-href').click(function(){
			// window.open("yvt-modify.php?yvtset=<?php echo($yvtset); ?>&id=1");
		// });
		$('#delete').click(function(){
			var chk = confirm("您確定要刪除？刪除後是無法復原的喔！");
			if(chk == true){
				window.location.href = 'apo/yvt-delete.php?yvtset=<?php echo($yvtset); ?>&id=<?php echo($unoSelectRow[$yvtListMeta['columns_idx']]); ?>';
			}
		});

		$('.btn_confirm').click(function(e){
			var chk = confirm("您確定要 "+$(this).html()+" 嗎？");
			if(chk == true){
				return true;
			}else{
				e.preventDefault();
				return false;
			}
		})

		$('#copy').click(function(){
			$.ajax({
				url: "apo/yvt-copy.php",
				type: "POST",
				cache: false,
				// data: {type : 'email', email : mineyEmail, password : mineyEmailPWD},
				data: $('form').serialize(),
				success: function(data){
					//alert(data);
					console.log(data);
					data = JSON.parse(data);
					console.log(data);

					if(data.status){
						// $('#copy-href').attr('href', "yvt-modify.php?yvtset=<?php echo($yvtset); ?>&id="+data.uno_id);
						// $('#copy-href').trigger('click');
						alert("複製成功！新的ID為 "+data.uno_id+"。目前預設為草稿狀態，編輯完成請將顯示狀態改為「顯示 (上線)」");
						window.location.href = "yvt-modify.php?yvtset=<?php echo($yvtset); ?>&id="+data.uno_id;
						// $('#copy-href').click();
						// newTab(data.uno_id);
						// function(event){
			   //           event.preventDefault();
			   //           event.stopPropagation();
			   //           window.open("yvt-modify.php?yvtset=<?php echo($yvtset); ?>&id="+data.uno_id, '_blank');
         				// }
						// setSession('miney_name',data.name);
						// setSession('miney_email',data.email);
						// setSession('miney_gender',data.gender);
						// setSession('miney_id',data.id);

						// setSession('miney_fbname',data.name);
						
						// $('#theloginmodal').modal('hide');
						// showStatus('已經成功登入囉');

						// setTimeout(function(){
						// 	location.reload();
						// },800);
					}else{
						// showStatus('登入失敗，請檢查您的帳號密碼');
					}
				}
			});
		});

		$('.get-srturl').click(function(){
			getShortUrl($(this).attr('rel'));
		});
	});

function newTab(uno_id){
	window.open("yvt-modify.php?yvtset=<?php echo($yvtset); ?>&id="+uno_id);
}

function getShortUrl(longUrl){
	$('.short-url-wrapper').html('<i class="fa fa-spinner fa-spin"></i> 取得縮址中...');
	var longUrl = longUrl;
	$.ajax({
		url: "apo/short-url.php",
		type: "POST",
		cache: false,
		data: {longUrl : longUrl},
		success: function(data){
			$('.short-url-wrapper').html('縮址：'+data);
		}
	});
}
</script>

<style>
 
/* Side notes for calling out things
-------------------------------------------------- */
 
/* Base styles (regardless of theme) */
.bs-callout {
  margin: 20px 0;
  padding: 15px 30px 15px 15px;
  border-left: 5px solid #eee;
}
.bs-callout h4 {
  margin-top: 0;
}
.bs-callout p:last-child {
  margin-bottom: 0;
}
.bs-callout code,
.bs-callout .highlight {
  background-color: #fff;
}
 
/* Themes for different contexts */
.bs-callout-danger {
  background-color: #fcf2f2;
  border-color: #dFb5b4;
}
.bs-callout-warning {
  background-color: #fefbed;
  border-color: #f1e7bc;
}
.bs-callout-info {
  background-color: #f0f7fd;
  border-color: #d0e3f0;
}

</style>

<body id="<?php echo($yvtListMeta['label_id']); ?>">
	<section id="container">
		<a id="copy-href" href="" target="_blank" style="display: none;">copy</a>
		<?php include("source/header.php"); ?>
		<?php include("source/navi.php"); ?>
		<?php include("source/quick-navi.php"); ?>
	
		<section id="main">
			<div class="title-wrapper">
				<div class="title-content pull-left">
					<h3><?php echo($yvtListMeta['labelname']); ?>修改</h3>
					<small></small>
					<div class="short-url-wrapper"></div>
				</div>
				<div class="title-plus pull-right">
					<!-- 可以在 #main 右上角放一些額外的按鈕 -->
					<a href="yvt-list.php?yvtset=<?php echo($yvtset); ?>" type="button" class="btn btn-danger"><i class="fa fa-list"></i> <?php echo($yvtListMeta['labelname']); ?>列表</a>
					<btn id="delete" type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i> 刪除<?php echo($yvtListMeta['labelname']);?></btn>
					<btn id="copy" type="button" class="btn btn-danger"><i class="fa fa-copy"></i> 複製<?php echo($yvtListMeta['labelname']);?></btn>
				</div>
			</div>
			<form class="form-horizontal tasi-form" action="apo/yvt-update.php" method="post" enctype="multipart/form-data">
				<section class="panel">
					<header class="panel-heading">
						修改<?php echo($yvtListMeta['labelname']); ?>
					</header>

					<!-- for vyt system -->
					<input type="hidden" name="yvtset" value="<?php echo($yvtset);?>" />
					<input type="hidden" name="<?php echo($yvtListMeta['columns_idx']); ?>" value="<?php echo($uno_id); ?>" />
					
					<div class="panel-body">
						<?php
							foreach($yvtListMeta['columns'] as $mtk => $mtv){
								$labelname = $mtv['label'];

								$rdlyStr = '';
								if(isset($mtv['readonly'])){
									if($mtv['readonly']=='modify'||$mtv['readonly']=='all'){
										$rdlyStr = ' readonly="readonly" disabled="disabled" ';
									}
								}

								if(isset($mtv['hint'])){
									$labelname .= '<br/><span class="yvt-hint">'.$mtv['hint'].'</span>';
								}
								if( isset($mtv['edittype']) ){
									if( $mtv['edittype']=="input" ){
										//INPUT
						// 				echo('
						// <div class="form-group">
						// 	<label class="control-label col-md-2">'.$mtv['label'].'</label>
						// 	<div class="col-md-8">
						// 		<input size="16" style="float:left;" type="text" name="'.$mtk.'" class="form-control" value="'.$unoSelectRow[$mtk].'" >
						// 	</div>
						// </div>
						// 					');
										// by Evan editclass
										if(isset($mtv['editclass'])){
											$editclass = $mtv['editclass'];
										}else{
											$editclass = null;
										}

										$attr_placeholder = isset($mtv['placeholder']) ? 'placeholder="'.$mtv['placeholder'].'"' : null;

										echo('
						<div class="form-group">
							<label class="control-label col-md-2" yvtnavi="'.$mtk.'">'.$labelname.'</label>
							<div class="col-md-8">
								<input size="16" style="float:left;" type="text" name="'.$mtk.'" class="form-control '.$editclass.'" value="'.$unoSelectRow[$mtk].'" '.$attr_placeholder.' '.$rdlyStr.'>
							</div>
						</div>
											');
									}else if( $mtv['edittype']=="select" ){
										//SELECT
										echo('
						<div class="form-group">
							<label class="control-label col-md-2" yvtnavi="'.$mtk.'">'.$labelname.'</label>
							<div class="col-md-8">
								<select class="form-control m-bot15 wf200" name="'.$mtk.'" '.$rdlyStr.'>');
										foreach ($mtv['editarray'] as $ek => $ev) {
											$selk = "ID";
											$selv = "text";
											if( isset($mtv['listmapping']) ){
												foreach( $mtv['listmapping'] as $k => $v ){
													$selk = $k;
													$selv = $v;
												}
											}
											echo('
									<option value="'.$ev[$selk].'"');

											if($ev[$selk] == $unoSelectRow[$mtk]){
												echo(' selected="selected" ');
											}

											echo('>'.$ev[$selv].'</option>
												');
										}
								echo('</select>
							</div>
						</div>
											');
							// <div class="col-md-3">
							// 	<div class="input-group">
      	// 								<input type="text" class="form-control q-sch-ipt" placeholder="快速篩選" />
      	// 								<div class="input-group-addon">
      	// 									<i class="fa fa-search"></i>
      	// 								</div>
    			// 					</div>
							// </div>
									}else if( $mtv['edittype']=="textarea" || $mtv['edittype']=="ckeditor" ){
										//TEXTAREA
										if(isset($mtv['editclass'])){
											$editclass = $mtv['editclass'];
										}else{
											$editclass = null;
										}

										if( $mtv['edittype']=="ckeditor" ){
											$editclass .= " ckeditor ";
										}

										echo('
						<div class="form-group">
							<label class="control-label col-md-2" yvtnavi="'.$mtk.'">'.$labelname.'</label>
							<div class="col-md-8">
								<textarea class="form-control '.$editclass.'" name="'.$mtk.'" '.$rdlyStr.' >');

										if( $mtv['edittype']=="ckeditor" ){
											echo(stripslashes($unoSelectRow[$mtk]));
										}else{
											echo($unoSelectRow[$mtk]);
										}

										echo('</textarea>
							</div>
						</div>');
									}else if( $mtv['edittype']=="checkbox-text" ){

										$oValArr = null;
										if(isset($unoSelectRow[$mtk])){
											$oValArr = explode(",", $unoSelectRow[$mtk]);
										}

										echo('
						<div class="form-group">
							<label class="control-label col-md-2" yvtnavi="'.$mtk.'">'.$labelname.'</label>
							<div class="col-md-8">
								<div class="form-group">
										');
										foreach ($mtv['editarray'] as $ek => $ev) {
											$selk = "ID";
											$selv = "text";
											if( isset($mtv['listmapping']) ){
												foreach( $mtv['listmapping'] as $k => $v ){
													$selk = $k;
													$selv = $v;
												}
												echo('
								
									<label class="checkbox-inline">
										<input type="checkbox" name="'.$mtk.'[]" value="'.$ev[$selk].'" ');
												if(is_array($oValArr)){
													if( in_array($ev[$selk], $oValArr) ){
														$oValArr = array_diff($oValArr, array($ev[$selk]));

														echo('checked="checked"');
													}
												}
												echo(' '.$rdlyStr.' >
										'.$ev[$selv].'
									</label>

													');
											}
										}
										if(isset($mtv['elsetext'])){
											if($mtv['elsetext']==true){
												
												echo('
									<input size="4" style="float:left;" type="text" name="'.$mtk.'[]" class="form-control" placeholder="其他，若有多個請以「,」分隔 " ');
												if(is_array($oValArr)){
													echo(' value="'.implode(",", $oValArr).'" ');
												}
												echo(' >
													');
											}
										}
										
										echo('
								</div>
							</div>
						</div>
											');
									}else if( $mtv['edittype']=="idx_photo" ){
										$idx_photo_flag = true;
										if( isset($mtv['defaultvalue']) ){
											if( file_exists("../".$idx_photo_default_path) ){
												$idx_photo_default_path = $mtv['defaultvalue'];
											}
										}
									}else if( $mtv['edittype']=="url_photo" ){
										$url_photo_flag = true;
										$url_photo_colname = $mtk;
									}else if( $mtk=="youtube_id" ){
										// by Evan
										$url_youtube_flag = true;
										$url_youtube_colname = $mtk;
									}else{
										//HIDE
						// 				echo('
						// <div class="form-group">'.$mtv['edittype'].'</div>
						// 				');
									}
								}
							}

							//idx_photo
							if( $idx_photo_flag ){

								$idxPhotoMeta = null;
								$cut_info = "";
								$labelname = $yvtListMeta['labelname']."圖片";
								foreach($yvtListMeta['columns'] as $mtk => $mtv){
									if( isset($mtv['edittype']) ){
										if( $mtv['edittype']=="idx_photo" ){
											$idxPhotoMeta = $mtv;
										}
									}
								}

								if( $idxPhotoMeta!=null ){
									if(isset($idxPhotoMeta['photo_sizes'])){
										$cut_info = '上傳後系統自動裁切大小：寬：'.$idxPhotoMeta['photo_sizes']['w'].'px';
										if(isset($idxPhotoMeta['photo_sizes']['h'])){
											$cut_info .= ', 高：'.$idxPhotoMeta['photo_sizes']['h'].'px';
										}
									}
									if(isset($idxPhotoMeta['label'])){
										$labelname = $idxPhotoMeta['label'];
									}
									if(isset($idxPhotoMeta['hint'])){
										$labelname .= '<br/><span class="yvt-hint">'.$idxPhotoMeta['hint'].'</span>';
									}
								}

								echo('
						<div class="form-group">
							<label class="control-label col-md-2" yvtnavi="'.$mtk.'">'.$labelname.'</label>
							<div class="col-md-8">
								');
								// $img_src = $yvtListMeta['src_dir'].'/'.$unoSelectRow[$yvtListMeta['columns_idx']].".jpg";
								$photo_type = ( isset( $idxPhotoMeta['photo_type'] ) ) ? $idxPhotoMeta['photo_type'] : "jpg";

								$img_src = $yvtListMeta['src_dir'].'/'.$unoSelectRow[$yvtListMeta['columns_idx']].".".$photo_type;

								if( $unoSelectRow[$yvtListMeta['columns_idx']] != '' && file_exists($img_src) ) {
									echo '<img class="preview-image" src="'.$img_src.'" />';
								}

								echo('
								<input size="16" type="file" name="upload-photos-idx" class="form-control wp50">
								<span class="size">'.$cut_info.'</span><br/>
								');
								if($idx_photo_default_path!=null){
									echo('
								或者<br/>
								<input type="checkbox" name="upload-photos-idx-default" value="default"> 使用預設圖片 (將會刪除上傳圖片)<br/>
								<img src="'.$idx_photo_default_path.'" class="img-thumbnail" />
								
									');
								}
								echo('
							</div>
						</div>
							');
							}

							//url_photo
							if( $url_photo_flag ){

								$urlPhotoMeta = null;
								$cut_info = "";
								$labelname = $yvtListMeta['labelname'];
								foreach($yvtListMeta['columns'] as $mtk => $mtv){
									if( isset($mtv['edittype']) ){
										if( $mtv['edittype']=="url_photo" ){
											$urlPhotoMeta = $mtv;
										}
									}
								}

								if( $urlPhotoMeta!=null ){
									if(isset($urlPhotoMeta['photo_sizes'])){
										$cut_info = '上傳後系統自動裁切大小：寬：'.$urlPhotoMeta['photo_sizes']['w'].'px';
										if(isset($urlPhotoMeta['photo_sizes']['h'])){
											$cut_info .= ', 高：'.$urlPhotoMeta['photo_sizes']['h'].'px';
										}
									}
									if(isset($urlPhotoMeta['label'])){
										$labelname = $urlPhotoMeta['label'];
									}

									if(isset($urlPhotoMeta['hint'])){
										$labelname .= '<br/><span class="yvt-hint">'.$urlPhotoMeta['hint'].'</span>';
									}
								}

								echo('
						<div class="form-group">
							<label class="control-label col-md-2" yvtnavi="'.$mtk.'">'.$labelname.'</label>
							<div class="col-md-8">
								');
								$img_src = $yvtListMeta['src_dir'].'/'.$unoSelectRow[$url_photo_colname];
								if( file_exists($img_src)){
									echo '
									<img class="preview-image" src="'.$img_src.'" />
									';
								}
								echo('
								<input size="16" type="file" name="upload-photos-url" class="form-control wp50">
								<span class="size">'.$cut_info.'</span><br/>
							</div>
						</div>
								');
							}
						?>
					<?php
					if(isset($yvtListMeta['admin_log'])){
						echo('
						<div class="form-group">
							<label class="control-label col-md-2">最後更新紀錄</label>
							<div class="col-md-8">

						');
						if(isset($yvtListMeta['admin_log']['updadm_id'])){
							$admStm = $dbConnect->prepare("SELECT * FROM `Account` WHERE `id`=? ;");
							$admStm->execute(array($unoSelectRow['updadm_id']));
							$admRow = $admStm->fetch(PDO::FETCH_ASSOC);
							echo('<p>'.$admRow['name'].' '.$admRow['department'].'</p>');
						}

						if(isset($yvtListMeta['admin_log']['updadm_time'])){
							echo('<p>'.$unoSelectRow['updadm_time'].'</p>');
						}
						echo('
							</div>
						</div>
							');
					}
					?>
					</div>
					
				</section>
				<?php
				//關聯連結
				// if(isset($yvtListMeta['rel_tbl'])){
				// 	echo('<hr/>');
				// 	foreach ($yvtListMeta['rel_tbl'] as $fk => $fv) {
				// 		echo('
				// 		<a href="yvt-list.php?yvtset='.$fk.'&'.$fv['idx'].'='.$unoSelectRow[$yvtListMeta['columns_idx']].'" class="btn btn-primary">'.$fv['label'].'</a>
				// 		');
				// 	}
				// }
				?>
				<?php
					//orders sliders
					include_once("apo/yvtuikit.php");

					if(isset($yvtListMeta['rel_tbl'])){
						echo('<hr/>');
						foreach ($yvtListMeta['rel_tbl'] as $fk => $fv) {

							//利用function 避免 複寫 $yvtListMeta
							if( $fv['reledittype'] == 'orders' ){
								//orders slider
								rel_edit_block_orders( $fk, $fv, $unoSelectRow[$yvtListMeta['columns_idx']] );
							}else if( $fv['reledittype'] == 'msel_join' ){
								//orders slider
								rel_edit_block_msjoin( $fk, $fv, $unoSelectRow[$yvtListMeta['columns_idx']] );
							}else{
								rel_edit_block_default( $fk, $fv, $unoSelectRow[$yvtListMeta['columns_idx']] );
							}
						}
					}
				?>
				<hr/>
				<input type="submit" class="btn btn-primary" value="確定">
			</form>
		</section>
	</section>

	<script type="text/javascript">
	var qsi_cache_obj = {};
	var qsi_cache_idx = 0;
	function qSearchIpt(){

		var k_idx = null;

		if( typeof $(this).attr('qsi_cache_idx') == 'undefined' ){
			$(this).attr('qsi_cache_idx',qsi_cache_idx);
		}else{
			k_idx = $(this).attr('qsi_cache_idx');
		}

		var $par_div = $(this).parents('div.form-group');
		var $flt_sel = $par_div.find('select');	
		var $all_opts = $flt_sel.find('option');

		var sch_str = $(this).val();
		var sel_val = $flt_sel.val();

		if( k_idx==null || typeof qsi_cache_obj[k_idx] == 'undefined' ){
			var all_opts_objs = [];

			$all_opts.each(function(){
				all_opts_objs.push({'v':$(this).val(),'n':$(this).html()});
			});

			qsi_cache_obj[qsi_cache_idx] = all_opts_objs;
			k_idx = qsi_cache_idx;
			qsi_cache_idx++;
		}

		$all_opts.remove();

		$.each(qsi_cache_obj[k_idx],function(){
			if(this.n.search(sch_str) >= 0){
				var $newOpt = $('<option>');
				$newOpt.attr('value',this.v).html(this.n);
				$flt_sel.prepend($newOpt);
			}else if(this.v == sel_val){
				var $newOpt = $('<option>');
				$newOpt.attr('value',this.v).html(this.n);
				$flt_sel.prepend($newOpt);
			}
		});
	}
	$('document').ready(function(){
		$( ".slides-sortable" ).sortable({
			// placeholder: "ui-state-highlight",
			stop:function(event, ui){
	           console.log("stop: "+$(ui.item).attr('rel'));
			}
		});

		//HACK
		$('input.q-sch-ipt').on('change',qSearchIpt);
	});
	</script>

</body>

<?php include("source/footer.php"); ?>
