<?php
	include("apo/sqldata.php");

	$cid = $_GET["cid"];
	$token = $_GET["token"];
	$newPW = $_POST["pw"];

	// 按下 POST
	if(isset($newPW) && $newPW != ""){
		$md5PW = md5($cid.$newPW);

		$pwUpdateSql = $dbConnect->prepare("UPDATE `Account` SET `password` = ? WHERE `company_id` = ? ;");
		$pwUpdateSql->execute(array($md5PW, $cid ));

		header('Location: index.php?cid='.$cid);
	}
	else{
		// 判斷 Token 是否正確
		$pwCheckSql = $dbConnect->prepare("SELECT * FROM `Account` WHERE `company_id` = ? ;");
		$pwCheckSql->execute(array($cid));

		if($pwCheckSql->rowCount() > 0){
			$pwCheckRow = $pwCheckSql->fetch(PDO::FETCH_ASSOC);
			$tokenMySQL = md5($cid.$pwCheckRow["create_time"]);

			if($token != $tokenMySQL){
				// 踢回首頁
				header('Location: index.php');
			}
		}
	}
	
?>



<?php include("source/head.php"); ?>

<script type="text/javascript">
	$(document).ready(function() {
		$('#reset-pw').click(function() {
			if($('.pw').val() != $('.pw-confirm').val()){
				alert("請輸入兩次相同的密碼唷！");
			}
			else{
				$('#form-reset-pw').submit();
			}
		});
	});
</script>


<body>
	<form id="form-reset-pw" class="form-horizontal  tasi-form" action="" method="post">
		<section id="login">
			<h4 class="login-header">設定登入密碼</h4>
			<div class="login-body">
				<input type="text" name="company_id" class="form-control input-lg" placeholder="請輸入企業統編" disabled="disabled" value="<?php echo($cid); ?>">
				<input type="password" name="pw" class="form-control input-lg pw" placeholder="請輸入新的密碼">
				<input type="password" name="pwConfirm" class="form-control input-lg pw-confirm" placeholder="請再次輸入密碼確認">
				<br/><br/>
				<a href="#" id="reset-pw" class="btn btn-danger btn-lg btn-login btn-shadow btn-block">確定</a>
			</div>
		</section>
	</form>

</body>

<?php include("source/footer.php"); ?>
