<?php
	require("apo/session.php");
	
	include("apo/sqldata.php");
	include("source/head.php");

	//TODO
	//2file ini
	if(isset($_GET['yvtset'])){
		$yvtset = $_GET['yvtset'];
	}

	require("yvtset/".$yvtset.".php");
	//2file end

	$listSqlStr_PRE = "SELECT * FROM `".$yvtListMeta['sql_tbl_name']."` ";
	$listSqlStr_MID = "";
	$listSqlStr_FIN = "ORDER BY `".$yvtListMeta['sql_tbl_order_desc']."` DESC ;";

	$listSqlpArr = array();

	//for fast add
	$addQsParam = "yvtset=".$yvtset;

	//foreign keys
	if( isset($yvtListMeta['fk_keys']) ){
		$whereSqlStr_MID = "";
		$whereSqlStr_NEW = false;
		foreach ($_GET as $gk => $gv) {
			if( in_array($gk, $yvtListMeta['fk_keys']) ){

				if($yvtInsertSQL_NEW_FLAG){	$whereSqlStr_MID .= ",";	}

				$whereSqlStr_MID .= ' `'.$gk.'` = ? ';
				$whereSqlStr_NEW = true;
				$listSqlpArr[] = $gv;

				$addQsParam .= "&".$gk."=".$gv;
			}
		}

		if( strlen($whereSqlStr_MID) > 0 ){
			$listSqlStr_MID = " WHERE ".$whereSqlStr_MID;
		}
	}
?>
<body id="<?php echo($yvtListMeta['label_id']); ?>">
	<section id="container">
		<?php include("source/header.php"); ?>
		<?php include("source/navi.php"); ?>
	
		<section id="main">
			<div class="title-wrapper">
				<div class="title-content pull-left">
					<h3><?php echo($yvtListMeta['labelname']); ?>管理</h3>
					<small>檢視全部<?php echo($yvtListMeta['labelname']); ?></small>
				</div>
				<div class="title-plus pull-right">
					<a href="yvt-add.php?<?php echo($addQsParam);?>" type="button" class="btn btn-danger"><i class="fa fa-plus"></i> 建立新的<?php echo($yvtListMeta['labelname']); ?></a>
				</div>
			</div>

			<section class="panel">
				<header class="panel-heading">
					<?php echo($yvtListMeta['labelname']); ?>明細
				</header>
				<table class="table table-striped table-bordered datatable-ajax datatable-noside" id="ajs_datatable">
					<thead>
						<tr>
							<?php
								foreach($yvtListMeta['columns'] as $mtk => $mtv){
									if( $mtv['listshow'] != false ){
										echo('
							<th>'.$mtv['label'].'</th>
											');
									}
								}
							?>
						</tr>
					</thead>
					<tbody>
					</tbody>
					<tfoot>
						<tr>
							<?php
								foreach($yvtListMeta['columns'] as $mtk => $mtv){
									if( $mtv['listshow'] != false ){
										echo('
							<th>'.$mtv['label'].'</th>
											');
									}
								}
							?>
						</tr>
					</tfoot>
				</table>
			</section>
		</section>
	</section>

	<script type="text/javascript">
	$(document).ready( function() {
	    $('#ajs_datatable').dataTable({
	    	"ajax": 'apo/yvt-ajlist.php?yvtset=<?php echo($yvtset);?>',
	    	"language": {
	            "lengthMenu": "顯示 _MENU_ 筆資料",
	            "zeroRecords": "",
	            "info": "顯示 _START_ 至 _END_ 筆，共 _TOTAL_ 筆",
	            "infoEmpty": "無資料",
	            "infoFiltered": "（從 _MAX_ 筆資料過濾）",
	            "paginate": {
					"next": "下一頁",
					"previous": "前一頁"
				},
				"sSearch": "<i class='fa fa-search'></i> 搜尋：",
	        },
	        <?php
	        if($yvtset=='books'){
	        	echo('"aaSorting": [[ 6, "desc" ]],');
	        }else{
	        	echo('"aaSorting": [[ 0, "desc" ]],');
	        }
	        ?>
	        "dom": 'Tlfrtip',
	        "tableTools": {
	            "sSwfPath": "lib/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
	            "aButtons": [
	                {
	                    "sExtends":    "copy",
	                    "sButtonText": "複製表格",
	                    "sDiv":        "copy",
	                },
	                {
	                    "sExtends":    "xls",
	                    "sButtonText": "匯出成 Excel",
	                    "sDiv":        "xls",
	                }
	            ]
	        },
	        "initComplete": function () {
	            var api = this.api();
	            api.$('tr').click( function () {
	            	var itm_id = $(this).children('td:eq(0)').html();
	            	// location.href = "yvt-modify.php?yvtset=items&id="+itm_id;
	            	var newWin = window.open();
 					newWin.location= "yvt-modify.php?yvtset=<?php echo($yvtset);?>&id="+itm_id;
	            } );
	        }
	    });

	    $("#ajs_datatable_wrapper").css('overflow-x','scroll');
	});
	</script>


</body>

<?php include("source/footer.php"); ?>
