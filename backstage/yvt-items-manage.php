<?php
	if(isset($_GET['id'])){
		$uno_id = $_GET['id'];
	}else{
		header("Location: product.php");
	}

	require("apo/session.php");
	include("apo/sqldata.php");
	include("source/head.php");

	//for 愛藝享，opts分類
	$optsCatGroup = array(
							"1"	=>	array(1),
							"2"	=>	array(2,3,4)
		);

	$itemsDataSql = $dbConnect->prepare("SELECT * FROM `items` WHERE `items_id` = ? ;");
	$itemsDataSql->execute(array($uno_id));

	$itemsDataRow = $itemsDataSql->fetch(PDO::FETCH_ASSOC);
?>


<?php include("source/head.php"); ?>

<!-- CKEditor  -->
<!-- <script type="text/javascript" src="lib/ckeditor/ckeditor.js"></script> -->

<script type="text/javascript">
$(document).ready(function(){
        // CKEDITOR
        // CKEDITOR.replaceAll();
});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#delete').click(function(){
			var chk = confirm("您確定要刪除？刪除後是無法復原的喔！");
			if(chk == true){
				// window.location.href = 'apo/yvt-delete.php?yvtset=<?php echo($yvtset); ?>&id=<?php echo($unoSelectRow[$yvtListMeta['columns_idx']]); ?>';
			}
		});
	});
</script>

<style>
 
/* Side notes for calling out things
-------------------------------------------------- */
 
/* Base styles (regardless of theme) */
.bs-callout {
  margin: 20px 0;
  padding: 15px 30px 15px 15px;
  border-left: 5px solid #eee;
}
.bs-callout h4 {
  margin-top: 0;
}
.bs-callout p:last-child {
  margin-bottom: 0;
}
.bs-callout code,
.bs-callout .highlight {
  background-color: #fff;
}
 
/* Themes for different contexts */
.bs-callout-danger {
  background-color: #fcf2f2;
  border-color: #dFb5b4;
}
.bs-callout-warning {
  background-color: #fefbed;
  border-color: #f1e7bc;
}
.bs-callout-info {
  background-color: #f0f7fd;
  border-color: #d0e3f0;
}

</style>

<body id="items">
	<section id="container">
		<?php include("source/header.php"); ?>
		<?php include("source/navi.php"); ?>
	
		<section id="main">
			<div class="title-wrapper">
				<div class="title-content pull-left">
					<h3>商品庫存</h3>
					<small></small>
				</div>
				<div class="title-plus pull-right">
					<!-- 可以在 #main 右上角放一些額外的按鈕 -->
					<a href="yvt-modify.php?yvtset=items&id=<?php echo($uno_id); ?>" type="button" class="btn btn-danger"><i class="fa fa-list"></i> 回到商品頁</a>
				</div>
			</div>
			<form class="form-horizontal tasi-form" action="apo/yvt-items-mng-update.php" method="post" enctype="multipart/form-data">
				<section class="panel">
					<header class="panel-heading">
						修改商品庫存與價格 for <?php echo('['.$itemsDataRow['name_tw']."] / [".$itemsDataRow['name_en'].']'); ?>
					</header>

					<!-- for vyt system -->
					<input type="hidden" name="items_id" value="<?php echo($uno_id); ?>" />
				<?php
				//庫存管理

				//cat of options
				$allOptsCatArr = array();
				$allOptsCatSql = $dbConnect->query("SELECT * FROM `items_opts_cats` ;");
				while($allOptsCatRow = $allOptsCatSql->fetch(PDO::FETCH_ASSOC)){
					$allOptsCatArr[ $allOptsCatRow['items_opts_cats_id'] ] = array('text'=>$allOptsCatRow['text'],'opts'=>array());
				}
					
				//cache all opts
				$allOptsSql = $dbConnect->query("SELECT * FROM `items_opts` ;");
					// $allOptsByCatArr = array();
					// items_opts_cats_id
				$allOptsNameArr = array();
				while($allOptsRow = $allOptsSql->fetch(PDO::FETCH_ASSOC)){
					$allOptsNameArr[ $allOptsRow['ID'] ] = $allOptsRow['descpt'];

					$allOptsCatArr[$allOptsRow['items_opts_cats_id']]['opts'][] = $allOptsRow;
				}

					//show all qty & prices
					echo('
					<section class="panel">
						<header class="panel-heading">
							庫存及價格
						</header>
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>產品選項</th>
									<th>總庫存</th>
									<th>出貨量</th>
									<th>當前價格</th>
								</tr>
							</thead>
							<tbody>
						');


					$qtyItemsSql = $dbConnect->prepare("SELECT * FROM `items_opts_quantity` WHERE `items_id` = ? ;");
					$qtyItemsSql->execute(array($uno_id));
					if( $qtyItemsSql->rowCount() > 0 ){
						while($qtyItemsRow = $qtyItemsSql->fetch(PDO::FETCH_ASSOC)){
							echo('
								<tr>
								');

							//parse all opts
							$qtyItemsOptsStr = "";
            				$optsArr = explode("_",$qtyItemsRow['items_opts_hash']);

				            foreach($optsArr as $opk => $opv){
			                    if( isset($allOptsNameArr[$opv]) ){
			                    	$qtyItemsOptsStr .= $allOptsNameArr[$opv]." ";
			                    }
				            }
							echo('
									<td>'.$qtyItemsOptsStr.'</td>
									<td>
										<input size="8" style="float:left;" type="text" name="qty'.$qtyItemsRow['items_opts_hash'].'" class="form-control wp25" value="'.$qtyItemsRow['qty_num'].'">
									</td>
									<td>'.$qtyItemsRow['sell_num'].'</td>
								');

							$pricesItemsSql = $dbConnect->prepare("SELECT * FROM `items_opts_prices` WHERE `items_id` = ? AND `items_opts_hash` = ? ORDER BY `updtime` DESC LIMIT 0, 1 ;");
							$pricesItemsSql->execute(array($uno_id,$qtyItemsRow['items_opts_hash']));
							$pricesItemsRow = $pricesItemsSql->fetch(PDO::FETCH_ASSOC);
							echo('
									<td>
										<input size="8" style="float:left;" type="text" name="prices'.$qtyItemsRow['items_opts_hash'].'" class="form-control wp25" value="'.$pricesItemsRow['prices'].'">
									</td>
								');
							echo('
								</tr>
								');
						}
					}
					echo('
							<tbody>
						</table>
					</section>
						');
					
				?>
				<input type="submit" class="btn btn-primary" value="更新">
			</form>
			<hr/>
			<form class="form-horizontal tasi-form" action="apo/yvt-items-mng-insert.php" method="post" enctype="multipart/form-data">
				<section class="panel">
					<header class="panel-heading">
						新增商品選項
					</header>
					<input type="hidden" name="items_id" value="<?php echo($uno_id); ?>" />
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
							<?php
								$OkOptsArr = $optsCatGroup[$itemsDataRow['items_cat_id']];
								foreach($allOptsCatArr as $ock => $ocv){
									if(is_array($OkOptsArr) && in_array($ock, $OkOptsArr)){
										if(count($ocv['opts']) > 0){
											echo('<th>'.$ocv['text'].'</th>');
										}
									}
								}
							?>
								<th>初次上架數量</th>
								<th>價格</th>
							</tr>
						</thead>
						<tbody>
							<tr>

							<?php
							// manual
								
								foreach($allOptsCatArr as $ock => $ocv){
									if(is_array($OkOptsArr) && in_array($ock, $OkOptsArr)){
										if(count($ocv['opts']) > 0){
											echo('
									<td>
										<select class="form-control m-bot15 wf200" name="new_opts[]">
											<option value="">&nbsp;</option>
												');
												foreach ($ocv['opts'] as $opk => $opv) {
												echo('
											<option value="'.$opv['ID'].'">'.$opv['descpt'].'</option>
													');
											}
									echo('
										</select>
									</td>
										');
										}
									}
								}

							
							/* auto
								foreach($allOptsCatArr as $ock => $ocv){
									if(count($ocv['opts']) > 0){
										echo('
								<td>
									<select class="form-control m-bot15 wf200" name="new_opts[]">
										<option value="">&nbsp;</option>
											');
											foreach ($ocv['opts'] as $opk => $opv) {
											echo('
										<option value="'.$opv['ID'].'">'.$opv['descpt'].'</option>
												');
										}
								echo('
									</select>
								</td>
									');
									}
								}
								*/
							?>
								<td>
									<input size="8" style="float:left;" type="text" name="new_opts_qty" class="form-control wp25" placeholder="請輸入上架數量" />
								</td>
								<td>
									<input size="8" style="float:left;" type="text" name="new_opts_price" class="form-control wp25" placeholder="請輸入價格" />
								</td>
							</tr>
						</tbody>
					</table>
					<input type="submit" class="btn btn-primary" value="新增">
				</section>
			</form>
		</section>
	</section>
</body>

<?php include("source/footer.php"); ?>
