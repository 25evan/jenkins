<?php

	session_start();
	include("apo/sqldata.php");

	if( isset( $_GET["cid"] ) ) {
		$cid = $_GET["cid"];
	} else {
		$cid = null;
	}

	if( isset($_POST["pw"]) && $_POST["pw"] != "" ) {
		// 判斷密碼是否正確
		$loginCheckSql = $dbConnect -> prepare( "SELECT * FROM `Account` WHERE `company_id` = ? ;" );
		$loginCheckSql -> execute( array($_POST["company_id"]) );

		if( $loginCheckSql -> rowCount() > 0 ) {
			$loginCheckRow = $loginCheckSql -> fetch(PDO::FETCH_ASSOC);
			
			$md5PW = md5($_POST["company_id"].$_POST["pw"]);

			if( $md5PW != $loginCheckRow["password"] ) {
				// 踢回首頁
				header('Location: index.php?pw=false');
			} else {
				// 驗證成功，寫入 SESSION
				$_SESSION['ACCOUNT_TYPE'] = $loginCheckRow['account_type'];
				$_SESSION['CID'] = $loginCheckRow['company_id'];
				$_SESSION['NAME'] = $loginCheckRow['name'];

				$accountPrivilegeSelectSql = $dbConnect -> prepare( "SELECT * FROM `AccountPrivilege` WHERE `account_type` = ? ;" );
				$accountPrivilegeSelectSql -> execute( array($_SESSION['ACCOUNT_TYPE']) );
				$accountPrivilegeSelectDetail = $accountPrivilegeSelectSql -> fetch(PDO::FETCH_ASSOC);
				$array_privilege = json_decode( $accountPrivilegeSelectDetail['account_privilege'], true );

				if( $array_privilege != null ) {
					header('Location: yvt-list.php?yvtset='.$array_privilege[0].'');
				} else {
					header('Location: index.php?priviledge=false');
				}
			}
		} else {
			header('Location: index.php?pw=false');
		}
	}
?>

<?php include("source/head.php"); ?>

<body>
	<form class="form-horizontal  tasi-form" action="" method="post">
		<section id="login">
			<h4 class="login-header"><?php echo $config['project']; ?> Backstage 後台登入</h4>
			<div class="login-body">
				<?php
					// 如果密碼輸入錯誤
					if(isset($_GET["pw"]) && $_GET["pw"] && $_GET["pw"] == 'false'){
						echo('
							<p class="danger">請輸入正確的統編和密碼</p>
						');
					}else if(isset($_GET["priviledge"]) && $_GET["priviledge"] == 'false'){
						echo('
							<p class="danger">您沒有權限進入</p>
						');
					}
				?>
				<input type="text" name="company_id" class="form-control input-lg" placeholder="請輸入帳號" value="<?php echo($cid); ?>">
				<input type="password" name="pw" class="form-control input-lg" placeholder="請輸入密碼">
				<a href="#" class="pull-right">忘記密碼？</a>
				<br/><br/>
				<input class="btn btn-danger btn-lg btn-login btn-shadow btn-block" type="submit" value="登入" />
			</div>
		</section>
	</form>
</body>

<?php include("source/footer.php"); ?>

