<?php
	session_start();
	include("apo/sqldata.php");

	$email = null;
	$message = null;

	// 如果沒有權限 直接踢出
	if($_SESSION['ACCOUNT_TYPE'] == null || $_SESSION['ACCOUNT_TYPE'] != 1){
		echo "<script language=javascript>
	      window.location.replace(\"apo/logout.php\");
	      top.leftFrame.location.reload();
	      </script>";
	}

	if(isset($_POST["company_id"])){
		$accountSelectSql = $dbConnect->prepare("SELECT * FROM `Account` WHERE `company_id` = ? ;");
		$accountSelectSql->execute(array($_POST["company_id"]));
		if($accountSelectSql->rowCount() > 0){
			echo '<script>
				alert("帳號重複，請輸入不同的帳號或重發認證信！");
				window.location = "account.php";
			</script>';
			exit;
		}
	}

	if(isset($_POST["setting_type"]) && $_POST["setting_type"] == 1){
		$nowTime = date("Y-m-d H:i:s");

		$accoutAddSql = $dbConnect->prepare("INSERT INTO `Account` (`company_id`, `name`,`department`,`password`, `account_type`, `create_time`, `creator`)
										  VALUES (?, ?, ?, ?, ?, ?, ?);");
		if($accoutAddSql->execute(array($_POST["company_id"], $_POST["name"], $_POST["department"], md5($_POST["company_id"].$_POST["password"]), $_POST["account_type"], $nowTime, $_SESSION['NAME']))){
			echo '<script>
				alert("新增成功！");
				window.location = "account.php";
			</script>';
		}else{
			echo '<script>
				alert("新增失敗！請聯絡技術人員！");
				window.location = "account.php";
			</script>';
		}
	}else if(isset($_POST["setting_type"]) && $_POST["setting_type"] == 2){
		if(isset($_POST["company_id"]) && $_POST["company_id"] != ""){
			$nowTime = date("Y-m-d H:i:s");

			$accoutAddSql = $dbConnect->prepare("INSERT INTO `Account` (`company_id`, `name`,`department`,`email`, `account_type`, `create_time`, `creator`)
											  VALUES (?, ?, ?, ?, ?, ?, ?);");
			$accoutAddSql->execute(array($_POST["company_id"], $_POST["name"], $_POST["department"], $_POST["email"], $_POST["account_type"], $nowTime, $_SESSION['NAME']));
			
			$email = $_POST["email"];

			$token = $config['hostUrl']."/backstage/pw-reset.php?cid=".$_POST["company_id"]."&token=".md5($_POST["company_id"].$nowTime);
			
			$message = '\
					<meta http-equiv="content-type" content="text/html; charset=UTF-8" />\
					<body style="margin:0; padding:15px; background: #F5F5F5">\
					<div style="width:100%; background: #F5F5F5; padding: 30px">\
						<div style="background: #FFF; margin: 0 auto; padding: 20px; width: 420px; -webkit-border-bottom-right-radius: 10px;-webkit-border-bottom-left-radius: 10px;-moz-border-radius-bottomright: 10px;-moz-border-radius-bottomleft: 10px;border-bottom-right-radius: 10px;border-bottom-left-radius: 10px; border-top: 5px solid #009bc2">\
							<h1 style="font: 20px/36px Lucida Grande, Helvetica, Arial, sans-serif; color: #444;">親愛的 '.$_POST["name"].',</h1>\
							<h2 style="font: 16px/16px Lucida Grande, Helvetica, Arial, sans-serif; color: #444;">歡迎來到 '.$config['project'].' Backstage管理後台</h2><br/>\
							<p style="font: 14px/26px Lucida Grande, Helvetica, Arial, sans-serif; color: #555;">\
								您的帳號已經建立成功，請點擊以下連結設定您的密碼，完成註冊：</p>\
							<p style="font: 14px/26px Lucida Grande, Helvetica, Arial, sans-serif; color: #000;">\
							<p style="font: 14px/20px Lucida Grande, Helvetica, Arial, sans-serif; color: #555;">\
								<a href="'.$token.'" style="background:#48B0EC; color:#FFF; text-decoration:none; padding:8px 15px; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;">設定我的密碼</a>\
							</p>\
							<br />\
							<p style="font: 12px/20px Lucida Grande, Helvetica, Arial, sans-serif; color: #F26522;">With love,	<br />\
								25sprout</p>\
						</div>\
					</div>\
					</body>\
					</html>';

		 	// header('Location: account-update.php?cid='.$_POST["company_id"]);
			// exit;
		}
	}

?>


<?php include("source/head.php"); ?>

<script type="text/javascript">
	var regMail = "<?php echo($email); ?>";

	$(document).ready(function() {
		var setting_type = $('select[name="setting_type"]').val();
		$('select[name="setting_type"]').change(function(){
			if($(this).val() == 1){
				$('.email').css('display', 'none');
				$('.password').css('display', 'block');
			}else if($(this).val() == 2){
				$('.email').css('display', 'block');
				$('.password').css('display', 'none');
			}
		});
		if(setting_type == 2){
			if(regMail != ""){
				$.ajax({
					type: 'POST',
					cache: false,
					url: 'http://www.25sprout.com/aws_ses/index_super.php',
					data: {
						mailer: 'aws',
						Source: 'evan@25sprout.com',
						ToAddresses: regMail,
						CcAddresses: '',
						BccAddresses: '',
						Subject: '註冊通知：CTBC Backstage管理後台',
						Body: '<?php echo($message); ?>',
						ReplyToAddresses: 'evan@25sprout.com',
						ReturnPath: 'evan@25sprout.com',
						Success: '成功寄出，謝謝您的來信',
						Failed: '信件尚未寄出，請稍後再試'
					},
					error: function() {
						alert("送出失敗");
						// 看你前台有沒有要有什麼變化。
					},
					success: function(data) {
						alert('已成功寄出密碼開通信至該帳戶信箱，請提醒對方收信完成開通帳號。');
						window.location = "account.php";
						// location.reload();
						// 看你前台有沒有要有什麼變化。
					}
				});
			}else{
				alert('請輸入E-mail');
			}
		}
	});
</script>


<body id="account">
	<section id="container">
		<?php include("source/header.php"); ?>
		<?php include("source/navi.php"); ?>
	
		<section id="main">
			<div class="title-wrapper">
				<div class="title-content pull-left">
					<h3>帳號建立</h3>
					<small></small>
				</div>
				<div class="title-plus pull-right">
					<!-- 可以在 #main 右上角放一些額外的按鈕 -->
				</div>
			</div>
			<form class="form-horizontal  tasi-form" action="?" method="post">
				<section class="panel">
					<header class="panel-heading">
						新增帳號
					</header>
					
					<div class="panel-body">
						<div class="form-group">
							<label class="control-label col-md-2">設定方式</label>
							<div class="col-md-8">
								<select class="form-control m-bot15 wf200" name="setting_type">
									<option value="1" selected>直接設定密碼</option>
									<!-- <option value="2">E-mail設定密碼</option> -->
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">部門別</label>
							<div class="col-md-8">
								<input size="16" type="text" name="department" class="form-control wp50">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">帳號</label>
							<div class="col-md-8">
								<input size="16" type="text" name="company_id" class="form-control wf200">
							</div>
						</div>
						<div class="form-group password">
							<label class="control-label col-md-2">密碼</label>
							<div class="col-md-8">
								<input size="16" type="password" maxLength="8" name="password" class="form-control wf200">
								<br/>
								<span class="label label-danger">Note</span>
								請輸入8個字密碼
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">登入會員姓名</label>
							<div class="col-md-8">
								<input size="16" type="text" name="name" class="form-control wp50">
							</div>
						</div>
						<div class="form-group email" style="display: none;">
							<label class="control-label col-md-2">E-mail</label>
							<div class="col-md-8">
								<input size="16" type="text" name="email" class="form-control wp50">
								<br/>
								<span class="label label-danger">Note</span>
								當建立帳號時，系統會自動寄發一封驗證信至這個 E-mail 要求設定新的密碼進行登入。
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">會員類別</label>
							<div class="col-md-8">
								<select class="form-control m-bot15 wf200" name="account_type">
									<?php
										$privilegeSelectSql = $dbConnect->prepare("SELECT * FROM `AccountPrivilege` ORDER BY `account_type` ;");
										$privilegeSelectSql->execute();
										$privilegeSelectDetail = $privilegeSelectSql->fetchAll(PDO::FETCH_ASSOC);
										foreach ($privilegeSelectDetail as $key => $value) {
											if($privilegeSelectDetail[$key]['account_type'] == 2){
												echo '
									<option value="'.$privilegeSelectDetail[$key]['account_type'].'" selected>'.$privilegeSelectDetail[$key]['account_type_name'].'</option>
												';
											}else{
												echo '
									<option value="'.$privilegeSelectDetail[$key]['account_type'].'">'.$privilegeSelectDetail[$key]['account_type_name'].'</option>
												';
											}
										}
									?>
								</select>
							</div>
						</div>
					</div>
				</section>

				<hr/>
				<input type="submit" class="btn btn-primary" value="確定">
			</form>
		</section>
	</section>


</body>

<?php include("source/footer.php"); ?>
