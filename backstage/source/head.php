<!DOCTYPE html>
<html lang="en">
<head>

	<!-- HTML meta 設定 -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="google-site-verification" content="" />
	<meta name="description" content="務必輸入的網站描述 for Google Search">
	<meta name="author" content="25sprout, LLC. 新芽網路有限公司">
	<meta name="copyright" content="網站版權聲明">
	<meta name="keywords" content="網站關鍵字" />
	<meta name="URL" content="網站連結">

	<!-- 網站標題 & Favicon &  -->
	<title><?php echo $config['project']; ?> Backstage</title>
	<link rel="shortcut icon" href="images/favi.png" >

	<!-- jQuery -->
	<script type="text/javascript" src="lib/jquery/jquery-2.1.1.min.js"></script>

	<!-- jQuery UI -->
	<link type="text/css" rel="stylesheet" href="lib/jquery-ui/css/ui-lightness/jquery-ui-1.10.0.custom.css">
	<script type="text/javascript" src="lib/jquery-ui/js/jquery-ui-1.10.0.custom.min.js"></script>

	<!-- CKeditor -->
	<script type="text/javascript" src="lib/ckeditor/ckeditor.js"></script>

	<!-- Datetimepicker  -->
	<link type="text/css" rel="stylesheet" href="lib/datepicker/css/datepicker.css">
	<script type="text/javascript" src="lib/datepicker/js/bootstrap-datepicker.js"></script>
	<link type="text/css" rel="stylesheet" href="lib/datepicker/css/datetimepicker.css">
	<script type="text/javascript" src="lib/datepicker/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="lib/twzipcode/jquery.twzipcode-1.6.7.js"></script>

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
	<link type="text/css" rel="stylesheet" href="lib/bootstrap/css/bootstrap-reset.css">
	<script type="text/javascript" src="lib/bootstrap/js/bootstrap.min.js"></script>

	<!-- Font Awesome -->
	<link rel="stylesheet" href="lib/font-awesome/css/font-awesome.min.css">

	<!-- My LESS Style -->
	<link type="text/css" rel="stylesheet" href="css/style.css">
	<!--<link rel="stylesheet/less" type="text/css" href="css/style.less">
	<script src="lib/less.min.js" type="text/javascript"></script>-->

	<!-- Morris Chart -->
	<!-- <link rel="stylesheet" href="http://flatlab.t16.se/assets/morris.js-0.4.3/morris.css">
	<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
	<script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script> -->

	<!-- Google Chart -->
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>

	<!-- Google Font -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>

	<!-- jQuery Textarea Autosize -->
	<script type="text/javascript" src="lib/jquery-autosize/jquery.autosize-min.js"></script>
	<script>
		$(document).ready(function(){
			$('textarea').autosize();
		});
	</script>

	<!-- Google Analytics ***** 務必更新 GA Account No# *****-->
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-31519246-1']);
		_gaq.push(['_trackPageview']);

		(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>


	<!-- Bootstrap Setting -->
	<script type="text/javascript">
		$(document).ready(function() {
		// Tooltip	
			$('.fa-question-circle').tooltip();
		});
	</script>


	<!-- DataTable -->
	<link rel="stylesheet" type="text/css" href="lib/DataTables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="lib/DataTables/extensions/TableTools/css/dataTables.tableTools.min.css">
	<script type="text/javascript" language="javascript" src="lib/DataTables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" language="javascript" src="lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
	<script type="text/javascript">
		$(document).ready( function() {

		    $('.datatable').dataTable({
		    	"language": {
		            "lengthMenu": "顯示 _MENU_ 筆資料",
		            "zeroRecords": "沒有相符的搜尋結果",
		            "info": "顯示 _START_ 至 _END_ 筆，共 _TOTAL_ 筆",
		            "infoEmpty": "無資料",
		            "infoFiltered": "（從 _MAX_ 筆資料過濾）",
		            "paginate": {
						"next": "下一頁",
						"previous": "前一頁"
					},
					"sSearch": "<i class='fa fa-search'></i> 搜尋：",
		        },
	            "aaSorting": [[ 0, "desc" ]],
		        "dom": 'Tlfrtip',
		        "tableTools": {
		            "sSwfPath": "lib/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
		            "aButtons": [
		                {
		                    "sExtends":    "copy",
		                    "sButtonText": "複製表格",
		                    "sDiv":        "copy",
		                },
		                {
		                    "sExtends":    "xls",
		                    "sButtonText": "匯出成 Excel",
		                    "sDiv":        "xls",
		                }
		            ]
		        }
		    });
		
		// Datepicker
			$('.bs-datepicker').datepicker({
				format: 'yyyy-mm-dd',
			}).on('changeDate', function(){
				$(this).datepicker('hide');
			});

			$(".bs-datetimepicker").datetimepicker({
			    format: "yyyy-mm-dd hh:ii:00",
			    autoclose: true,
			    todayBtn: false,
			    pickerPosition: "bottom-left"
			});

		});
	</script>

	<!-- multi select lou -->
	<script type="text/javascript" src="lib/lou-multi-select/js/jquery.multi-select.js"></script>
	<link type="text/css" rel="stylesheet" href="lib/lou-multi-select/css/multi-select.css">
	<script type="text/javascript" src="lib/quicksearch/jquery.quicksearch.js"></script>

	<!-- multi select 25sprout -->
	<link type="text/css" rel="stylesheet" href="lib/yvt-mul-select/css/yvt-mul-select.css">
	<script type="text/javascript" src="lib/yvt-mul-select/js/yvt-mul-select.js"></script>

	<!--[if lt IE 9]>
	<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>

