<navi id="navi">
	<ul class="sidebar-menu" id="nav-accordion">
		<?php
			//尚未使用yvtset的頁面
			$array_temp = array( 'account' );

			$privilegeSelectSql = $dbConnect -> prepare( "SELECT * FROM `AccountPrivilege` WHERE `account_type` = ? ;" );
			$privilegeSelectSql -> execute( array($_SESSION['ACCOUNT_TYPE']) );
			$privilegeSelectDetail = $privilegeSelectSql -> fetch(PDO::FETCH_ASSOC);
			$array_privilege = json_decode( $privilegeSelectDetail['account_privilege'], true );

			$naviSelectSql = $dbConnect -> prepare( "SELECT * FROM `metadata` WHERE `key` = 'navi' ;" );
			$naviSelectSql -> execute();
			$naviSelectDetail = $naviSelectSql -> fetch(PDO::FETCH_ASSOC);
			$array_navi = json_decode( $naviSelectDetail['value'], true );
			foreach( $array_navi as $key => $value ) {
				if( isset($array_privilege) && $array_privilege != null && in_array( $array_navi[$key]['name'], $array_privilege ) ) {
					if( in_array( $array_navi[$key]['name'], $array_temp ) ) {
						echo '
			<li class="sub-menu dcjq-parent-li">
				<a href="'.$array_navi[$key]['name'].'.php" id="navi-'.$array_navi[$key]['name'].'" class="dcjq-parent">
					<i class="fa fa-'.$array_navi[$key]['icon'].'"></i>
					<span>'.$array_navi[$key]['name_tw'].'</span>
					<span class="dcjq-icon"></span>
				</a>
			</li>';
					} else {
						echo '
			<li class="sub-menu dcjq-parent-li">
				<a href="yvt-list.php?yvtset='.$array_navi[$key]['name'].'" id="navi-'.$array_navi[$key]['name'].'" class="dcjq-parent">
					<i class="fa fa-'.$array_navi[$key]['icon'].'"></i>
					<span>'.$array_navi[$key]['name_tw'].'</span>
					<span class="dcjq-icon"></span>
				</a>
			</li>';
					}
				}
			}
		?>
	</ul>
</navi>

<script type="text/javascript">
	$(document).ready( function(){
		var thisNavi = $('body').attr('id');
		$('#navi-'+thisNavi).addClass('active');
	});
</script>

