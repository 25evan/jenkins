<div id="yvt-quick-navi">
	<div>快速工具列</div>
	<ul>
		<li><a id="gototop-btn" onclick="javascript:scrollto(0);"><i class="fa fa-arrow-circle-up"></i> GO TOP</a></li>

		<?php 
		// print_r($yvtListMeta['quick_navi']);
		// Array ( [0] => items_id [1] => ta_ima010 [2] => sale_desc [3] => img_shelf_url [4] => books_peoples )
		if(isset($yvtListMeta['quick_navi'])){
			if(is_array($yvtListMeta['quick_navi'])){
				foreach($yvtListMeta['quick_navi'] as $qk => $qv){
					if(isset($yvtListMeta['columns'][$qv])){
						if(isset($yvtListMeta['columns'][$qv]['label'])){
							echo('<li><a class="yvthref" yvtnavihref="'.$qv.'">'.$yvtListMeta['columns'][$qv]['label'].'</a></li>');
						}
					}

					if(isset($yvtListMeta['rel_tbl'][$qv])){
						if(isset($yvtListMeta['rel_tbl'][$qv]['label'])){
							echo('<li><a class="yvthref" yvtnavihref="'.$qv.'">'.$yvtListMeta['rel_tbl'][$qv]['label'].'</a></li>');
						}
					}
					
				}
			}
		}
		?>
		<li><a id="quick-save-btn" onclick="javascript:quicksave();"><i class="fa fa-floppy-o"></i> 快速存檔</a></li>
	</ul>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('.yvthref').on('click',function(){
			var thishref = $(this).attr('yvtnavihref');
			var target = $('[yvtnavi="'+thishref+'"]')
			if(target.length == 1){
				scrollto(target.offset().top-50);
			}
		});
	});

	function scrollto(position){
		$('body,document').scrollTop(position);
	}

	function quicksave(){
		if($('form').length == 1){
			$('form').submit();
		}
	}
</script>