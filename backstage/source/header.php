<header id="header" class="header">

	<div class="logo">
		<img src="images/logo.png" />
	</div>

	<div class="account-wrapper">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			<span class="name pull-right">
				<?php echo($_SESSION["NAME"]); ?> <b class="caret"></b>
			</span>
			<span class="thumb-sm avatar pull-right">
				<img src="images/jake.jpg" alt="...">
			</span>
		</a>
		<ul class="dropdown-menu dropdown-menu-right animated fadeInRight">
			<li> 
				<a href="#">技術協助</a> 
			</li>
			<li>
				<a href="apo/logout.php">登出</a> 
			</li> 
		</ul>

	</div>

</header>

