<?php
	session_start();
	include("apo/sqldata.php");

	// 如果沒有權限 直接踢出
	if($_SESSION['ACCOUNT_TYPE'] == null || $_SESSION['ACCOUNT_TYPE'] != 1){
		echo "<script language=javascript>
	      window.location.replace(\"apo/logout.php\");
	      top.leftFrame.location.reload();
	      </script>";
	}

	// 更新 Update 表單
	if(isset($_POST["updateForm"]) && $_POST["updateForm"] != ""){
		if(isset($_POST["account-privilege"]) && $_POST["account-privilege"] != ""){
			$privilege = json_encode($_POST["account-privilege"]);
		}else{
			$privilege = null;
		}
		$privilegeSelectSql = $dbConnect->prepare("SELECT MAX(`account_type`) AS type FROM `AccountPrivilege` ;");
		$privilegeSelectSql->execute();
		$privilegeSelectDetail = $privilegeSelectSql->fetch(PDO::FETCH_ASSOC);
		$type = $privilegeSelectDetail['type'];
		$type++;
		$privilegeInsertSql = $dbConnect->prepare("INSERT INTO `AccountPrivilege`(`account_type`, `account_type_name`, `account_privilege`) VALUES(?, ?, ?) ;");
		$err = $privilegeInsertSql->execute(array($type, $_POST['account-type-name'], $privilege));

		if($err){
			echo "<script language=javascript>
				alert('新增成功！');
				window.location.replace(\"account-setting.php\");
				top.leftFrame.location.reload();
				</script>";
		}else{
			echo "<script language=javascript>
				alert('新增失敗！');
				window.location.replace(\"account-setting.php\");
				top.leftFrame.location.reload();
				</script>";
		}
	}
?>


<?php include("source/head.php"); ?>

<body id="account">
	<section id="container">
		<?php include("source/header.php"); ?>
		<?php include("source/navi.php"); ?>
	
		<section id="main">
			<div class="title-wrapper">
				<div class="title-content pull-left">
					<h3>新增會員類別</h3>
					<small>變更權限</small>
				</div>
				<div class="title-plus pull-right">
					<a href="account-setting.php" type="button" class="btn btn-info"><i class="fa fa-gear"></i> 回到權限管理</a>
				</div>
			</div>

			<form class="form-horizontal  tasi-form" action="privilege-create.php" method="post">
				<section class="panel">
					<header class="panel-heading">
						新增
					</header>
					
					<div class="panel-body">
						<div class="form-group">
							<label class="control-label col-md-2">類別名稱：</label>
							<div class="col-md-8">
								<input size="16" type="text" name="account-type-name" class="form-control wf200">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">權限選項：</label>
							<div class="col-md-8">
								<?php
									$naviSelectSql = $dbConnect->prepare("SELECT * FROM `metadata` WHERE `key` = 'navi' ;");
									$naviSelectSql->execute();
									$naviSelectDetail = $naviSelectSql->fetch(PDO::FETCH_ASSOC);
									$array_navi = json_decode($naviSelectDetail['value'], true);
									foreach ($array_navi as $key => $value) {
										echo '
								<input type="checkbox" name="account-privilege[]" value="'.$array_navi[$key]['name'].'">'.$array_navi[$key]['name_tw'].'<br/>
										';
									}
								?>
							</div>
						</div>
					</div>
				</section>

				<hr/>
				<input type="hidden" name="updateForm" value="update"/>
				<input type="submit" class="btn btn-primary" value="確定">
			</form>
		</section>
	</section>


</body>

<?php include("source/footer.php"); ?>
